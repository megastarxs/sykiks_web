import { compose } from 'recompose'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated, UserIsAdmin } from 'utils/router'
import { ProfileModalHandler } from 'components/ProfileModal'

export default compose(
  UserIsAuthenticated,
  UserIsAdmin,
  ProfileModalHandler,
  firebaseConnect(props => [
    {
      path: `payout/${props.params.id}/paidTo`,
      storeAs: `payoutdetails_${props.params.id}`
    }
  ]),
  connect(({ firebase: { data } }, props) => ({
    payoutdetails: data[`payoutdetails_${props.params.id}`],
    id: props.params.id
  })),
  spinnerWhileLoading(['payoutdetails'])
)
