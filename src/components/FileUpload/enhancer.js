import { compose, withStateHandlers, withHandlers } from 'recompose'

let CropperRef

export default compose(
  withStateHandlers(
    { open: false, image: null },
    {
      updateImage: () => image => ({ open: true, image }),
      close: () => () => ({ open: false })
    }
  ),
  withHandlers({
    handleChange: ({ updateImage }) => e => {
      let files = e.target.files
      let reader = new FileReader()
      reader.readAsDataURL(files[0])
      e.target.value = ''
      reader.onload = () => updateImage(reader.result)
    },
    setRef: () => ref => {
      CropperRef = ref
    },
    done: ({ onDone, close }) => () => {
      onDone(CropperRef.getCroppedCanvas().toDataURL())
      close()
    }
  })
)
