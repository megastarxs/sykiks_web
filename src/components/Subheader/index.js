import React from 'react'
import './Subheader.scss'

let Title = ({ children }) => <title>Sykiks - {children}</title>
let Subheader = ({ subheader }) => (
  <h4 className="sub-header">
    {subheader}
    <Title>{subheader}</Title>
  </h4>
)

export default Subheader
