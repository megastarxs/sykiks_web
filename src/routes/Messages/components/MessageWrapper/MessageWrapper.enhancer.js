import { compose } from 'redux'
import { connect } from 'react-redux'
import { withFirebase } from 'react-redux-firebase'
import { UserIsAuthenticated } from 'utils/router'

export default compose(
  UserIsAuthenticated,
  withFirebase,
  connect(({ firebase }, { router }) => ({
    profile: firebase.profile,
    roomId: router.params.roomId
  }))
)
