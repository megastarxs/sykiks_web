import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStateHandlers } from 'recompose'
import { now } from 'utils/helpers'
import { withFirebase } from 'react-redux-firebase'
import { withNotifications } from 'modules/notification'
import { withRouter } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'
export default compose(
  withFirebase,
  UserIsAuthenticated,
  // create listener for review, results go into redux
  // firebaseConnect([{ path: 'review' }]),
  // map redux state to props
  connect(({ firebase: { auth } }) => ({
    myId: auth.uid
  })),
  withRouter,
  withNotifications,
  withStateHandlers(
    { text: '', rate: '' },
    {
      changeText: () => e => ({
        text: e.target.value
      }),
      setRating: () => rating => ({
        rate: rating
      }),
      sendReview: (
        { text, rate },
        { firebase, myId, router, showError }
      ) => () => {
        if (rate == '') {
          showError('Please select Up or Down rating')
        } else if (text == '') {
          showError('Please enter your review')
        } else {
          let partnerId = router.params.id
          let sessionId = router.params.sessionid
          firebase.ref(`reviews/${partnerId}`).push({
            from: myId,
            message: text,
            postedOn: now,
            rating: rate
          })
          firebase
            .ref(`chat-sessions/${sessionId}`)
            .update({
              reviewed: true
            })
            .then(() => {
              router.push('/')
            })
          return {
            text: ''
          }
        }
      }
    }
  )
)
