import { now } from 'utils/helpers'
import firebase from 'firebase/app'

let firebaseDB = firebase.database()
let chatRequestRef = firebaseDB.ref(`chat-requests`)
let chatSessionsRef = firebaseDB.ref('chat-sessions')
let chatPairsRef = firebaseDB.ref('chat-pairs')
let settingsRef = firebaseDB.ref('settings')
let chatsRef = firebaseDB.ref('chats')
let usersRef = firebaseDB.ref('users')

let Chat = {}

Chat.initClientRequest = (client, psychic) => {
  chatRequestRef.push({
    time: now,
    status: 'init',
    client,
    psychic
  })
}

Chat.removeRequest = reqId => {
  chatRequestRef.child(reqId).remove()
}

Chat.denyRequest = reqId => {
  chatRequestRef.child(reqId).update({ status: 'rejected' })
}

Chat.acceptRequest = (reqId, profile, psychic, client) => {
  let sessionKey = chatSessionsRef.push().key
  chatRequestRef.child(reqId).update({ status: 'accepted', sessionKey })

  chatSessionsRef.child(sessionKey).update({
    sessionKey,
    chatRate: profile.chatRate,
    psychic,
    client,
    amount: 0,
    acceptedAt: now
  })
  Chat.setCurrentSessionKey(psychic, sessionKey)
  chatPairsRef.child(`${psychic}-${client}`).transaction(d => {
    let count = 1
    if (d && d.count) count = d.count + 1
    return { psychic, count, client }
  })
  usersRef.child(psychic).update({ status: 'Busy' })
}

Chat.setCurrentSessionKey = (uid, currentSessionKey) => {
  settingsRef.child(uid).update({ currentSessionKey })
}

Chat.endCurrentSession = (uid, isPsychic) => {
  settingsRef.child(`${uid}/currentSessionKey`).remove()
  if (isPsychic) usersRef.child(uid).update({ status: 'Available' })
}

Chat.startSession = (sessionKey, amount) => {
  Chat.updateSession(sessionKey, {
    sessionStarted: true,
    sessionStartedAt: now
  })
  setTimeout(() => {
    chatSessionsRef
      .child(sessionKey)
      .child('sessionEnded')
      .once('value', s => {
        if (!s.val())
          Chat.systemMessage({
            sessionKey,
            message: 'Paid session has started'
          })
        Chat.updateSession(sessionKey, {
          amount,
          sessionStarted: true
        })
      })
  }, 3000)
}

Chat.updateSession = (sessionKey, details) => {
  chatSessionsRef.child(sessionKey).update(details)
}

Chat.systemMessage = ({ sessionKey, message }) => {
  let from = 'system'
  chatsRef.child(sessionKey).push({
    from,
    message,
    time: now
  })
}

Chat.sendChat = ({ sessionKey, from, message }) => {
  chatsRef.child(sessionKey).push({
    from,
    message,
    time: now
  })
}

export default Chat
