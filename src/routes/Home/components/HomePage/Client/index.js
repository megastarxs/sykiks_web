import React from 'react'
import PsychicList from './PsychicList'
import Icon from 'components/Icon'
import Filter from './Filter'
import enhacer from './enhancer'

const Psychics = ({ filters, updateFilter, auth }) => (
  <div className="container client-home">
    <title>Sykiks - Home</title>
    <div className="header-div">
      <Icon name="ios-flower" className="psychic-header-icon" />
      <h3 className="purple-text">Psychic Advisors</h3>
      <h5 className="grey-text">
        Select a psychic below to get the clarity you need
      </h5>
    </div>
    <Filter updateFilter={updateFilter} auth={auth} filters={filters} />
    <div className="d-flex align-items-center mt-40 main-list">
      <PsychicList filters={filters} />
    </div>
  </div>
)

export default enhacer(Psychics)
