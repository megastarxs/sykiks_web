import * as admin from 'firebase-admin'
import initPaypal from '../paypal_config'
import { now } from '../helpers'
const url = require('url')

export default app => {
  app.get('/pay/:uid/:price', async (req, res) => {
    var redirectHost = 'sykiks.com'
    let paypal = await initPaypal()

    try {
      redirectHost = url.parse(req.headers['referer']).host
    } catch (error) {}

    const payReq = JSON.stringify({
      intent: 'sale',
      payer: {
        payment_method: 'paypal'
      },
      redirect_urls: {
        return_url: `${req.protocol}://${redirectHost}/api/process`,
        cancel_url: `${req.protocol}://${redirectHost}/payment?status=failed`
      },
      transactions: [
        {
          amount: {
            total: req.params.price,
            currency: 'USD'
          },
          // description: req.params.uid,
          custom: req.params.uid
        }
      ]
    })

    paypal.payment.create(payReq, (error, payment) => {
      const links = {}
      if (error) {
        console.error(error)
        res.status('500').end()
      } else {
        payment.links.forEach(linkObj => {
          links[linkObj.rel] = {
            href: linkObj.href,
            method: linkObj.method
          }
        })

        if (links.hasOwnProperty('approval_url')) {
          console.info(links.approval_url.href)
          res.redirect(302, links.approval_url.href)
        } else {
          console.error('no redirect URI present')
          res.status('500').end()
        }
      }
    })
  })

  app.get('/5', (req, res) => {
    // res.json(req.headers['x-forwarded-host'])
    res.json('hello')
  })

  app.get('/payment', (req, res) => {
    res.send('Please wait...')
  })

  app.get('/process', async (req, res) => {
    let paypal = await initPaypal()

    const paymentId = req.query.paymentId
    const payerId = {
      payer_id: req.query.PayerID
    }
    return paypal.payment.execute(paymentId, payerId, (error, payment) => {
      if (error) {
        console.error(error)
        res.redirect(`/payment?status=failed`)
      } else {
        let amount = payment.transactions[0].amount.total - 0
        let uid = payment.transactions[0].custom

        if (payment.state === 'approved') {
          let balanceRef = admin
            .database()
            .ref(`settings/${uid}/accountInfo/balance`)
          let transactionsRef = admin
            .database()
            .ref(`settings/${uid}/transactions`)
          let latestRef = admin
            .database()
            .ref(`master-config/today/clientCredits`)
          balanceRef.transaction(function(balance) {
            return balance - 0 + amount
          })
          latestRef.transaction(function(clientCredits) {
            return clientCredits - 0 + amount
          })
          transactionsRef.push({
            type: 'funds_added',
            date: now(),
            amount: amount,
            from: 'Paypal'
          })
          console.info(
            `payment completed successfully, description: ${uid} ${amount} `
          )

          res.redirect(
            `/payment?status=success&amount=` +
              payment.transactions[0].amount.total
          )
        } else {
          console.warn(`payment.state: not approved ? ${uid} ${amount} `)
          res.redirect(`/payment?status=failed`)
        }
      }
    })
  })
}
