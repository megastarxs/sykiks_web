import React from 'react'
import { Table, Button } from 'reactstrap'
import { FormattedNumber } from 'react-intl'
import ProfileModal from 'components/ProfileModal'
import Subheader from 'components/Subheader'
import DefaultMessage from 'components/DefaultMessage'
import enhance from './enhancer'

function Users({
  users,
  setSearch,
  search,
  updateProfileModal,
  closeModal,
  profileId,
  percentageCut,
  referenceId,
  paidUsers,
  paid,
  open
}) {
  percentageCut = percentageCut - 0
  let feesPercent = percentageCut / 100
  let paidPercent = 1 - percentageCut / 100

  if (!users)
    return (
      <div className="container">
        <Subheader subheader="Pending payouts" />
        <DefaultMessage>No Users Listed Here.</DefaultMessage>}
      </div>
    )

  users = Object.keys(users)
    .map((key, index) => {
      users[key].key = key
      return users[key]
    })
    .filter(d => d.accountInfo && d.accountInfo.balance && !paidUsers[d.key])
    .sort((a, b) => b.accountInfo.balance - a.accountInfo.balance)

  let total = { balance: 0, fees: 0, toBePaid: 0 }
  users.forEach(d => {
    d.balance = d.accountInfo.balance
    d.paypalId = d.accountInfo.paypalId
    d.fees = d.balance * feesPercent
    d.toBePaid = d.balance * paidPercent

    total.balance += d.balance
    total.fees += d.fees
    total.toBePaid += d.toBePaid
  })

  if (search != '')
    users = users.filter(d =>
      d.screenName.toLowerCase().includes(search.toLowerCase())
    )

  return (
    <div className="container">
      <Subheader subheader="Pending payouts" />
      {users.length == 0 && (
        <DefaultMessage>No Users Listed Here.</DefaultMessage>
      )}
      {users.length != 0 && (
        <div>
          <div className="row my-3">
            <div className="col form-inline">
              <label className="mr-3"> Reference Id:</label>
              <input
                className="form-control col-md-6"
                type="search"
                placeholder="Search"
                readOnly
                value={referenceId}
              />
            </div>
            <div className="col">
              <input
                className="form-control col-md-6 float-right"
                type="search"
                placeholder="Search"
                onChange={setSearch}
              />
            </div>
          </div>

          <Table responsive bordered hover className="text-center">
            <thead>
              <tr>
                <th>Name</th>
                <th>Paypal Email</th>
                <th>Earned</th>
                <th>Fees</th>
                <th>To be paid</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {users.map(user => {
                return (
                  <tr key={user.key}>
                    <td>
                      <Button
                        color="link"
                        onClick={() => updateProfileModal(user.key)}>
                        {user.screenName}
                      </Button>
                    </td>
                    <td>
                      <a href={'mailto:' + user.email}>{user.paypalId}</a>
                    </td>
                    <td>
                      <FormattedNumber
                        style="currency"
                        currency="USD"
                        value={user.balance}
                      />
                    </td>
                    <td className="currency">
                      <FormattedNumber
                        style="currency"
                        currency="USD"
                        value={user.fees}
                      />
                    </td>
                    <td>
                      <FormattedNumber
                        style="currency"
                        currency="USD"
                        value={user.toBePaid}
                      />
                    </td>
                    <td>
                      <button
                        className="btn btn-lg btn-primary"
                        onClick={() => {
                          let {
                            key,
                            screenName,
                            paypalId,
                            uid,
                            balance,
                            fees
                          } = user
                          paid({
                            key,
                            screenName,
                            paypalId,
                            balance,
                            fees,
                            uid
                          })
                        }}>
                        Paid
                      </button>
                    </td>
                  </tr>
                )
              })}
              <tr>
                <td colSpan="6" />
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th> Total </th>
                <th />
                <th>
                  <FormattedNumber
                    style="currency"
                    currency="USD"
                    value={total.balance}
                  />
                </th>
                <th className="currency">
                  <FormattedNumber
                    style="currency"
                    currency="USD"
                    value={total.fees}
                  />
                </th>
                <th>
                  <FormattedNumber
                    style="currency"
                    currency="USD"
                    value={total.toBePaid}
                  />
                </th>
                <th />
              </tr>
            </tfoot>
          </Table>
        </div>
      )}
      <ProfileModal
        key={profileId}
        uid={profileId}
        open={open}
        onClose={() => closeModal()}
      />
    </div>
  )
}

export default enhance(Users)
