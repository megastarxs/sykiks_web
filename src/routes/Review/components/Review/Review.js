import React from 'react'
import './Review.scss'
import Subheader from 'components/Subheader'
import { AvatarByUid } from 'components/Avatar'
import ScreenName from 'components/ScreenName'
import Icon from 'components/Icon'

export const Review = ({
  router: {
    params: { id }
  },
  changeText,
  sendReview,
  rate,
  text,
  setRating
}) => (
  <div className="container text-center review-div">
    <Subheader subheader="Review" />
    <div>
      <AvatarByUid uid={id} className="img-fluid avt-image" />
    </div>
    <div className="screen-name">
      Leave a review for your session with <ScreenName uid={id} />
    </div>
    <div className="review-icons">
      <button onClick={() => setRating('up')}>
        <Icon
          name="md-thumbs-up"
          className={`thumb-up ${rate == 'up' ? 'active' : ''}`}
        />
      </button>
      <button onClick={() => setRating('down')}>
        <Icon
          name="md-thumbs-down"
          className={`thumb-down ${rate == 'down' ? 'active' : ''}`}
        />
      </button>
    </div>
    <div className="text-send">
      <textarea
        type="text"
        placeholder="Write your review here..."
        rows={3}
        onChange={changeText}
        value={text}
      />
    </div>
    <div>
      <button className="btn purple-button" onClick={sendReview}>
        POST REVIEW
      </button>
    </div>
  </div>
)

export default Review
