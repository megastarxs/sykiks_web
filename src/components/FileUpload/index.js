import React from 'react'
import Cropper from 'react-cropper'
import './FileUpload.scss'
import './cropper.scss'
import { Modal, ModalBody } from 'reactstrap'
import enhancer from './enhancer'

let FileUpload = ({
  children,
  handleChange,
  image,
  open,
  setRef,
  done,
  progress,
  close
}) => (
  <div className="image-upload">
    <label htmlFor="file-input">{children}</label>
    <input
      id="file-input"
      className="file-input"
      type="file"
      accept="image/*"
      onChange={handleChange}
    />
    <Modal isOpen={open}>
      <ModalBody className="text-center cropper-box">
        <h5>Edit Image</h5>
        <Cropper
          ref={setRef}
          src={image}
          style={{ width: 200, height: 200, objectFit: 'contain' }}
          className="img-fluid ml-auto mr-auto mt-3 mb-3"
          aspectRatio={1}
          guides={false}
          background={false}
          viewMode={2}
        />
        <button className="purple-button btn btn-lg " onClick={done}>
          Save
        </button>{' '}
        <button className="light-purple-button btn btn-lg " onClick={close}>
          Cancel
        </button>
      </ModalBody>
    </Modal>
  </div>
)

export default enhancer(FileUpload)
