import * as admin from 'firebase-admin'
import { now, getVal } from '../helpers'
import _ from 'lodash'

export default app => {
  app.get('/admin-dashboard', async (req, res) => {
    let usersRef = admin.database().ref('users')
    let settingsRef = admin.database().ref('settings')
    let masterConfigRef = admin.database().ref(`master-config`)
    let highestPaidId,
      usersList,
      todaysDetails,
      todaysPsychicEarning,
      todaysClientCredits
    let highestPaidAmt = 0
    var d = new Date()
    d.setDate(d.getDate() - 1)
    let utc = d
      .toJSON()
      .slice(0, 10)
      .replace(/-/g, '-')

    // Get all Users
    usersList = await getVal(usersRef)
    let balances = {}
    let usersListPromises = Object.keys(usersList).map(key =>
      getVal(settingsRef.child(`${key}/accountInfo/balance`)).then(
        bal => (balances[key] = bal)
      )
    )
    await Promise.all(usersListPromises)
    {
      Object.keys(usersList).forEach(key => {
        let profile = usersList[key]
        let balance = balances[key]
        if (_.isNumber(balance)) {
          if (profile.type == 'psychic') {
            if (highestPaidAmt < balance) {
              highestPaidAmt = balance
              highestPaidId = key
            }
          }
        }
      })

      todaysDetails = await getVal(admin.database().ref(`master-config/today`))
      todaysPsychicEarning = todaysDetails.psychicEarning
      todaysClientCredits = todaysDetails.clientCredits

      let adminReportPsychicEarning = todaysPsychicEarning
      let adminReportClientCredits = todaysClientCredits
      let adminReportSykiksEarning = 0
      if (adminReportPsychicEarning > 0) {
        let Earning = (adminReportPsychicEarning / 100) * 40
        adminReportSykiksEarning = Earning
      }

      let adminReportHighestPaidId = highestPaidId
      // Update last date
      // masterConfigRef.push().key
      masterConfigRef.update({ lastReportId: utc, lastAdminUpdated: now() })

      // Add data in admin-reports
      admin
        .database()
        .ref(`adminReports/${utc}`)
        .update({
          psychicEarning: adminReportPsychicEarning,
          clientCredits: adminReportClientCredits,
          SykiksEarning: adminReportSykiksEarning,
          HighPaidId: adminReportHighestPaidId,
          lastAdminUpdated: now()
        })
      masterConfigRef.child('today').update({
        clientCredits: 0,
        psychicEarning: 0
      })
      return res.json({ hello: true })
    }
  })
}
