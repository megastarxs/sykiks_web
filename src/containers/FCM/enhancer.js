/* eslint-disable */
import { compose } from 'redux'
import { connect } from 'react-redux'
import { lifecycle } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import firebase from 'firebase/app'
import { withRouter } from 'utils/components'
import { now } from 'utils/helpers'


// window.Iamawake = ()=>{
//   if (firebase.auth().currentUser !== null)
//     {
//       let { uid } = firebase.auth().currentUser
//       firebase.database().ref(`users/${uid}/online`).update({ desktop: now})
//     }
// }


let messaging = false
let getToken
try {
  messaging = firebase.messaging()
  getToken = auth =>
    messaging.getToken().then(currentToken => {
      if (currentToken) {
        firebase.ref(`settings/${auth.uid}/tokens/fcm`).set(currentToken)
      }
    })
} catch (error) {}

export default compose(
  firebaseConnect(({}, firebase) => [
    {
      path: `settings/${firebase.firebase._.authUid}/tokens/fcm`,
      storeAs: 'fcm'
    }
  ]),
  withRouter,
  connect(({ firebase: { auth, data: { fcm } } }) => ({ fcm, auth })),
  lifecycle({
    componentDidMount() {
      let { auth, fcm, router } = this.props
      let { desktop_token }=  router.location.query
      if(desktop_token)
        firebase.ref(`settings/${auth.uid}/tokens/desktop`).set(desktop_token)
      

      var myConnectionsRef = firebase.database().ref(`users/${this.props.auth.uid}/online`)
      var connectedRef = firebase.database().ref('.info/connected')
      connectedRef.on('value', function (snap) {
        if (snap.val()) {
          var con = myConnectionsRef.push()
          con.onDisconnect().remove()
          con.set(true)
        }
      })

      if (!messaging) return
      if (!fcm)
        messaging.onTokenRefresh(function() {
          getToken(auth)
        })

      messaging
        .requestPermission()
        .then(function() {
          getToken(auth)
        })
        .catch(function() {
          // console.log('Unable to get permission to notify.')
        })
      

    }
  })
)
