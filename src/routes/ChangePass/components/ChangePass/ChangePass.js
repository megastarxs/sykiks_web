import React from 'react'
import './ChangePass.scss'
import Form from './ChangePassForm'
import Subheader from 'components/Subheader'

const ChangePass = ({ submit }) => (
  <div className="container">
    <Subheader subheader="Change Password" />
    <div className="change-pass">
      <div>
        <Form onSubmit={submit} />
      </div>
    </div>
  </div>
)

export default ChangePass
