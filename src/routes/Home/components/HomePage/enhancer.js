import { compose } from 'redux'
import { connect } from 'react-redux'
import { withProps } from 'recompose'
import { withFirebase, isEmpty, isLoaded } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { withNotifications } from 'modules/notification'

export default compose(
  withFirebase,
  withNotifications,
  connect(({ config, firebase: { profile, auth } }) => ({
    profile,
    auth,
    isDesktop: config.desktop
  })),
  withProps(({ auth, profile }) => ({
    loggedIn: isLoaded(auth) && !isEmpty(auth)
  })),
  spinnerWhileLoading(['profile'])
)
