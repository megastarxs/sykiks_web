import { connect } from 'react-redux'
import { withFirebase } from 'react-redux-firebase'
import { compose, withStateHandlers } from 'recompose'

export default compose(
  // create listener for admin, results go into redux
  withFirebase,
  // map redux state to props
  connect(({ firebase: { auth } }) => ({
    auth
  })),
  withStateHandlers(
    {
      filters: {
        available: false,
        favorites: false,
        sortBy: 'Top Psychics',
        specialities: ''
      }
    },
    {
      updateFilter: ({ filters }) => update => ({
        filters: { ...filters, ...update }
      })
    }
  )
)
