import _ from 'lodash'
import firebase from 'firebase/app'

export let now = firebase.database.ServerValue.TIMESTAMP

export let smallText = text => _.truncate(text, { length: 100, separator: ' ' })

export const ComputeProfile = profile => {
  let ratingUp = profile.rating_up || 0
  let ratingDown = profile.rating_down || 0
  let ratingCount = ratingUp + ratingDown || 1

  return {
    ratingUp,
    ratingDown,
    ratingCount, // eslint-disable-next-line
    percentage: (ratingUp * 100) / ratingCount
  }
}

export let updateImage = (
  firebase,
  uid,
  file,
  showSuccess,
  showError,
  updateProgress
) => {
  if (updateProgress) updateProgress(1)
  var uploadTask = firebase
    .storage()
    .ref()
    .child(`avatars/${uid}.png`)
    .putString(file, 'data_url')
  uploadTask.on(
    'state_changed',
    function(snapshot) {
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
      if (progress == 0) progress = 1
      if (updateProgress) updateProgress(progress)
    },
    function() {
      showError('Sorry something went wrong')
    },
    function() {
      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
        if (updateProgress) {
          let avatarRef = firebase.ref(`users/${uid}/avatar`)
          let oldUrl = false
          avatarRef.on('value', s => {
            if (oldUrl) {
              setTimeout(() => {
                updateProgress(false)
              }, 1000)
            } else oldUrl = s.val()
          })
        }
      })
    }
  )
}

export let uploadAttachment = (file, fileName, updateProgress, callback) => {
  updateProgress(1)
  var uploadTask = firebase
    .storage()
    .ref()
    .child(fileName)
    .put(file)

  uploadTask.on(
    'state_changed',
    function(snapshot) {
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
      if (progress == 0) progress = 1
      updateProgress(progress)
    },
    function() {
      // showError('Sorry something went wrong')
    },
    function() {
      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
        updateProgress(false)
        callback(downloadURL)
      })
    }
  )
}
