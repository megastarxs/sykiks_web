import React from 'react'
import Icon from 'components/Icon'

export default ({ children }) => (
  <div className="container">
    <h4 className="text-center not-verified">
      <Icon name="md-information-circle" />
      {children}
    </h4>
  </div>
)
