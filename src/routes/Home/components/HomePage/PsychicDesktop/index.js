import React from 'react'
import enhancer from './enhancer'
import { AvatarByUid } from 'components/Avatar'
import DataMissingAlert from 'components/DataMissingAlert'
import { FormattedNumber, FormattedDate } from 'react-intl'
import './Psychic.scss'
let Psychic = ({ auth, accountInfo, profile }) => {
  return (
    <div className="container dashboard">
      <div className="col-md-8 col-lg-8 col-sm-12 offset-md-2 offset-lg-2">
        <div className="row dashmini">
          <DataMissingAlert profile={profile} accountInfo={accountInfo} />
          <div className="col-md-12">
            <AvatarByUid
              divClassName="col-lg-12 dashpic"
              uid={auth.uid}
              className="avt-image"
            />
          </div>
        </div>
      </div>
      <div className="col-md-8 col-lg-8 col-sm-12 offset-md-2 offset-lg-2">
        <div className="row">
          <div className="card-block col-md-4">
            <p className="title">Balance</p>
            <p className="green-value">
              <FormattedNumber
                style="currency"
                currency="USD"
                value={(accountInfo && accountInfo.balance - 0) || 0}
              />
            </p>
          </div>
          <div className="card-block col-md-4">
            <p className="title">Last Session</p>
            <p className="green-value">
              <FormattedNumber
                style="currency"
                currency="USD"
                value={(accountInfo && accountInfo.lastSession - 0) || 0}
              />
            </p>
          </div>
          <div className="card-block col-md-4">
            <p className="title">Next Payout</p>
            <p className="blue-value">
              {accountInfo &&
                accountInfo.nextPayoutDate && (
                  <FormattedDate
                    value={accountInfo.nextPayoutDate}
                    year="numeric"
                    month="short"
                    day="2-digit"
                  />
                )}
              {!accountInfo || !accountInfo.nextPayoutDate ? 'Soon' : ''}
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default enhancer(Psychic)
