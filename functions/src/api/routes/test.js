/* eslint-disable */
import * as admin from 'firebase-admin'
import { getVal } from '../helpers'


export default app => {
    app.get('/fix', async (req, res) => {

        let usersRef = admin.database().ref('users')

        let psychics = await getVal(
            usersRef.orderByChild('type').equalTo('psychic')
        )

        Object.keys(psychics).forEach(uid=>{
            usersRef.child(uid).update({uid})
        })
        
        
        res.json('done')
    })
}

