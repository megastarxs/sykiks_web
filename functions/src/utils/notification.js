import Expo from 'expo-server-sdk'
import * as admin from 'firebase-admin'

let expo = new Expo()

let expoNotify = (pushToken, notification) => {
  let messages = []

  if (!Expo.isExpoPushToken(pushToken)) {
    console.error(`Push token ${pushToken} is not a valid Expo push token`)
  }
  messages.push({
    to: pushToken,
    sound: 'default',
    priority: 'high',
    ...notification
  })

  let chunks = expo.chunkPushNotifications(messages)

  for (let chunk of chunks) {
    expo
      .sendPushNotificationsAsync(chunk)
      .then(receipts => {})
      .catch(error => {
        console.error(error)
      })
  }
}

let notify = async (uid, notification) => {
  let host = 'https://sykiks.com/'
  let icon = `https://sykiks-mvp.firebaseapp.com/22a85c22329ab4bc41eb29ba1857a29e.png`
  notification.click_action = host + notification.click_action
  notification.icon = icon
  if (notification.data) notification.data = JSON.stringify(notification.data)

  const payload = { notification }

  let tokens = await admin
    .database()
    .ref(`settings/${uid}/tokens`)
    .once('value')
  tokens = tokens.val()

  if (!tokens) return null
  console.log(tokens)
  if (tokens.expo) expoNotify(tokens.expo, notification)

  let sendTokens = []

  if (tokens.fcm) sendTokens.push(tokens.fcm)
  if (tokens.desktop) sendTokens.push(tokens.desktop)

  if (sendTokens[0])
    return admin
      .messaging()
      .sendToDevice(sendTokens, payload)
      .then(response => {})
}

export default notify
