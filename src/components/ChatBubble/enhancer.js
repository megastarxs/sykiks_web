import { compose } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import { connect } from 'react-redux'
import { ProfileModalHandler } from 'components/ProfileModal'

export default compose(
  firebaseConnect(({ uid }) => [
    { path: `.info/serverTimeOffset`, storeAs: `serverTimeOffset` }
  ]),
  ProfileModalHandler,
  connect(({ firebase: { data, auth, profile } }) => ({
    serverTimeOffset: data.serverTimeOffset,
    auth,
    profile
  }))
)
