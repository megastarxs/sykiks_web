import MasterConfig from './MasterConfig'
import enhance from './MasterConfig.enhancer'

export default enhance(MasterConfig)
