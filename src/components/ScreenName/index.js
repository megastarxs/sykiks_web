import React, { Fragment } from 'react'
import enhance from './enhancer'

const FromScreenName = ({ screenName }) =>
  screenName ? <Fragment>{screenName}</Fragment> : null

export default enhance(FromScreenName)
