import { compose } from 'redux'
import { connect } from 'react-redux'
import { withProps, withHandlers } from 'recompose'
import { withFirebase, isEmpty, isLoaded } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { withNotifications } from 'modules/notification'

export default compose(
  withFirebase,
  connect(({ firebase: { profile, auth } }) => ({
    profile,
    auth
  })),
  spinnerWhileLoading(['profile']),
  withNotifications,
  withHandlers({
    verifyEmail: ({ showSuccess, firebase }) => () => {
      firebase
        .auth()
        .currentUser.sendEmailVerification({ url: location.origin })
        .then(function() {
          showSuccess('Email sent. Please check your email')
        })
    }
  }),
  withProps(({ auth, profile }) => ({
    loggedIn: isLoaded(auth) && !isEmpty(auth)
  }))
)
