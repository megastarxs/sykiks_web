import ForgotPass from './ForgotPass'
import enhance from './ForgotPass.enhancer'

export default enhance(ForgotPass)
