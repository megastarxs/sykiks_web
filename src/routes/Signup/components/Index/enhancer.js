import { withFirebase } from 'react-redux-firebase'
import { withHandlers, pure, compose, withStateHandlers } from 'recompose'
import { UserIsNotAuthenticated } from 'utils/router'
import { withNotifications } from 'modules/notification'

export default compose(
  UserIsNotAuthenticated, // redirect to list page if logged in
  withNotifications, // add props.showError
  withFirebase, // add props.firebase (firebaseConnect() can also be used)
  // Handlers
  withStateHandlers(
    { file: false, captcha: false },
    {
      updateImage: () => file => ({
        file
      }),
      captchaVerified: () => () => ({
        captcha: true
      })
    }
  ),
  withHandlers({
    onSubmitFail: props => (formErrs, dispatch, err) =>
      props.showError(
        formErrs
          ? 'Please check the details you entered'
          : err.message || 'Error'
      ),
    emailSignup: ({
      firebase,
      showError,
      showSuccess,
      router,
      captcha,
      file
    }) => form => {
      let type = router.params.type
      if (!captcha) return showError('Please check the reCaptcha')
      if (form.login.password != form.extra.confirmPassword)
        return showError('Password and Confirm Password do not match')
      form.profile.createdAt = firebase.database.ServerValue.TIMESTAMP
      if (type == 'psychic') {
        if (!form.profile.categories)
          return showError('Please select some categories')
        form.profile.status = 'Available'
        form.profile.rating_down = 0
        form.profile.rating_up = 0
        form.profile.chatRate = 0
        form.profile.msgRate = 0
        form.profile.rating_up = 0
      }
      form.profile.type = type
      form.profile.email = form.login.email
      firebase
        .createUser(form.login, form.profile)
        .then(auth => {
          let uid = firebase.auth().currentUser.uid
          firebase
            .auth()
            .currentUser.sendEmailVerification({ url: location.origin })
            .then(function() {
              showSuccess('Email sent. Please check your email')
            })

          if (file) {
            let obj = {}
            obj[uid] = file
            firebase.ref(`images`).update(obj)
          }
        })
        .catch(err => showError(err.message))
    }
  }),
  pure // shallow equals comparison on props (prevent unessesary re-renders)
)
