import React from 'react'
import enhancer from './enhancer'
import { AvatarByUid } from 'components/Avatar'
import DataMissingAlert from 'components/DataMissingAlert'
import Reviews from 'components/Reviews'
import { FormattedNumber, FormattedDate } from 'react-intl'
import Subheader from 'components/Subheader'
import Icon from 'components/Icon'
import { Link } from 'react-router'
import downloadImg from 'static/images/sykiks-dwnld.jpg'
import './Psychic.scss'
let Psychic = ({ auth, accountInfo, profile }) => {
  return (
    <div className="container dashboard">
      <Subheader subheader="My Dashboard" />
      <div className=" col-lg-12 col-sm-12">
        <div className="row dashmini">
          <DataMissingAlert profile={profile} accountInfo={accountInfo} />
          <div className="col-lg-5 col-md-6">
            <AvatarByUid
              divClassName="col-lg-12 dashpic"
              uid={auth.uid}
              className="avt-image"
            />
            <div className="card-block px-2">
              <p className="card-title">{profile.screenName}</p>
              <p className="card-text">
                Member Since:{' '}
                <FormattedDate
                  value={profile.createdAt}
                  year="numeric"
                  month="long"
                  day="2-digit"
                />{' '}
              </p>
              <div>
                <Link to="/account" className="btn button btn-sm edit">
                  <Icon name="ios-create" />Edit Profile
                </Link>
                <Link to={`profile/${auth.uid}`} className="btn btn-sm button">
                  <Icon name="ios-person" />View Profile
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-7 col-md-6 psych-how">
            <div className="dwnl">
              <Link to={`download`} className="link">
                <img src={downloadImg} />
              </Link>
            </div>
            <h5 style={{ fontSize: '1.2em' }}>
              How to use our platform as a Psychic:
            </h5>
            <h5>
              <i className="icon ion-ios-chatboxes" />Set Your Rates
            </h5>
            <p>
              Before you begin chatting with paying clients,
              <Link to="/account">&nbsp;set your rate</Link>&nbsp;per minute and
              optional per message charge. Per message rates apply when a client
              sends a message to you but not when you send a message to a
              client.
            </p>
            <h5>
              <i className="icon ion-ios-wallet" />Input Payment Information
            </h5>
            <p>
              In order for us to pay you, fill out your PayPal email ID on the
              <Link to="/account">&nbsp;edit account page</Link>
              &nbsp;and please make sure it is correct.&nbsp;We payout a{' '}
              <b>60%</b>
              &nbsp;commission of your balance every <b>2 weeks</b>.
            </p>
            <h5>
              <i className="icon ion-ios-wifi" />Setting Your Status
            </h5>
            <p>
              <b style={{ color: '#7daf3d' }}>Online - Chat Now</b>
              <br />
              To receive client calls set your status to <b>"Available"</b> in
              the top navigation bar. Google Chrome and Firefox will notify you
              of new calls even when sykiks.com is not opened. All other
              browsers must keep sykiks.com opened and in focus to receive
              calls.<br />
              <b style={{ color: '#ed9d2b' }}>Away: Auto Status</b>
              <br />
              Your status will automatically be set to "Away" when you've logged
              out of sykiks.com or have closed your browser.<br />
              <b style={{ color: '#764095' }}>Busy: Auto Status</b>
              <br />
              When you are engaging in a pay-per-minute chat with a client, your
              status will automatically display to others as 'Busy'.<br />
              <b>Offline</b>
              <br />
              When you no longer wish to receive notifications for calls or
              accept calls, change your status to 'Offline'.
              <br />
            </p>
          </div>
        </div>
      </div>
      <div className="col-md-8 col-lg-8 col-sm-12 offset-md-2 offset-lg-2">
        <div className="row">
          <div className="card-block col-md-4">
            <p className="title">Balance</p>
            <p className="green-value">
              <FormattedNumber
                style="currency"
                currency="USD"
                value={(accountInfo && accountInfo.balance - 0) || 0}
              />
            </p>
          </div>
          <div className="card-block col-md-4">
            <p className="title">Last Session</p>
            <p className="green-value">
              <FormattedNumber
                style="currency"
                currency="USD"
                value={(accountInfo && accountInfo.lastSession - 0) || 0}
              />
            </p>
          </div>
          <div className="card-block col-md-4">
            <p className="title">Next Payout</p>
            <p className="blue-value">
              {accountInfo &&
                accountInfo.nextPayoutDate && (
                  <FormattedDate
                    value={accountInfo.nextPayoutDate}
                    year="numeric"
                    month="short"
                    day="2-digit"
                  />
                )}
              {!accountInfo || !accountInfo.nextPayoutDate ? 'Soon' : ''}
            </p>
          </div>
        </div>
      </div>
      <h4 className="review-title">Latest Reviews</h4>
      <div className="col-md-8 col-lg-8 col-sm-12 offset-md-2 offset-lg-2">
        <div className="card review-card">
          <Reviews uid={auth.uid} />
        </div>
      </div>
    </div>
  )
}

export default enhancer(Psychic)
