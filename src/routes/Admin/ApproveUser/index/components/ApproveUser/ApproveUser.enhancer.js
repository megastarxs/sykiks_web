import { compose, withHandlers } from 'recompose'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated, UserIsAdmin } from 'utils/router'
import { withNotifications } from 'modules/notification'

export default compose(
  // create listener for admin, results go into redux
  UserIsAuthenticated,
  UserIsAdmin,
  firebaseConnect(() => [
    {
      path: 'users',
      queryParams: ['orderByChild=status', 'equalTo=pending'],
      storeAs: `psychics`
    }
  ]),
  withNotifications,
  // map redux state to props
  connect(({ firebase: { data } }) => ({
    psychics: data[`psychics`]
  })),
  withHandlers({
    approveUser: ({ firebase, showSuccess }) => uid => {
      firebase
        .ref(`users/${uid}`)
        .update({ status: 'Available' })
        .then(user => showSuccess('User has been approved'))
    },
    rejectUser: ({ firebase, showSuccess }) => uid => {
      firebase
        .ref(`users/${uid}`)
        .update({ status: 'Rejected' })
        .then(showSuccess('User has been rejected'))
    }
  }),
  spinnerWhileLoading(['psychics'])
)
