import PrivacyPolicy from './content/privacy_policy'
import TermsOfService from './content/terms_of_service'
import Disclaimer from './content/disclaimer'
import RefundPolicy from './content/refund_policy'
import About from './content/about'
import HowItWorks from './content/how_it_works'

const config = {
  privacy_policy: {
    title: 'Privacy Policy',
    content: PrivacyPolicy
  },
  terms_of_service: {
    title: 'Terms of service',
    content: TermsOfService
  },
  disclaimer: {
    title: 'Disclaimer',
    content: Disclaimer
  },
  refund_policy: {
    title: 'Refund Policy',
    content: RefundPolicy
  },
  about: {
    title: 'About',
    content: About
  },
  how_it_works: {
    title: 'How It Works',
    content: HowItWorks
  }
}

export default config
