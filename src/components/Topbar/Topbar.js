import React from 'react'
import logoImage from 'static/header1.png'
import transparentLogo from 'static/header2.png'
import './Topbar.scss'
import { Link } from 'react-router'

export const Topbar = ({ transparent }) => (
  <div className="container no-desktop">
    <div className="topbar">
      <nav className="navbar navbar-expand-sm navbar-light">
        <div className="navbar-collapse">
          <Link className="navbar-brand" to="/">
            <img src={transparent ? transparentLogo : logoImage} width="134" />
          </Link>
          <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
            <li className="nav-item">
              <Link
                to="/pages/how_it_works"
                className="nav-link how-works"
                activeClassName="active">
                How It Works
              </Link>
            </li>
            <li className="nav-item">
              <Link
                to="/psychics"
                className="nav-link psychics"
                activeClassName="active">
                Psychics
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>
)

export default Topbar
