import React from 'react'
import { Alert } from 'reactstrap'

export const Payment = ({ location }) => (
  <div className="container">
    {location.query.status == 'success' ? (
      <Alert color="success">
        Thanks for for using sykiks. Credits have been added to your account.
      </Alert>
    ) : (
      <Alert color="danger">We were not able to receive your payment.</Alert>
    )}
  </div>
)

export default Payment
