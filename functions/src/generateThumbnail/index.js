import * as admin from 'firebase-admin'
const functions = require('firebase-functions')

const spawn = require('child-process-promise').spawn
const path = require('path')
const os = require('os')
const fs = require('fs')

let projectConfig = require(process.cwd() + '/account/service.json')
let projectId = projectConfig.project_id

let keyFilename = 'account/service.json'

const gcs = require('@google-cloud/storage')({
  projectId,
  keyFilename
})

try {
  admin.initializeApp()
} catch (e) {}

/**
 * When an image is uploaded in the Storage bucket We generate a thumbnail automatically using
 * ImageMagick.
 */

export default functions.storage.object().onFinalize(generateThumbnailEvent)

let avatarUrl = uid => {
  let usersRef = admin.database().ref('users')
  let { key } = usersRef.push()
  let avatar = `http://storage.googleapis.com/${projectId}.appspot.com/avatars/${uid}.png?alt=media&ver=${key}`
  usersRef.child(uid).update({ avatar })
}

async function generateThumbnailEvent(object) {
  const fileBucket = object.bucket
  const filePath = object.name
  const contentType = object.contentType

  if (!contentType.startsWith('image/')) {
    console.log('This is not an image.')
    return null
  }

  const fileName = path.basename(filePath)
  let uid = fileName.replace('.png', '')

  if (fileName.endsWith('.thumb')) {
    console.log('Already a Thumbnail.')
    return null
  }

  const bucket = gcs.bucket(fileBucket)
  const tempFilePath = path.join(os.tmpdir(), fileName)
  const metadata = {
    contentType: contentType
  }
  return bucket
    .file(filePath)
    .download({
      destination: tempFilePath
    })
    .then(() => {
      console.log('Image downloaded locally to', tempFilePath)

      return spawn('convert', [
        tempFilePath,
        '-thumbnail',
        '200x200>',
        tempFilePath
      ])
    })
    .then(() => {
      console.log('Thumbnail created at', tempFilePath)

      const thumbFileName = `${fileName}.thumb`
      const thumbFilePath = path.join(path.dirname(filePath), thumbFileName)

      return bucket.upload(tempFilePath, {
        destination: thumbFilePath,
        public: true,
        metadata: metadata
      })
    })
    .then(() => {
      fs.unlinkSync(tempFilePath)
      avatarUrl(uid)
    })
}
