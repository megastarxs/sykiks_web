import React from 'react'
import './content.scss'
export default (
  <div className="page-content">
    <p>
      Thank you for taking time to read through our terms and conditions. Please
      note that “on the site” “our” “us” “we” “our platform” “our website” are
      all terms used to describe and refer to <b>sykiks.com</b>
    </p>
    <ol>
      <li>
        <p>
          You understand that during your chat session, it is your
          responsibility as a client to verify any information provided to you
          by your chosen advisor.
        </p>
      </li>
      <li>
        <p>
          As a member you agree to being solely responsible for all of the
          content that you submit from your account and are responsible for the
          full accuracy, and validity of information that you provide during
          your interactions with advisors on the site.{' '}
        </p>
      </li>

      <li>
        <p>
          You agree that you shall not provide any advisor with personal contact
          information such as your address or telephone number, or any other
          information which is of a similar nature.
        </p>
      </li>
      <li>
        <p>
          You understand that the creation of multiple accounts per singular
          user is strictly prohibited, this applies to clients and advisors.
        </p>
      </li>
      <li>
        <p>
          As a member you must not directly provide the advisor with any payment
          information such as credit card details or paypal addresses. All
          payments must go through the legitimate payment channels/gateways that{' '}
          <b>sykiks.com</b> have provided.
        </p>
      </li>

      <li>
        <p>
          You understand that you must be fully authorised as the account holder
          of your credit card/debit card and or paypal account when purchasing
          any of our advisors services.
        </p>
      </li>
      <li>
        <p>
          You must ensure that all payment details that you provide upon
          purchasing services from advisors on <b>sykiks.com</b> are accurate
          and up to date.
        </p>
      </li>
      <li>
        <p>
          You agree that you will be subject to your chosen payment gateways
          terms and conditions, as well as the terms and conditions of{' '}
          <b>sykiks.com</b>.
        </p>
      </li>
      <li>
        <p>
          <b>sykiks.com</b> informs you that requesting charge backs and seeking
          refunds is a breach of our terms and conditions and can be classed as
          fraudulent activity.{' '}
        </p>
      </li>
      <li>
        <p>
          You understand that all payment transactions are non refundable and{' '}
          <b>sykiks.com</b> will not be liable for unsatisfactory services.
        </p>
      </li>
      <li>
        <p>
          You understand that this agreement applies to all users and members of
          the Site including all third party independent advisors.{' '}
        </p>
      </li>
      <li>
        <p>
          <b>sykiks.com</b> may make changes to this agreement on occasion
          without notifying you directly.
        </p>
      </li>
      <li>
        <p>
          You understand that your continued use of the Services on{' '}
          <b>sykiks.com</b> will automatically mean that you have accepted,
          agreed and kept up to date with any changes made to the terms and
          conditions/agreements and privacy policy on our site.{' '}
        </p>
      </li>
      <li>
        <p>
          If you do not accept updated versions of our services, terms,
          conditions and policies then <b>sykiks.com</b> shall not bear any
          responsibility or liability for your decision to continue using the
          services provided on its site.{' '}
        </p>
      </li>
      <li>
        <p>
          You understand that your right to access <b>sykiks.com</b> will be
          revoked if you do not adhere to our terms and conditions . Any
          misconduct may lead to permenant suspension from our site and its
          services.
        </p>
      </li>
      <li>
        <p>
          You agree and are aware of the fact that <b>sykiks.com</b> may end
          your membership and restrict you from entering the Site and its
          Services with or without prior notice with no reason or explanation.{' '}
        </p>
      </li>
    </ol>
  </div>
)
