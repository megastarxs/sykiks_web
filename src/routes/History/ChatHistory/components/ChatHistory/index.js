import ChatHistory from './ChatHistory'
import enhance from './ChatHistory.enhancer'

export default enhance(ChatHistory)
