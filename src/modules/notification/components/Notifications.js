import React from 'react'
import { size } from 'lodash'
import { connect } from 'react-redux'
import { pure, compose, renderNothing, branch } from 'recompose'
import { Alert } from 'reactstrap'
import './Notifications.scss'
import * as actions from '../actions'

export const Notifications = ({ allIds, byId, dismissNotification }) => (
  <div>
    {allIds.map(id => (
      <Alert
        key={id}
        color={byId[id].type}
        isOpen
        toggle={() => dismissNotification(id)}
        className="col-md-4 col-lg-4 col-sm-6 offset-md-4 offset-lg-4">
        {byId[id].message}
      </Alert>
    ))}
  </div>
)

/* eslint-disable */
export default compose(
  pure,
  connect(
    ({ notifications: { allIds, byId } }) => ({ allIds, byId }),
    actions
  ),
  branch(props => !size(props.allIds), renderNothing) // only render if notifications exist
)(Notifications)

/* eslint-enable */
