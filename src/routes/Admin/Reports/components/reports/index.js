import Reports from './Reports'
import enhance from './Reports.enhancer'

export default enhance(Reports)
