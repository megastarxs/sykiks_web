import React from 'react'
import enhance from './chat.enhancer'
import { FormattedTime, FormattedDate } from 'react-intl'
const chatsModalContent = ({ chats, back }) => (
  <div className="text-left chat-window">
    <button className="btn btn-primary" onClick={back}>
      Back
    </button>
    {chats &&
      Object.keys(chats)
        .reverse()
        .map(key => {
          let chat = chats[key]
          return (
            <div key={key}>
              {chat.from == 'system' && (
                <div className="text-center system-message">{chat.message}</div>
              )}
              {chat.from != 'system' && (
                <div className="msg">
                  <div className="msg-container">
                    <span className="screen-name">
                      {chat.fromScreenName || chat.from}:
                    </span>
                    <span>{chat.message}</span>
                  </div>
                  <div className="msg-time">
                    <FormattedDate
                      value={chat.time}
                      year="numeric"
                      month="long"
                      day="2-digit"
                    />
                    {'   '}
                    <FormattedTime value={chat.time} />
                  </div>
                </div>
              )}
            </div>
          )
        })}
    {!chats && 'No chats to be seen'}
  </div>
)

export default enhance(chatsModalContent)
