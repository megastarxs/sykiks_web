import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'
import { required, validateEmail } from 'utils/form'
import './LoginForm.scss'
import { Button, Form } from 'reactstrap'
import FieldInput from 'components/FieldInput'
import { Link } from 'react-router'
export const LoginForm = ({ pristine, submitting, handleSubmit }) => (
  <Form onSubmit={handleSubmit} className="form-panel">
    <Field
      name="email"
      component={FieldInput}
      label="Email"
      validate={[required, validateEmail]}
      defaultValue=""
    />
    <Field
      name="password"
      component={FieldInput}
      label="Password"
      type="password"
      validate={required}
      defaultValue=""
    />
    <Link to="/forgot-pass" className="forgot-pass">
      <small>Forgot Password?</small>
    </Link>
    <Button
      type="submit"
      disabled={pristine || submitting}
      className="btn btn-lg btn-info btn-block">
      {submitting ? 'Loading' : 'Login'}
    </Button>
  </Form>
)

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired, // eslint-disable-line react/no-unused-prop-types
  pristine: PropTypes.bool.isRequired, // added by redux-form
  submitting: PropTypes.bool.isRequired, // added by redux-form
  handleSubmit: PropTypes.func.isRequired // added by redux-form (calls onSubmit)
}

export default LoginForm
