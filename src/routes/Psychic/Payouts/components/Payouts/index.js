import Payouts from './Payouts'
import enhance from './Payouts.enhancer'

export default enhance(Payouts)
