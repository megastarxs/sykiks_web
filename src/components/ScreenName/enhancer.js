import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'

export default compose(
  // UserIsAuthenticated,
  firebaseConnect(({ uid }) => [
    { path: `users/${uid}/screenName`, storeAs: `screenName_${uid}` }
  ]),
  connect(({ firebase: { data } }, { uid }) => ({
    screenName: data[`screenName_${uid}`]
  }))
)
