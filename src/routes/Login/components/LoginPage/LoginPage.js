import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import LoginForm from '../LoginForm'
import './LoginPage.scss'

export const LoginPage = ({ emailLogin, googleLogin, onSubmitFail }) => (
  <div className="form-signin">
    <title>Sykiks - Login</title>
    <div className="header-div">
      <br />
      <h3 className="purple-text">Login</h3>
    </div>
    <LoginForm onSubmit={emailLogin} onSubmitFail={onSubmitFail} />
    <div className="text-center no-desktop">
      <div>
        <h6>
          <Link to={'/signup/psychic'}> Register As Psychic</Link>
        </h6>{' '}
        - OR -{' '}
        <h6>
          <Link to={'/signup/client'}>Register As Client</Link>
        </h6>
      </div>
    </div>
  </div>
)

LoginPage.propTypes = {
  emailLogin: PropTypes.func, // from enhancer (withHandlers)
  onSubmitFail: PropTypes.func, // from enhancer (withHandlers)
  googleLogin: PropTypes.func // from enhancer (withHandlers)
}

export default LoginPage
