import React from 'react'
import { FormattedTime, FormattedDate, FormattedNumber } from 'react-intl'
import { Table } from 'reactstrap'
import Subheader from 'components/Subheader'
import { Link } from 'react-router'
import './Payouts.scss'
import DefaultMessage from 'components/DefaultMessage'
function Payouts({
  classes,
  payouts,
  updateProfileModal,
  closeModal,
  profileId,
  open
}) {
  return (
    <div className="container">
      <Subheader subheader="User Payouts" />
      {!payouts && (
        <DefaultMessage>
          No Payouts Yet. All User payouts are seen here.
        </DefaultMessage>
      )}
      {payouts && (
        <Table responsive bordered hover className="text-center">
          <thead>
            <tr>
              <th>Key</th>
              <th>Date</th>
              <th>Paid</th>
              <th>Fees</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            {Object.keys(payouts)
              .reverse()
              .map((key, index) => {
                let payout = payouts[key]
                return (
                  <tr key={key}>
                    <td component="th" scope="row">
                      <Link to={`/admin/payouts/${key}`}> {key} </Link>
                    </td>
                    <td>
                      <FormattedDate
                        value={payout.date}
                        year="numeric"
                        month="long"
                        day="2-digit"
                      />
                      {'   '}
                      <FormattedTime value={payout.date} />
                    </td>
                    <td className="currency">
                      <FormattedNumber
                        style="currency"
                        currency="USD"
                        value={payout.totalFees}
                      />
                    </td>
                    <td className="currency">
                      <FormattedNumber
                        style="currency"
                        currency="USD"
                        value={payout.totalPaid - payout.totalFees}
                      />
                    </td>
                    <td className="currency">
                      <FormattedNumber
                        style="currency"
                        currency="USD"
                        value={payout.totalPaid - 0}
                      />
                    </td>
                  </tr>
                )
              })}
          </tbody>
        </Table>
      )}
    </div>
  )
}

export default Payouts
