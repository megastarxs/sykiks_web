import { withFirebase } from 'react-redux-firebase'
import { compose, withStateHandlers } from 'recompose'
import { connect } from 'react-redux'
export default compose(
  withFirebase,
  connect(({ firebase: { auth } }) => ({
    auth
  })),
  withStateHandlers(
    { loading: false },
    {
      addCredits: (a, { auth }) => amount => {
        location.href = `/api/pay/${auth.uid}/${amount}`
        return { loading: true }
      }
    }
  )
)
