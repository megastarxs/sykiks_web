import { compose, withStateHandlers } from 'recompose'
import { connect } from 'react-redux'
import { firebaseConnect, populate } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated, UserIsAdmin } from 'utils/router'

const populates = [
  {
    child: 'key',
    root: 'users',
    childParam: 'screenName',
    childAlias: 'fromScreenName'
  }
]

export default compose(
  // create listener for payouts, results go into redux
  UserIsAuthenticated,
  UserIsAdmin,
  firebaseConnect([{ path: 'payout', populates }]),
  // map redux state to props
  connect(({ firebase }) => ({
    payouts: populate(firebase, 'payout', populates)
  })),
  spinnerWhileLoading(['payouts']),
  withStateHandlers(() => ({ profileId: false, open: false }), {
    updateProfileModal: ({ profileId, open }) => id => ({
      profileId: id,
      open: true
    }),
    closeModal: () => () => ({ open: false })
  })
)
