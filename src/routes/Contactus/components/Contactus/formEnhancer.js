import { reduxForm } from 'redux-form'
import { CONTACT_FORM_NAME } from 'constants'

export default reduxForm({
  form: CONTACT_FORM_NAME
})
