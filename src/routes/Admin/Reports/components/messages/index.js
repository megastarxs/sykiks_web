import React from 'react'
import { Modal, ModalBody } from 'reactstrap'
import enhance from './enhancer'
import { FormattedTime, FormattedDate } from 'react-intl'
const MessagesModalContent = ({ messages }) => (
  <div className="text-left">
    <ModalBody>
      <div>
        {messages &&
          Object.keys(messages)
            .reverse()
            .map(key => {
              let message = messages[key]
              return (
                <div key={key} className="msg">
                  <div className="msg-container">
                    <span className="screen-name">
                      {message.fromScreenName}:
                    </span>
                    <span>{message.message}</span>
                  </div>
                  <div className="msg-time">
                    <FormattedDate
                      value={message.time}
                      year="numeric"
                      month="long"
                      day="2-digit"
                    />
                    {'   '}
                    <FormattedTime value={message.time} />
                  </div>
                </div>
              )
            })}
        {!messages && 'No messages to be seen'}
      </div>
    </ModalBody>
  </div>
)
let MessagesModalContentEnhance = enhance(MessagesModalContent)

const MessagesModal = ({ open, onClose, messageId }) => (
  <Modal toggle={onClose} isOpen={open} className="large-modal">
    <MessagesModalContentEnhance messageId={messageId} onClose={onClose} />
  </Modal>
)

export default MessagesModal
