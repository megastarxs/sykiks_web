import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import notify from '../utils/notification'

try {
  admin.initializeApp()
} catch (e) {}

/**
 * @name chatRequest
 * Cloud Function triggered by Real Time Database Create Event
 * @type {functions.CloudFunction}
 */
export default functions.database
  .ref('/chat-requests/{pushId}')
  .onCreate(chatRequestEvent)

/**
 * @param  {functions.Event} event - Function event
 * @return {Promise}
 */
async function chatRequestEvent(snap, context) {
  const request = snap.val()
  const { pushId } = context.params

  let isBlocked = await admin
    .database()
    .ref(`settings/${request.psychic}/blocked/${request.client}`)
    .once('value')
  isBlocked = isBlocked.val()

  if (isBlocked)
    return admin
      .database()
      .ref(`chat-requests/${pushId}`)
      .update({ status: 'rejected' })

  let user = await admin
    .database()
    .ref(`users/${request.client}`)
    .once('value')
  let client = user.val()

  const payload = {
    title: 'Sykiks - New chat request',
    body: `${client.screenName} wants to chat with you.`,
    click_action: ''
  }

  notify(request.psychic, payload)
}
