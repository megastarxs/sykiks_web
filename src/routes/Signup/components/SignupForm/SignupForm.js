import React from 'react'
import { Field } from 'redux-form'
import FieldInput from 'components/FieldInput'
import { Button, Form } from 'reactstrap'
import { Link } from 'react-router'
import { required, validateEmail } from 'utils/form'
import { recaptchaSitekey } from 'config'
import Recaptcha from 'react-recaptcha'
import PsychicCategories from 'components/PsychicCategories'
import FileUpload from 'components/FileUpload'
import defaultUserImageUrl from 'static/images/user-default.png'
import './SignupForm.scss'
const CustomLabel = () => {
  return (
    <span className="check-label">
      Please make sure you agree to{' '}
      <Link to="/pages/privacy_policy" className="fw-500" target="_new">
        Privacy Policy
      </Link>{' '}
      and{' '}
      <Link to="/pages/terms_of_service" className="fw-500" target="_new">
        Terms of service
      </Link>
    </span>
  )
}
const SignupForm = ({
  pristine,
  submitting,
  handleSubmit,
  type,
  step,
  error,
  updateImage,
  captchaVerified,
  file
}) => (
  <Form onSubmit={handleSubmit} className="form-design form-panel row">
    <div className="col-lg-6 text-center">
      <FileUpload onDone={file => updateImage(file)}>
        <img
          src={file || defaultUserImageUrl}
          style={{ width: 200, height: 200, objectFit: 'contain' }}
          className="img-fluid"
        />
      </FileUpload>
    </div>
    <div className="col-lg-6">
      <Field
        name="profile.screenName"
        component={FieldInput}
        label="Display Name*"
        validate={[required]}
      />

      <Field
        name="login.email"
        component={FieldInput}
        label="Email*"
        validate={[required, validateEmail]}
      />
      <div className="row">
        <Field
          name="login.password"
          component={FieldInput}
          label="Password*"
          type="password"
          validate={required}
          className="col-sm-6"
        />

        <Field
          name="extra.confirmPassword"
          component={FieldInput}
          label="Confirm Password*"
          type="password"
          validate={required}
          className="col-sm-6"
        />
      </div>
      <Field
        name="type"
        component={FieldInput}
        defaultValue={type}
        type="hidden"
        className="d-none"
      />

      {type == 'psychic' && (
        <div>
          <Field
            name="profile.introduction"
            component={FieldInput}
            label="Introduction"
            maxLength={100}
          />

          <Field
            name="profile.aboutme"
            type="textarea"
            component={FieldInput}
            label="About Me"
          />
          <Field
            name="profile.categories"
            component={PsychicCategories}
            label="Categories*"
          />
        </div>
      )}

      <Field
        name="extra.IAgree"
        component={FieldInput}
        label={<CustomLabel />}
        validate={[required]}
        type={'checkbox'}
      />
      <div className="captcha-class">
        <Recaptcha
          sitekey={recaptchaSitekey}
          verifyCallback={captchaVerified}
        />
      </div>
      <br />
      <Button
        className="btn-lg btn-block"
        color="primary"
        type="submit"
        variant="raised"
        disabled={pristine || submitting}>
        {submitting ? 'Loading' : 'Sign Up'}
      </Button>
    </div>
  </Form>
)

export default SignupForm
