import React from 'react'
import { Table } from 'reactstrap'
import Subheader from 'components/Subheader'
import './ApproveUser.scss'
import DefaultMessage from 'components/DefaultMessage'
function ApproveUser({ psychics, approveUser, rejectUser }) {
  return (
    <div className="container">
      <Subheader subheader="Psychics To Approve" />
      {!psychics && <DefaultMessage>No Users to approve Yet.</DefaultMessage>}
      {psychics && (
        <Table responsive bordered hover className="text-center">
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {Object.keys(psychics).map((key, index) => {
              let psychic = psychics[key]
              let psychicUrl = '../profile/' + key
              return (
                <tr key={key}>
                  <td>
                    <a href={psychicUrl} target="_blank">
                      {psychic.screenName}
                    </a>
                  </td>
                  <td>
                    <a href={'mailto:' + psychic.email}>{psychic.email}</a>
                  </td>
                  <td>
                    <button
                      className="btn btn-lg btn-primary"
                      onClick={() => approveUser(key)}>
                      Approve
                    </button>{' '}
                    <button
                      className="btn btn-lg btn-danger"
                      onClick={() => rejectUser(key)}>
                      Reject
                    </button>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </Table>
      )}
    </div>
  )
}

export default ApproveUser
