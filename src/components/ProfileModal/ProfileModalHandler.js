import { withStateHandlers } from 'recompose'

let ProfileModalHandler = withStateHandlers(
  () => ({ profileId: false, open: false }),
  {
    updateProfileModal: ({ profileId, open }) => id => ({
      profileId: id,
      open: true
    }),
    closeModal: () => () => ({ open: false })
  }
)

export default ProfileModalHandler
