import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, populate } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'

const populates = [
  {
    child: 'from',
    root: 'users',
    childParam: 'screenName',
    childAlias: 'fromScreenName'
  }
]

export default compose(
  firebaseConnect(({ messageId }, firebase) => [
    {
      path: `messages/${messageId}`,
      storeAs: `messages_${messageId}`,
      populates
    }
  ]),
  connect(({ firebase }, { messageId }) => ({
    messages: populate(firebase, `messages_${messageId}`, populates)
  })),
  spinnerWhileLoading(['messages'])
)
