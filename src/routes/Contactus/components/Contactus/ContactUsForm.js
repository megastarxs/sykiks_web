import React from 'react'
import { Field } from 'redux-form'
import FieldInput from 'components/FieldInput'
import { Button, Form } from 'reactstrap'
import './Contactus.scss'
import { required, validateEmail } from 'utils/form'
import enhancer from './formEnhancer'
import { recaptchaSitekey } from 'config'
import Recaptcha from 'react-recaptcha'
const ContactusForm = ({
  pristine,
  submitting,
  handleSubmit,
  captchaVerified
}) => (
  <Form onSubmit={handleSubmit} className="form-design form-panel row">
    <Field
      name="fullName"
      component={FieldInput}
      placeholder="Full Name..."
      autocomplete="name"
      className="col-12"
      validate={[required]}
    />
    <Field
      name="email"
      component={FieldInput}
      placeholder="Email Address..."
      autocomplete="email"
      className="col-6"
      validate={[required, validateEmail]}
    />
    <Field
      name="message"
      component={FieldInput}
      type="textarea"
      placeholder="Message..."
      className="col-12"
      validate={[required]}
    />
    <div className="col-10 mb-3">
      <Recaptcha sitekey={recaptchaSitekey} verifyCallback={captchaVerified} />
    </div>

    <div className="col-3">
      <Button
        className="btn-lg "
        color="primary"
        type="submit"
        variant="raised"
        disabled={pristine || submitting}>
        {submitting ? 'Loading' : 'Submit Now'}
      </Button>
    </div>
  </Form>
)

export default enhancer(ContactusForm)
