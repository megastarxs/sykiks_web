import ChangePass from './ChangePass'
import enhance from './enhancer'

export default enhance(ChangePass)
