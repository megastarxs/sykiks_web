import AddFunds from './AddFunds'
import enhance from './AddFunds.enhancer'

export default enhance(AddFunds)
