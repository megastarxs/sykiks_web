import { connect } from 'react-redux'
import { compose, withHandlers, withStateHandlers } from 'recompose'
import { withNotifications } from 'modules/notification'
import { firebaseConnect, populate } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated, UserIsAdmin } from 'utils/router'
import { ProfileModalHandler } from 'components/ProfileModal'

const populates = [
  {
    child: 'reportedById',
    root: 'users',
    childParam: 'screenName',
    childAlias: 'reportedBy'
  },
  {
    child: 'reportedId',
    root: 'users',
    childParam: 'screenName',
    childAlias: 'reported'
  }
]

export default compose(
  // create listener for reports, results go into redux
  UserIsAuthenticated,
  UserIsAdmin,
  firebaseConnect([
    {
      path: 'reports',
      populates,
      queryParams: ['orderByChild=archived', 'notParsed', 'equalTo=null']
    }
  ]),
  // map redux state to props
  connect(({ firebase }) => ({
    reports: populate(firebase, 'reports', populates)
  })),
  spinnerWhileLoading(['reports']),
  withNotifications,
  withStateHandlers(
    {
      isMessagesModalOpen: false,
      messageId: null,
      isChatModalOpen: false,
      pair: null
    },
    {
      openMessagesModal: () => report => {
        let messageId
        let { reportedByType, reportedById, reportedId } = report
        if (reportedByType == 'client')
          messageId = `${reportedById}-${reportedId}`
        else messageId = `${reportedId}-${reportedById}`

        return { isMessagesModalOpen: true, messageId }
      },
      closeMessagesModal: () => () => ({
        isMessagesModalOpen: false,
        messageId: null
      }),
      openChatModal: () => report => {
        let pair = {}
        let { reportedByType, reportedById, reportedId } = report
        if (reportedByType == 'client') {
          pair.client = reportedById
          pair.psychic = reportedId
        } else {
          pair.client = reportedId
          pair.psychic = reportedById
        }

        return { isChatModalOpen: true, pair }
      },
      closeChatModal: () => () => ({
        isChatModalOpen: false,
        pair: null
      })
    }
  ),
  withHandlers({
    archive: ({ firebase, showSuccess }) => id => {
      if (confirm('Are you sure you want to archive this item?'))
        firebase
          .ref(`reports/${id}/archived`)
          .set(true)
          .then(() => {
            showSuccess(`This item has been archived.`)
          })
    }
  }),
  ProfileModalHandler
)
