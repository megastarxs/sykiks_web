import React from 'react'
import './Chat.scss'
import ChatBubble from 'components/ChatBubble'
import { isEmpty } from 'react-redux-firebase'
import Icon from 'components/Icon'
import enhance from './enhancer'
import Timer from 'components/Timer'
import ScreenName from 'components/ScreenName'
import DeboundTextArea from './DeboundTextArea'
import { FormattedNumber } from 'react-intl'
import Recorder from 'components/Recorder'
export const Session = ({
  chat,
  message,
  me,
  session,
  profile,
  startSession,
  setSessionTime,
  endSession,
  startTyping,
  stopTyping,
  balance,
  chatPairs,
  registerTextRef,
  registerScrollRef,
  serverTimeOffset,
  perHalfMinute,
  autoStartSessionIn,
  sendChat,
  openRecorder,
  closeRecorder,
  record
}) => {
  if (!session) return null
  let repeat = false

  if (profile.type == 'psychic' && chatPairs) {
    let pair = Object.values(chatPairs).find(
      d => d && session.client == d.client
    )
    if (pair && pair.count > 1) repeat = true
  }

  return (
    <div className="chat">
      <div className="purple-background row justify-content-between">
        <div className="profile-name">
          {profile.type == 'client' ? (
            <div>
              <ScreenName uid={session.psychic} />
              {session.typing &&
                session.typing.psychic && (
                  <span className="typing"> is typing...</span>
                )}
            </div>
          ) : (
            <div>
              <ScreenName uid={session.client} />
              {session.typing &&
                session.typing.client && (
                  <span className="typing"> is typing...</span>
                )}
            </div>
          )}
        </div>
        <div className="timer-section">
          <div className="row">
            <div className="timer-div col-md-auto">
              {session.sessionStarted && (
                <Timer
                  start={session.sessionStarted && !session.sessionEnded}
                  startTime={session.sessionStartedAt}
                  callback={setSessionTime}
                  perHalfMinute={perHalfMinute}
                  serverTimeOffset={serverTimeOffset}
                />
              )}
              {!session.sessionStarted && (
                <Timer
                  start={!session.sessionStarted}
                  startTime={session.acceptedAt}
                  serverTimeOffset={serverTimeOffset}
                  countdown={autoStartSessionIn}
                  countdownCallback={startSession}
                />
              )}
            </div>
            {profile.type == 'psychic' &&
              repeat &&
              !session.sessionStarted && (
                <button
                  className="start-button btn col-md-auto"
                  onClick={startSession}>
                  Start
                </button>
              )}
            <div className="cancel-div btn" onClick={endSession}>
              {/* <Icon name="md-close-circle" /> */}
              End Session
            </div>
          </div>
        </div>
      </div>

      <div className="chats frame">
        <div className="content">
          {isEmpty(chat) ? null : (
            <div className="messages">
              <ul>
                {Object.keys(chat).map(key => {
                  let message = chat[key]
                  return (
                    <div key={key}>
                      <ChatBubble
                        uid={message.from}
                        key={key}
                        time={message.time}>
                        {message.message}
                      </ChatBubble>
                    </div>
                  )
                })}
              </ul>
              <div
                style={{ float: 'left', clear: 'both' }}
                id="test"
                ref={registerScrollRef}
              />
              {session.sessionEnded && (
                <div className="text-center">
                  <button
                    className="btn btn-lg btn-danger"
                    onClick={endSession}>
                    Close Session
                  </button>
                </div>
              )}
              {session.creditsEmpty &&
                profile.type == 'client' && (
                  <div
                    className="text-center mt-3 mb-2"
                    style={{ width: '50%', margin: '0 auto' }}>
                    <p style={{ fontSize: '1rem' }}>
                      You don't have enough credits to continue this chat
                      session{' '}
                      <button
                        className="btn btn-lg btn-danger"
                        onClick={() => endSession('addcredit')}>
                        Add Credits
                      </button>
                    </p>
                  </div>
                )}
            </div>
          )}
          <div className="card message-input amount-detail text-center">
            {profile.type == 'client' && (
              <span>
                Paid{' '}
                <span className="currency">
                  <FormattedNumber
                    style="currency"
                    currency="USD"
                    value={session.amount}
                  />
                </span>
                {' for current session. '}
                Balance:
                <span className="currency">
                  <FormattedNumber
                    style="currency"
                    currency="USD"
                    value={balance}
                  />
                </span>{' '}
              </span>
            )}
            {profile.type == 'psychic' && (
              <span>
                Earned
                <span className="currency">
                  {' '}
                  <FormattedNumber
                    style="currency"
                    currency="USD"
                    value={session.amount}
                  />
                </span>{' '}
                from current session
              </span>
            )}
          </div>
          <div className="message-input ">
            <Recorder open={record} close={closeRecorder} onDone={sendChat} />
            <div className="wrap">
              <DeboundTextArea
                startTyping={startTyping}
                stopTyping={stopTyping}
                value={message}
                onEnter={sendChat}
                disable={session.sessionEnded}
                registerTextRef={registerTextRef}
              />
              <button className="submit" onClick={sendChat}>
                <Icon name="ios-send" />
              </button>
              <button className="submit" onClick={openRecorder}>
                <Icon name="ios-mic" />
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default enhance(Session)
