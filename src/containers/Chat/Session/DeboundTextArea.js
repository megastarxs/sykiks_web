import React from 'react'
import _ from 'lodash'
export default class DebounceTextArea extends React.Component {
  componentDidMount() {
    let { startTyping, stopTyping } = this.props

    this.startTyping = _.debounce(
      function() {
        startTyping()
      },
      500,
      { leading: true, trailing: false }
    )

    this.stopTyping = _.debounce(function() {
      stopTyping()
    }, 500)
  }

  textChange(e) {
    let text = e.target.value
    if (text != '') {
      if (this.props.onChange) this.props.onChange(text)
      this.startTyping()
      this.stopTyping()
    }
  }

  _handleKeyPress(e) {
    if (e.key === 'Enter') {
      e.preventDefault()
      this.props.onEnter()
    }
  }
  render() {
    return (
      <textarea
        onChange={this.textChange.bind(this)}
        value={this.props.value}
        placeholder="Write your message..."
        disabled={this.props.disable}
        onKeyPress={this._handleKeyPress.bind(this)}
        ref={this.props.registerTextRef}
        rows={3}
      />
    )
  }
}
