import * as functions from 'firebase-functions'
import express from 'express'
import addCredits from './routes/add_credits'
import payout from './routes/payout'
import adminDashboard from './routes/admin_dashboard'
import sitemap from './routes/sitemap'
import * as admin from 'firebase-admin'
import test from './routes/test'

try {
  admin.initializeApp()
} catch (e) {}

const app = express()
app.set('json spaces', 2)

addCredits(app)
payout(app)
adminDashboard(app)
sitemap(app)
test(app)

const main = express()
main.use('/api', app)

export default functions.https.onRequest((request, response) => {
  return main(request, response)
})
