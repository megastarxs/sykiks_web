import * as admin from 'firebase-admin'

var now = () => admin.database.ServerValue.TIMESTAMP
export { now }
export const getVal = promise => {
  return promise
    .once('value')
    .then(s => s.val())
    .catch(function() {
      Promise.resolve(false)
    })
}
