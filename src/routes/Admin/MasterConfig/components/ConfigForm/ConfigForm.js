import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'
import FieldInput from 'components/FieldInput'
import { Button, Form } from 'reactstrap'
import classes from './ConfigForm.scss'
export const ConfigForm = ({ handleSubmit, submitting, pristine }) => (
  <Form className="form-panel" onSubmit={handleSubmit}>
    <Field
      component={FieldInput}
      type="checkbox"
      name="maintenanceMode"
      label="Maintenance Mode"
    />

    <Field
      component={FieldInput}
      type="number"
      name="percentageCut"
      label="Percentage cut"
    />
    <Field
      component={FieldInput}
      type="text"
      name="clientId"
      label="Client Id"
    />
    <Field
      component={FieldInput}
      type="text"
      name="clientSecret"
      label="Client Secret"
    />
    <div className="form-group">
      <label>Paypal Mode</label>
      <div>
        <Field
          name="paypalMode"
          label="Paypal Mode"
          component="select"
          className="form-control">
          <option>sandbox</option>
          <option>live</option>
        </Field>
      </div>
    </div>

    <Button
      disabled={submitting || pristine}
      color="primary"
      type="submit"
      className={classes.submit}>
      Save
    </Button>
  </Form>
)

ConfigForm.propTypes = {
  submitting: PropTypes.bool.isRequired, // from enhancer (reduxForm)
  pristine: PropTypes.bool.isRequired, // from enhancer (reduxForm)
  handleSubmit: PropTypes.func.isRequired // from enhancer (reduxForm)
}

export default ConfigForm
