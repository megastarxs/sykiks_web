import React from 'react'
import PropTypes from 'prop-types'
import { browserHistory, Router } from 'react-router'
import { Provider } from 'react-redux'
import { IntlProvider } from 'react-intl'

function handleUpdate() {
  let { action } = this.state.location

  if (action === 'PUSH') {
    window.scrollTo(0, 0)
  }
}

const App = ({ routes, store }) => (
  <IntlProvider locale="en">
    <Provider store={store}>
      <Router history={browserHistory} onUpdate={handleUpdate}>
        {routes}
      </Router>
    </Provider>
  </IntlProvider>
)

App.propTypes = {
  routes: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired
}

export default App
