import ConfigForm from './ConfigForm'
import enhance from './ConfigForm.enhancer'

export default enhance(ConfigForm)
