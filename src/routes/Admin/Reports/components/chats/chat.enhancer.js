import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, populate } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'

const populates = [
  {
    child: 'from',
    root: 'users',
    childParam: 'screenName',
    childAlias: 'fromScreenName'
  }
]

export default compose(
  firebaseConnect(({ chatId }, firebase) => [
    {
      path: `chats/${chatId}`,
      storeAs: `chats_${chatId}`,
      populates
    }
  ]),
  connect(({ firebase }, { chatId }) => ({
    chats: populate(firebase, `chats_${chatId}`, populates)
  })),
  spinnerWhileLoading(['chats'])
)
