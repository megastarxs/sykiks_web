import { compose } from 'redux'
import { connect } from 'react-redux'
import { withProps, lifecycle } from 'recompose'
import { firebaseConnect, isEmpty, isLoaded } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { updateconfig } from 'store/config'
export default compose(
  firebaseConnect(() => [
    { path: `master-config/maintenanceMode`, storeAs: `maintenanceMode` }
  ]),
  connect(
    ({ location, config, firebase: { profile, auth, data } }) => ({
      profile,
      auth,
      config,
      maintenanceMode: data['maintenanceMode'],
      loggedIn: isLoaded(auth) && !isEmpty(auth)
    }),
    dispatch => ({
      updateconfig: config => {
        updateconfig(dispatch, config)
      }
    })
  ),
  lifecycle({
    componentDidMount() {
      let { location, updateconfig } = this.props
      let { search } = location
      if (location.query && location.query.desktop_token) {
        updateconfig({ desktop: true, search })
        document.body.classList.add('hide-sumo')
      }
    }
  }),
  spinnerWhileLoading(['profile']),
  withProps(({ auth, profile, location }) => ({
    loggedIn: isLoaded(auth) && !isEmpty(auth)
  }))
)
