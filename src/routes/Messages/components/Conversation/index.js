import conversation from './conversation'
import enhance from './conversation.enhancer'

export default enhance(conversation)
