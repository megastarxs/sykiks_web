import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'

try {
  admin.initializeApp()
} catch (e) {}

/**
 * @name addReview
 * Cloud Function triggered by Real Time Database Write Event
 * @type {functions.CloudFunction}
 */
export default functions.database
  .ref('/reviews/{psychicId}/{insertId}')
  .onCreate(addReviewEvent)

/**
 * @param  {functions.Event} event - Function event
 * @return {Promise}
 */
async function addReviewEvent(snap, context) {
  const { psychicId } = context.params
  const reviewData = snap.val()
  let refPath = `users/${psychicId}/rating_down`
  if (reviewData.rating == 'up') refPath = `users/${psychicId}/rating_up`

  return admin
    .database()
    .ref(refPath)
    .transaction(rating => (rating ? rating + 1 : 1))
}
