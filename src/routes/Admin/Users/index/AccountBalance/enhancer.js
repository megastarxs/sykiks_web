import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'

export default compose(
  firebaseConnect(({ uid }, firebase) => [
    {
      path: `settings/${uid}/accountInfo/balance`,
      storeAs: 'balance_' + uid
    }
  ]),
  connect(({ firebase: { data } }, { uid }) => ({
    balance: data['balance_' + uid]
  }))
)
