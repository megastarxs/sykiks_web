import React from 'react'
import { connect } from 'react-redux'
import { withHandlers, compose } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import { withRouter } from 'utils/components'
import { withNotifications } from 'modules/notification'
import Chat from 'utils/Chat'
import Icon from 'components/Icon'

let enhancer = compose(
  firebaseConnect(({ roomId, psychic }, firebase) => {
    let arr = []
    if (firebase.firebase._.authUid)
      arr = [
        {
          path: `settings/${firebase.firebase._.authUid}/accountInfo/balance`,
          storeAs: 'balance'
        },
        {
          path: 'chat-pairs',
          queryParams: [
            'orderByChild=client',
            'equalTo=' + firebase.firebase._.authUid
          ],
          storeAs: `chatPairs`
        }
      ]
    return arr
  }),
  withNotifications,
  withRouter,
  connect(({ firebase: { data, auth, profile } }, { psychic }) => ({
    loggedIn: !auth.isEmpty,
    uid: auth.uid,
    auth,
    balance: data.balance,
    chatPairs: data.chatPairs,
    profile
  })),
  withHandlers({
    callBack: ({
      loggedIn,
      profile,
      router,
      auth,
      messagePath,
      psychic,
      uid,
      chatRate,
      balance,
      chatPairs,
      showError
    }) => () => {
      if (!loggedIn) router.push('/login')
      else if (!auth.emailVerified)
        showError('You email id is not verified. Please verify your email id.')
      else if (profile.type == 'psychic')
        showError('Sorry you cannot perform this action')
      else if (messagePath) router.push(messagePath)
      else if (psychic) {
        let pair = {}
        if (chatPairs)
          pair = Object.values(chatPairs).find(d => d && psychic == d.psychic)
        if (pair && pair.count)
          if (!balance || balance < chatRate)
            return showError(
              "You don't have enough credits to chat with this psychic"
            )
        Chat.initClientRequest(uid, psychic)
      }
    }
  })
)

let Button = ({ children, callBack, chatRate, messagePath, online }) => (
  <div onClick={callBack}>
    {chatRate >= 0 &&
      online && (
        <button className="btn green-button card-link ">
          <Icon name="ios-chatboxes" />CHAT NOW
        </button>
      )}
    {chatRate >= 0 &&
      !online && (
        <button className="btn orange-button card-link ">
          <Icon name="ios-time" />Away
        </button>
      )}
    {messagePath && children}
  </div>
)

export default enhancer(Button)
