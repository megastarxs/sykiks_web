import { connect } from 'react-redux'
import { compose } from 'recompose'
import { firebaseConnect, populate } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'

const populates = [
  {
    child: 'from',
    root: 'users',
    childParam: 'screenName',
    childAlias: 'fromScreenName'
  }
]

export default compose(
  firebaseConnect((props, firebase) => [
    {
      path: `settings/${
        props.profile.type == 'admin' ? 'admin' : firebase.firebase._.authUid
      }/messages-listing`,
      storeAs: 'messages',
      populates
    },
    {
      path: `settings/${
        props.profile.type == 'admin' ? 'admin' : firebase.firebase._.authUid
      }/unread`,
      storeAs: 'unread'
    }
  ]),
  connect(({ firebase }, { roomId }) => ({
    messages: populate(firebase, 'messages', populates),
    unread: firebase.data.unread,
    roomId: roomId,
    profile: firebase.profile
  })),
  spinnerWhileLoading(['messages', 'unread'])
)
