import { compose, withStateHandlers, withHandlers } from 'recompose'
import { connect } from 'react-redux'
import { firebaseConnect, populate } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated, UserIsAdmin } from 'utils/router'
import { withNotifications } from 'modules/notification'
import { ProfileModalHandler } from 'components/ProfileModal'

const populates = [
  {
    child: 'uid',
    root: 'settings',
    childParam: 'accountInfo',
    childAlias: 'accountInfo'
  }
]

function weekNumber() {
  var today = new Date()
  var onejan = new Date(today.getFullYear(), 0, 1)
  return (
    Math.ceil(((today - onejan) / 86400000 + onejan.getDay() + 1) / 7) +
    '-' +
    today.getFullYear()
  )
}

export default compose(
  // create listener for admin, results go into redux
  UserIsAuthenticated,
  UserIsAdmin,
  firebaseConnect([
    {
      path: 'users',
      queryParams: ['orderByChild=type', 'equalTo=psychic'],
      storeAs: `pendingPayouts`,
      populates
    },
    {
      path: 'master-config/percentageCut',
      storeAs: `percentageCut`
    }
  ]),
  withNotifications,
  connect(({ firebase }) => ({
    users: populate(firebase, `pendingPayouts`, populates),
    percentageCut: firebase.data.percentageCut
  })),
  withStateHandlers(
    { search: '', referenceId: 'SYKIKS-' + weekNumber(), paidUsers: {} },
    {
      updatePaid: ({ paidUsers }) => key => ({
        paidUsers: { [key]: true, ...paidUsers }
      }),
      setSearch: () => e => ({ search: e.target.value })
    }
  ),
  withHandlers({
    paid: ({ firebase, showSuccess, updatePaid, referenceId }) => paidInfo => {
      paidInfo.manual = true
      firebase
        .ref(`payout/${referenceId}/paidTo`)
        .push(paidInfo)
        .then(user => {
          updatePaid(paidInfo.key)
          showSuccess('User payout updated..')
        })
    }
  }),
  spinnerWhileLoading(['users', 'percentageCut']),
  ProfileModalHandler
)
