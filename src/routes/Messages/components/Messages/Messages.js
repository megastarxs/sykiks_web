import React from 'react'
import './Messages.scss'
import { AvatarByUid } from 'components/Avatar'
import Subheader from 'components/Subheader'
import Conversation from '../Conversation'
import DefaultMessage from 'components/DefaultMessage'
import ScreenName from 'components/ScreenName'
import { Link } from 'react-router'
import { smallText } from 'utils/helpers'
import Icon from 'components/Icon'

let getUserId = (roomId, type) => {
  let roomIds = roomId.split('-')
  if (type == 'admin' && roomIds[1] == 'admin') return roomIds[0]

  return roomIds[1]
}

const Messages = ({ messages, roomId, unread, profile }) => (
  <div className="container messages-list">
    <Subheader subheader="Messages" />
    {!messages &&
      !roomId && (
        <DefaultMessage>
          You don't have any messages yet. Your Messages will be seen here
        </DefaultMessage>
      )}
    {(messages || roomId) && (
      <div className="row">
        <div className="col-sm-3 col-md-3 col-lg-3 grey-bg">
          <div>
            {messages &&
              Object.keys(messages).map(key => {
                let message = messages[key]
                return (
                  <div key={key}>
                    <Link
                      activeClassName="active"
                      to={`/messages/${message.roomId}`}>
                      <div className="card flex-row">
                        <div className="card-header border-0">
                          <AvatarByUid
                            uid={message.from}
                            className="avt-image img-fluid"
                          />
                        </div>
                        <div className="card-block px-2">
                          <p
                            className={`card-title ${
                              unread && unread[message.from]
                                ? 'font-weight-bold'
                                : ''
                            }`}>
                            {message.fromScreenName}
                          </p>
                          <p
                            className={`card-text ${
                              unread && unread[message.from]
                                ? 'font-weight-bold'
                                : ''
                            }`}>
                            {message.lastMessage.audio ? (
                              <span>
                                <Icon name="md-mic" />Audio Received
                              </span>
                            ) : (
                              smallText(message.lastMessage)
                            )}
                          </p>
                        </div>
                      </div>
                    </Link>
                  </div>
                )
              })}
            {roomId &&
              (!messages ||
                (messages &&
                  !Object.keys(messages)
                    .map(key => messages[key].roomId)
                    .includes(roomId))) && (
                <div>
                  <Link activeClassName="active" to={`/messages/${roomId}`}>
                    <div className="card flex-row">
                      <div className="card-header border-0">
                        <AvatarByUid
                          uid={getUserId(roomId, profile.type)}
                          className="avt-image img-fluid"
                        />
                      </div>
                      <div className="card-block px-2 ">
                        <p className="card-title">
                          <ScreenName uid={getUserId(roomId, profile.type)} />
                        </p>
                      </div>
                    </div>
                  </Link>
                </div>
              )}
          </div>
        </div>
        <div className="col-sm-9 col-md-9 col-lg-9 conversation">
          {roomId ? (
            <Conversation roomId={roomId} />
          ) : (
            <DefaultMessage>
              {' '}
              Your conversation will be seen here{' '}
            </DefaultMessage>
          )}
        </div>
      </div>
    )}
  </div>
)

export default Messages
