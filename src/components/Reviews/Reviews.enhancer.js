import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, populate } from 'react-redux-firebase'
import { withHandlers } from 'recompose'
import { spinnerWhileLoading } from 'utils/components'
import { withNotifications } from 'modules/notification'

const populates = [
  {
    child: 'from',
    root: 'users',
    childParam: 'screenName',
    childAlias: 'fromScreenName'
  }
]

export default compose(
  firebaseConnect(({ uid }) => [
    {
      path: `reviews/${uid}`,
      storeAs: `reviews_${uid}`,
      populates
    }
  ]),
  withNotifications,
  connect(({ firebase }, { uid }) => ({
    reviews: populate(firebase, `reviews_${uid}`, populates),
    profile: firebase.profile
  })),
  withHandlers({
    deleteReview: ({ firebase, showSuccess, uid }) => reviewId => {
      if (confirm('Are you sure you want to delete this review'))
        firebase
          .ref(`reviews/${uid}/${reviewId}`)
          .remove()
          .then(() => {
            firebase
              .ref(`users/${uid}/rating_down`)
              .transaction(rating => rating - 1)

            showSuccess(`Review has been deleted`)
          })
    }
  }),
  spinnerWhileLoading(['reviews'])
)
