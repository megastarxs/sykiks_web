import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'

export default compose(
  firebaseConnect((props, firebase) => [
    {
      path: `settings/${
        props.type == 'admin' ? 'admin' : firebase.firebase._.authUid
      }/unread`,
      storeAs: 'messagesCount'
    }
  ]),
  connect(({ firebase: { data: { messagesCount } } }) => ({
    messagesCount
  }))
)
