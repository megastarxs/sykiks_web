import { compose } from 'redux'
import { withFirebase } from 'react-redux-firebase'
import { withHandlers } from 'recompose'
import { withNotifications } from 'modules/notification'

export default compose(
  // UserIsAuthenticated,
  withFirebase,
  withNotifications,
  withHandlers({
    resetPass: ({ firebase, showSuccess, showError }) => email => {
      var auth = firebase.auth()
      auth
        .sendPasswordResetEmail(email, { url: location.origin })
        .then(function() {
          showSuccess(
            `Email has been sent to registered email. Please check your email. `
          )
        })
        .catch(function() {
          showError('Oops! This email is not registered.')
        })
    }
  })
)
