import React from 'react'
import enhancer from './enhancer'
import Session from '../Session'
import { Modal, ModalBody } from 'reactstrap'
import '../Chat.scss'
let CheckSession = ({ currentSessionKey, hasCurrentSession, profile }) => (
  <div className="chat-container container">
    <div>
      {hasCurrentSession && (
        <Modal
          isOpen={hasCurrentSession}
          style={{
            width: '75%',
            maxWidth: '75%',
            marginLeft: 'auto',
            marginRight: 'auto'
          }}>
          <ModalBody style={{ paddingTop: 0 }}>
            <Session sessionKey={currentSessionKey} ouner={profile} />
          </ModalBody>
        </Modal>
      )}
    </div>
  </div>
)
export default enhancer(CheckSession)
