import React from 'react'
import Subheader from 'components/Subheader'
import config from './config'

export const Pages = ({
  router: {
    params: { id }
  }
}) => (
  <div className="container">
    <Subheader subheader={config[id].title} />
    {config[id].content}
  </div>
)

export default Pages
