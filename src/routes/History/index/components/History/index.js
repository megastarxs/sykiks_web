import React from 'react'
import History from './History'
import {
  clientEnhancer,
  psychicEnhancer,
  profileEnhancer
} from './History.enhancer'

const ClientHistory = clientEnhancer(History)
const PsychicHistory = psychicEnhancer(History)

export const HistoryWrap = ({ profile }) => (
  <div>
    {profile.type == 'client' && <ClientHistory profile={profile} />}
    {profile.type == 'psychic' && <PsychicHistory profile={profile} />}
  </div>
)
export default profileEnhancer(HistoryWrap)
