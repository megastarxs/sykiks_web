import React from 'react'
import { HISTORY_PATH } from 'constants'
import { Link } from 'react-router'
import { Nav, NavItem, NavLink } from 'reactstrap'
import Icon from 'components/Icon'

export default props => (
  <Nav className="ml-auto user-navigation" navbar>
    <NavItem>
      <NavLink
        tag={Link}
        activeClassName="active"
        to={HISTORY_PATH}
        className="d-flex align-items-center">
        <Icon name="ios-timer" className="icon" />
        Session History
      </NavLink>
    </NavItem>
  </Nav>
)
