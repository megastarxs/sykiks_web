import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'

export default compose(
  // UserIsAuthenticated,
  firebaseConnect(({ uid }) => [
    { path: `users/${uid}/avatar`, storeAs: `image_${uid}` }
  ]),
  connect(({ firebase: { data } }, { uid }) => ({
    src: data[`image_${uid}`]
  }))
)
