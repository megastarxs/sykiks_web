import React from 'react'
import { Modal, ModalBody, Table, Button } from 'reactstrap'
import enhance from './chatSessions.enhancer'
import Chats from './chats'
import './chat.scss'
import { FormattedTime, FormattedDate, FormattedNumber } from 'react-intl'
const ChatSessionContent = ({ chatSessions, pair, setChatId, sessionKey }) => (
  <div className="text-center">
    <ModalBody>
      {!sessionKey && (
        <Table>
          <thead>
            <tr>
              <td>Accepted At</td>
              <td>Session Started At</td>
              <td>Chat Rate</td>
              <td>Amount</td>
              <td>Duration</td>
              <td />
            </tr>
          </thead>
          <tbody>
            {chatSessions &&
              Object.values(chatSessions)
                .filter(d => d.psychic == pair.psychic)
                .map(session => (
                  <tr key={session.sessionKey}>
                    <td>
                      <FormattedDate
                        value={session.acceptedAt}
                        year="numeric"
                        month="long"
                        day="2-digit"
                      />
                    </td>
                    <td>
                      <FormattedTime value={session.sessionStartedAt} />
                    </td>
                    <td>
                      <FormattedNumber
                        style="currency"
                        currency="USD"
                        value={session.chatRate}
                      />
                    </td>
                    <td>
                      <FormattedNumber
                        style="currency"
                        currency="USD"
                        value={session.amount}
                      />
                    </td>
                    <td>
                      {session.sessionTime}
                      {!session.sessionTime && 'Session did not start'}
                    </td>
                    <td>
                      <Button
                        color="link"
                        onClick={() => setChatId(session.sessionKey)}>
                        View
                      </Button>
                    </td>
                  </tr>
                ))}
          </tbody>
        </Table>
      )}
      {sessionKey && <Chats chatId={sessionKey} back={() => setChatId(null)} />}
    </ModalBody>
  </div>
)
let ChatSessionModalContentEnhance = enhance(ChatSessionContent)

const ChatSessionModal = ({ open, onClose, pair }) => (
  <Modal toggle={onClose} isOpen={open} className="large-modal">
    {pair && <ChatSessionModalContentEnhance pair={pair} onClose={onClose} />}
  </Modal>
)

export default ChatSessionModal
