import User from './Users'
import enhance from './enhancer'

export default enhance(User)
