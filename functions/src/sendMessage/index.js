import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import notify from '../utils/notification'
import { getVal } from '../utils/helpers'
try {
  admin.initializeApp()
} catch (e) {}

/**
 * @name sendMessage
 * Cloud Function triggered by Real Time Database Update Event
 * @type {functions.CloudFunction}
 */
export default functions.database
  .ref('settings/{myId}/messages-listing/{partnerId}')
  .onWrite(sendMessageEvent)

async function sendMessageEvent(change, context) {
  const { myId } = context.params
  const msgData = change.after.val()

  if (msgData.fromServer) return

  let { roomId } = msgData
  let partnerId = ''
  let ids = roomId.split('-')
  if (ids[0] == myId) partnerId = ids[1]
  else partnerId = ids[0]

  let settingsRef = admin.database().ref(`settings/${myId}`)
  let partnerSettingsRef = admin.database().ref(`settings/${partnerId}`)
  let masterConfigRef = admin
    .database()
    .ref(`master-config/today/psychicEarning`)

  let isBlocked = await partnerSettingsRef
    .child(`blocked/${myId}`)
    .once('value')
  isBlocked = isBlocked.val()

  if (isBlocked) return

  let profile = await getVal(admin.database().ref(`users/${myId}`))

  let label = ''
  if (msgData.lastMessage.audio) label = { audio: '' }
  else label = `${msgData.lastMessage.slice(0, 20)}...`

  const notification = {
    title: `${profile.screenName}`,
    body: label,
    click_action: `messages/${roomId}`,
    data: { roomId }
  }
  notify(partnerId, notification)

  let msgRate = 0

  if (profile.type == 'client' && !roomId.includes('admin')) {
    msgRate = msgData.msgRate - 0 || 0
    if (msgRate != 0) {
      settingsRef
        .child(`accountInfo/balance`)
        .transaction(balance => balance - msgRate)

      partnerSettingsRef
        .child(`accountInfo/balance`)
        .transaction(balance => balance + msgRate)

      masterConfigRef.transaction(balance => balance + msgRate)

      settingsRef.child(`transactions`).push({
        type: 'paid_for_message',
        date: msgData.time,
        amount: msgRate,
        from: partnerId
      })

      partnerSettingsRef.child(`transactions`).push({
        type: 'received_for_message',
        date: msgData.time,
        amount: msgRate,
        from: myId
      })
    }
  }
  partnerSettingsRef.child(`unread/${myId}`).set(true)

  msgData.fromServer = true
  msgData.from = myId

  partnerSettingsRef.child(`messages-listing/${myId}`).update(msgData)

  return admin
    .database()
    .ref(`messages/${msgData.roomId}`)
    .push({
      from: myId,
      message: msgData.lastMessage,
      time: msgData.time,
      msgRate
    })
}
