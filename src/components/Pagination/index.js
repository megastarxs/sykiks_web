import React from 'react'

class Pagination extends React.Component {
  state = {
    currentPage: 1,
    dataPerPage: 10
  }
  constructor() {
    super()
    this.setSlicedData = this.setSlicedData.bind(this)
    this.setPageNumber = this.setPageNumber.bind(this)
  }

  setSlicedData(currentPage) {
    const { data, setData } = this.props
    const { dataPerPage } = this.state

    const indexOfLastTodo = currentPage * dataPerPage
    const indexOfFirstTodo = indexOfLastTodo - dataPerPage
    const currentdata = data.slice(indexOfFirstTodo, indexOfLastTodo)
    if (setData) setData(currentdata)
  }

  componentDidMount() {
    this.setSlicedData(1)
  }

  setPageNumber(currentPage) {
    this.setState({ currentPage })
    this.setSlicedData(currentPage)
  }

  render() {
    const { data } = this.props
    const { dataPerPage, currentPage } = this.state

    const pageNumbers = []
    for (let i = 1; i <= Math.ceil(data.length / dataPerPage); i++) {
      pageNumbers.push(i)
    }

    return (
      <nav>
        <ul className="pagination mt-3 justify-content-center">
          {pageNumbers.map(number => (
            <li
              key={number}
              className={`page-item ${number == currentPage ? 'active' : ''} `}
              onClick={() => this.setPageNumber(number)}>
              <button className="page-link" id={number}>
                {number}
              </button>
            </li>
          ))}
        </ul>
      </nav>
    )
  }
}

export default Pagination
