import React from 'react'
let UserModal = ({
  report,
  changeText,
  reportText,
  sendReport,
  regularProfileModal,
  block,
  blockUser,
  regular,
  reportProfileModal,
  blockProfileModal,
  blocked,
  uid,
  user,
  goToMessages,
  router,
  myId
}) => {
  let path =
    user.type == 'psychic'
      ? `/messages/${myId}-${uid}`
      : `/messages/${uid}-${myId}`
  return (
    <div>
      {report && (
        <div>
          <div className="reporting">
            <textarea
              placeholder="Write reason for reporting here..."
              className="reporting-text"
              rows="3"
              onChange={changeText}
              value={reportText}
            />
          </div>
          <div>
            <button className="btn btn-lg purple-button" onClick={sendReport}>
              Send Report
            </button>
            <button
              className="btn btn-lg light-purple-button"
              onClick={regularProfileModal}>
              Cancel
            </button>
          </div>
        </div>
      )}
      {block && (
        <div>
          <div className="reporting">
            <p>Are you sure you want to block this user?</p>
          </div>
          <div>
            <button className="btn btn-lg purple-button" onClick={blockUser}>
              Yes
            </button>
            <button
              className="btn btn-lg light-purple-button"
              onClick={regularProfileModal}>
              No
            </button>
          </div>
        </div>
      )}
      {regular &&
        uid != myId && (
          <div>
            <div>
              <button
                className="btn btn-lg purple-button"
                onClick={reportProfileModal}>
                Report
              </button>
              {!blocked && (
                <button
                  className="btn btn-lg light-purple-button"
                  onClick={blockProfileModal}>
                  Block
                </button>
              )}
              {blocked && (
                <button
                  className="btn btn-lg light-purple-button"
                  disabled="disabled">
                  Blocked
                </button>
              )}
              {path != router.location.pathname && (
                <button
                  className="btn btn-lg purple-button"
                  onClick={() => goToMessages(path)}>
                  Message
                </button>
              )}
            </div>
          </div>
        )}
    </div>
  )
}
export default UserModal
