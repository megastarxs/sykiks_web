import React from 'react'
import './recorder.scss'
import { Modal, ModalBody } from 'reactstrap'
import Icon from 'components/Icon'
import ReactMediaRecorder from 'react-media-recorder'
import enhancer from './enhancer'

let Recorder = ({ open, close, sendMessage, setRef, attach, progress }) => (
  <div className="recorder-parent">
    <Modal isOpen={open}>
      <ModalBody className="text-center cropper-box recorder-box">
        <ReactMediaRecorder
          ref={setRef}
          audio
          video={false}
          render={({
            status,
            startRecording,
            stopRecording,
            mediaBlob,
            blobPropertyBag
          }) => (
            <div>
              <div>{mediaBlob && <audio src={mediaBlob} controls />}</div>
              {progress && (
                <div className="progress">
                  <div
                    className="progress-bar"
                    style={{ width: progress + '%' }}
                  />
                </div>
              )}
              {!progress && (
                <div>
                  {status == 'idle' && (
                    <button
                      onClick={startRecording}
                      className="btn btn-outline-danger">
                      <span
                        className="glyphicon glyphicon-record"
                        aria-hidden="true"
                      />
                      <Icon name="md-radio-button-on" />Record
                    </button>
                  )}
                  {status == 'stopped' && (
                    <button
                      onClick={startRecording}
                      className="btn btn-outline-danger">
                      <Icon name="md-backspace" />Try again
                    </button>
                  )}
                  {status == 'recording' && (
                    <button
                      onClick={stopRecording}
                      className="btn btn-outline-primary">
                      <Icon name="md-square" />Stop
                    </button>
                  )}
                  {status == 'stopped' && (
                    <button
                      className="btn btn-outline-success"
                      onClick={attach}>
                      <Icon name="md-attach" />Attach
                    </button>
                  )}
                  {status != 'recording' && (
                    <button onClick={close} className="btn btn-outline-primary">
                      <Icon name="md-close-circle" />Cancel
                    </button>
                  )}
                </div>
              )}
            </div>
          )}
        />
      </ModalBody>
    </Modal>
  </div>
)

export default enhancer(Recorder)
