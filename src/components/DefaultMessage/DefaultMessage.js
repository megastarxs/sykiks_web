import React from 'react'

export const DefaultMessage = ({ check, message, children }) => (
  <div>
    <p className="text-center empty-msg">{children}</p>
  </div>
)

export default DefaultMessage
