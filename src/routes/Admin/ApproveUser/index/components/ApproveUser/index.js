import ApproveUser from './ApproveUser'
import enhance from './ApproveUser.enhancer'

export default enhance(ApproveUser)
