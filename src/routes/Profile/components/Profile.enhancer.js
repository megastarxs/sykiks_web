import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { ProfileModalHandler } from 'components/ProfileModal'

export default compose(
  // UserIsAuthenticated,
  firebaseConnect(props => [
    { path: `users/${props.params.id}`, storeAs: 'profile' }
  ]),
  ProfileModalHandler,
  connect(({ firebase }, { params }) => ({
    profile: firebase.data.profile,
    firebase: firebase,
    loggedIn: !firebase.auth.isEmpty,
    me: firebase.auth.uid,
    id: params.id
  })),
  spinnerWhileLoading(['profile'])
)
