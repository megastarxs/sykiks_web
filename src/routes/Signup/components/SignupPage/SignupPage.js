import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import { LOGIN_PATH } from 'constants'
import SignupForm from '../SignupForm'
import './SignupPage.scss'
import enhance from './SignupPage.enhancer'

export const SignupPage = ({
  emailSignup,
  googleLogin,
  onSubmitFail,
  router,
  updateImage,
  captchaVerified,
  file
}) => (
  <div className="container">
    <title>Sykiks - Register for an Account</title>
    <div className="header-div register-page">
      {router.params.type == 'client' && (
        <div>
          <i className="icon ion-ios-contact psychic-header-icon" />
          <h3 className="purple-text">Register as a Client</h3>
          <h5 className="grey-text">
            Fill out the form below to speak with a Reader.<br />
            If you'd like to offer your services as a reader{' '}
            <Link to="/signup/psychic">
              Register as a Psychic{' '}
              <i className="icon ion-ios-arrow-dropright-circle" />
            </Link>
          </h5>
        </div>
      )}
      {router.params.type == 'psychic' && (
        <div>
          <i className="icon ion-ios-flower psychic-header-icon" />
          <h3 className="purple-text">Register as a Psychic</h3>
          <h5 className="grey-text">
            Fill out the form below to render services as a Reader. <br />
            If you'd like to speak with a psychic instead&nbsp;
            <Link to="/signup/client">
              Register as Client{' '}
              <i className="icon ion-ios-arrow-dropright-circle" />
            </Link>
          </h5>
        </div>
      )}
    </div>
    <hr />
    <SignupForm
      onSubmit={emailSignup}
      onSubmitFail={onSubmitFail}
      type={router.params.type}
      file={file}
      updateImage={updateImage}
      captchaVerified={captchaVerified}
    />

    <div className="text-center offset-sm-6">
      {router.params.type == 'client' && (
        <div>
          <Link to="/signup/psychic">Register as Psychic</Link>
        </div>
      )}
      {router.params.type == 'psychic' && (
        <div>
          <Link to="/signup/client">Register as Client</Link>
        </div>
      )}

      <div>
        <span>Already have an account?</span>
        <div>
          <Link to={LOGIN_PATH}>Login</Link>
        </div>
      </div>
    </div>
  </div>
)

SignupPage.propTypes = {
  emailSignup: PropTypes.func, // from enhancer (withHandlers - firebase)
  googleLogin: PropTypes.func, // from enhancer (withHandlers - firebase)
  onSubmitFail: PropTypes.func // from enhancer (reduxForm)
}

export default enhance(SignupPage)
