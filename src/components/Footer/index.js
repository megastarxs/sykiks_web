import React from 'react'
import './Footer.scss'
import Icon from 'components/Icon'
import logoImage from 'static/header1.png'
// import appleImage from 'static/images/apple-store.png'
// import playstoreImage from 'static/images/play-store.png'
import { Link } from 'react-router'

const Footer = () => (
  <div>
    <footer className="footer1">
      <div className="container">
        <div className="row">
          <div className="col-lg-3 col-md-3">
            <ul className="list-unstyled clear-margins">
              <li className="widget-container widget_nav_menu">
                <p className="title-widget">Connect</p>
                <div className="social-icons">
                  <ul className="nomargin">
                    <a href="https://www.facebook.com/sykiks" target="_blank">
                      <Icon name="logo-facebook" className="icon facebook" />
                    </a>
                    <a href="https://twitter.com/sykiks" target="_blank">
                      <Icon name="logo-twitter" className="icon twitter" />
                    </a>
                    <a href="https://www.instagram.com/sykiks" target="_blank">
                      <Icon name="logo-instagram" className="icon instagram" />
                    </a>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
          <div className="col-lg-2 col-md-2">
            <ul className="list-unstyled clear-margins">
              <li className="widget-container widget_nav_menu">
                <p className="title-widget">Legal</p>
                <ul>
                  <li>
                    <Link to="/pages/privacy_policy">Privacy Policy</Link>
                  </li>
                  <li>
                    <Link to="/pages/terms_of_service">Terms of service</Link>
                  </li>
                  <li>
                    <Link to="/pages/disclaimer">Disclaimer</Link>
                  </li>
                  <li className="d-none">
                    <Link to="/pages/refund_policy">Refund Policy</Link>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <div className="col-lg-2 col-md-2">
            <ul className="list-unstyled clear-margins">
              <li className="widget-container widget_nav_menu">
                <p className="title-widget">Company</p>
                <ul>
                  <li>
                    <Link to="/contactus">Contact Us</Link>
                  </li>
                  <li className="d-none">
                    <Link to="/pages/about">About</Link>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <div className="col-lg-3 col-md-3 footer-column">
            {/* <img src={appleImage} className="footer-logo" />
            <img src={playstoreImage} className="footer-logo" /> */}
          </div>
          <div className="col-lg-2 col-md-2 footer-column">
            <img src={logoImage} className="footer-logo" />
          </div>
        </div>
      </div>
    </footer>
  </div>
)

export default Footer
