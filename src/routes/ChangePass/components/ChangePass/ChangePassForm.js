import React from 'react'
import { Field } from 'redux-form'
import FieldInput from 'components/FieldInput'
import { Button, Form } from 'reactstrap'
import './ChangePass.scss'
import { required } from 'utils/form'
import enhancer from './formEnhancer'
const ChangePassForm = ({ pristine, submitting, handleSubmit }) => (
  <Form
    onSubmit={handleSubmit}
    className="form-design form-panel form-changepass">
    <Field
      type="password"
      name="oldPass"
      component={FieldInput}
      placeholder="Old Password..."
      className="col-12"
      validate={[required]}
    />
    <Field
      name="newPass"
      type="password"
      component={FieldInput}
      placeholder="New Password..."
      className="col-12"
      validate={[required]}
    />
    <Field
      name="confirmPass"
      type="password"
      component={FieldInput}
      placeholder="Confirm Password..."
      className="col-12"
      validate={[required]}
    />

    <div className="offset-4">
      <Button
        className="btn-lg "
        color="primary"
        type="submit"
        variant="raised"
        disabled={pristine || submitting}>
        {submitting ? 'Loading' : 'Submit Now'}
      </Button>
    </div>
  </Form>
)

export default enhancer(ChangePassForm)
