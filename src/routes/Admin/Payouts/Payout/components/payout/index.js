import payout from './payout'
import enhance from './payout.enhancer'

export default enhance(payout)
