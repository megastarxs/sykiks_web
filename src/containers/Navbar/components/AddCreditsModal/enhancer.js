import { withStateHandlers } from 'recompose'

export default withStateHandlers(
  { loading: false },
  {
    addCredits: (a, { auth }) => amount => {
      location.href = `/api/pay/${auth.uid}/${amount}`
      return { loading: true }
    }
  }
)
