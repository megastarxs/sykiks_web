import * as admin from 'firebase-admin'
import initPaypal from '../paypal_config'
import { now } from '../helpers'

function dec(num) {
  var p = 100
  return Math.round(num * p) / p
}

function getNextPayoutDate() {
  var today = new Date()
  let requiredDate = new Date(
    today.getFullYear(),
    today.getMonth(),
    today.getDate() + 15
  )
  return requiredDate
}

export default app => {
  let usersRef = admin.database().ref('users')
  let settingsRef = admin.database().ref('settings')
  let payoutRef = admin.database().ref('payout')
  let masterConfigRef = admin.database().ref('master-config')

  app.get('/check-payout', async (req, res) => {
    let paypal = await initPaypal()
    let lastPayoutId = await masterConfigRef.child('lastPayoutId').once('value')
    lastPayoutId = lastPayoutId.val()
    paypal.payout.get(lastPayoutId, function(error, payout) {
      if (error) {
        console.log(error)
        res.json(error)
      } else {
        payout.items.forEach(d => {
          settingsRef
            .child(`${d.payout_item.sender_item_id}/payouts/${lastPayoutId}`)
            .update({ status: d.transaction_status, date: now() })
        })

        res.json({ ok: true })
      }
    })
  })
  app.get('/payout', async (req, res) => {
    let paypal = await initPaypal()

    var today = new Date().toLocaleDateString('en-US', {
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    })

    let percentageCut = await masterConfigRef
      .child('percentageCut')
      .once('value')
    percentageCut = percentageCut.val() / 100
    let percentageGive = 1 - percentageCut

    var senderBatchId = Math.random()
      .toString(36)
      .substring(9)
    usersRef
      .orderByChild('type')
      .equalTo('psychic')
      .once('value', function(snapshot) {
        let usersVal = snapshot.val()

        let promises = Object.keys(usersVal).map(d =>
          settingsRef
            .child(`${d}/accountInfo`)
            .once('value')
            .then(s => {
              usersVal[d].accountInfo = s.val()
              return 5
            })
        )
        Promise.all(promises).then(function(values) {
          let users = Object.keys(usersVal).map(key => ({
            key,
            screenName: usersVal[key].screenName,
            balance:
              usersVal[key].accountInfo &&
              dec(usersVal[key].accountInfo.balance * percentageGive),
            fees:
              usersVal[key].accountInfo &&
              dec(usersVal[key].accountInfo.balance * percentageCut),
            paypalId:
              usersVal[key].accountInfo && usersVal[key].accountInfo.paypalId
          }))

          users = users.filter(d => d.balance && d.paypalId)

          let items = users.map(d => ({
            recipient_type: 'EMAIL',
            amount: {
              value: d.balance,
              currency: 'USD'
            },
            receiver: d.paypalId,
            note: `Thank you, ${d.screenName} for your contributions`,
            sender_item_id: d.key
          }))
          var createPayoutJson = {
            sender_batch_header: {
              sender_batch_id: senderBatchId,
              email_subject: `Sykiks - Your payout for ${today}`
            },
            items
          }

          let nextPayoutDate = getNextPayoutDate()
          paypal.payout.create(createPayoutJson, true, function(error, payout) {
            if (error) {
              res.json(error.response)
            } else {
              let payoutId = payout.batch_header.payout_batch_id
              let paidRef = payoutRef.child(payoutId)
              let paidToRef = paidRef.child('paidTo')
              masterConfigRef.update({ lastPayoutId: payoutId })

              let totalPaid = 0
              let totalFees = 0
              users.forEach(d => {
                totalPaid += d.balance
                totalFees += d.fees
                paidToRef.push(d)
                let user = settingsRef.child(d.key)
                user.child('accountInfo').update({ balance: 0, nextPayoutDate })
                let obj = {}
                obj[payoutId] = {
                  balance: d.balance,
                  fees: d.fees,
                  date: now(),
                  status: 'Pending'
                }
                user.child('payouts').update(obj)
              })

              paidRef.update({ totalPaid, totalFees, date: now() })

              res.json({ ok: true, payoutId })
            }
          })
        })
      })
  })
}
