import React from 'react'
import { Modal, ModalBody, Card, CardBody, Button } from 'reactstrap'
import enhancer from './enhancer'
import _ from 'lodash'
import { AvatarByUid } from 'components/Avatar'
import ScreenName from 'components/ScreenName'
import '../Chat.scss'
let ClientChatRequest = ({ chatRequests, removeRequest }) => {
  let open = _.isEmpty(chatRequests)
  if (open) return null
  let requestId = Object.keys(chatRequests)[0]
  let request = chatRequests[requestId]

  return (
    <Modal isOpen={!open}>
      <ModalBody>
        <Card>
          <CardBody className="text-center client-chat-request">
            {request.status == 'init' && (
              <div className="text">
                We are connecting you to{' '}
                <span className="purple-text">
                  <ScreenName uid={request.psychic} />
                </span>. <p>Please wait...</p>
              </div>
            )}
            {request.status == 'rejected' && (
              <div className="text">
                <span className="purple-text">
                  <ScreenName uid={request.psychic} />
                </span>{' '}
                is not available.
              </div>
            )}
            <div className="text">
              <AvatarByUid uid={request.psychic} className="avt-image" />
            </div>
            <Button
              onClick={() => removeRequest(requestId)}
              className="light-purple-button">
              Cancel
            </Button>
          </CardBody>
        </Card>
      </ModalBody>
    </Modal>
  )
}
export default enhancer(ClientChatRequest)
