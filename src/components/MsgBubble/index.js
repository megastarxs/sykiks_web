import React from 'react'
import { AvatarByUid } from '../Avatar'
import { FormattedNumber, FormattedTime } from 'react-intl'
import './MsgBubble.scss'
import Icon from 'components/Icon'
import ProfileModal from 'components/ProfileModal'
import enhance from './enhancer'

const bubbleDirection = (from, me) =>
  from != 'system' ? (me == from ? 'right' : 'left') : 'system'

export const MsgBubble = ({
  uid,
  profile,
  direction,
  auth,
  time,
  children,
  msgRate,
  profileId,
  open,
  updateProfileModal,
  serverTimeOffset,
  closeModal
}) => {
  if (!direction) direction = bubbleDirection(uid, auth.uid)
  return (
    <div>
      {direction != 'system' && (
        <li className={[direction == 'left' ? 'sent' : 'sent-replies']}>
          <button
            onClick={() => updateProfileModal(uid)}
            className="avatar-button">
            <AvatarByUid uid={uid} />
          </button>

          <div className="msg-content">
            {children.audio ? (
              <audio
                src={children.audio}
                controls
                controlsList=" notimeline nodownload"
              />
            ) : (
              children
            )}
            <div className="time-display">
              {msgRate > 0 && (
                <span
                  className={
                    profile.type == 'psychic' ? 'plusRate' : 'minusRate'
                  }>
                  <FormattedNumber
                    style="currency"
                    currency="USD"
                    value={profile.type == 'psychic' ? msgRate : -1 * msgRate}
                  />{' '}
                  |{' '}
                </span>
              )}
              <FormattedTime value={time - (serverTimeOffset || 0)} />
            </div>
          </div>
        </li>
      )}
      {direction == 'system' && (
        <li className="system-message text-center">
          <div className="msg-content">
            <Icon name="md-information-circle" />
            {children}
          </div>
        </li>
      )}
      <ProfileModal
        key={profileId}
        uid={profileId}
        open={open}
        onClose={() => closeModal()}
      />
    </div>
  )
}
export default enhance(MsgBubble)
