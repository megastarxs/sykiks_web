import React from 'react'
import Subheader from 'components/Subheader'
import ChatBubble from 'components/ChatBubble'
import { AvatarByUid } from 'components/Avatar'
import './ChatHistory.scss'
import { FormattedTime, FormattedDate, FormattedNumber } from 'react-intl'
import Icon from 'components/Icon'
import ScreenName from 'components/ScreenName'
import { Link } from 'react-router'
export const ChatHistory = ({ chats, chatSession, profile, auth }) => (
  <div className="chathistory frame container">
    <div className="content">
      <Subheader subheader="Your Chat" />
      <div className="chat-session-detail">
        <div className="user-detail">
          <AvatarByUid
            divClassName="col-lg-1"
            uid={
              chatSession.client == auth.uid
                ? chatSession.psychic
                : chatSession.client
            }
          />

          <div className="float-left user-info">
            <div className="user-name">
              {chatSession.client == auth.uid ? (
                <ScreenName uid={chatSession.psychic} />
              ) : (
                <ScreenName uid={chatSession.client} />
              )}
            </div>
            <div>
              <span className="title">Duration : </span>
              <span className="value">{chatSession.sessionTime}</span>
              <span className="title"> Amount : </span>
              <span className="value">
                <FormattedNumber
                  style="currency"
                  currency="USD"
                  value={chatSession.amount - 0}
                />
              </span>
              {profile.type == 'client' &&
                !chatSession.reviewed && (
                  <span>
                    {' '}
                    <Link
                      to={`/review/${
                        chatSession.client == auth.uid
                          ? chatSession.psychic
                          : chatSession.client
                      }/${chatSession.sessionKey}`}>
                      Add a review
                    </Link>{' '}
                  </span>
                )}
            </div>
          </div>
        </div>
      </div>

      <div className="messages container">
        <ul>
          <li className="system-message text-center">
            <div className="msg-content">
              <Icon name="md-calendar" />
              <FormattedDate
                value={chatSession.acceptedAt}
                year="numeric"
                month="long"
                day="2-digit"
              />
              {'    '}
              <FormattedTime value={chatSession.acceptedAt} />
            </div>
          </li>
          {Object.keys(chats).map((key, index) => {
            let chatData = chats[key]
            let uid = chatData.from
            let time = chatData.time
            return (
              <div key={key}>
                <ChatBubble uid={uid} key={key} time={time}>
                  {chatData.message}
                </ChatBubble>
              </div>
            )
          })}
        </ul>
      </div>
    </div>
  </div>
)

export default ChatHistory
