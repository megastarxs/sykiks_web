import React from 'react'
import { Link } from 'react-router'

let AdminModal = ({ user, myId, banUser, unbanUser, userId }) => {
  let url = `/messages/admin-${userId}`
  if (user.type == 'client') url = `/messages/${userId}-admin`
  return (
    <div>
      {!user.banned && (
        <button className="btn btn-lg purple-button" onClick={banUser}>
          Ban User
        </button>
      )}
      {user.banned && (
        <button className="btn btn-lg purple-button" onClick={unbanUser}>
          UnBan User
        </button>
      )}
      <a
        href={'mailto:' + user.email}
        className="btn btn-lg light-purple-button">
        Send Mail
      </a>
      <Link to={url}>Send Message</Link>
    </div>
  )
}
export default AdminModal
