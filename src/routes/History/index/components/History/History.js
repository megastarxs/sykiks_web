import React from 'react'
import { Table, Card } from 'reactstrap'
import { FormattedTime, FormattedDate, FormattedNumber } from 'react-intl'
import Subheader from 'components/Subheader'
import { Link } from 'react-router'
import DefaultMessage from 'components/DefaultMessage'
import Pagination from 'components/Pagination'
import ScreenName from 'components/ScreenName'
function History({
  chatSessions,
  profile,
  slicedData,
  setData,
  percentageCut
}) {
  var partnerKey = profile.type == 'client' ? 'psychic' : 'client'

  let chatSessionsTable = []

  if (chatSessions) {
    chatSessionsTable = Object.keys(chatSessions)
      .reverse()
      .map((key, index) => {
        chatSessions[key].key = key
        return chatSessions[key]
      })
  }

  return (
    <div className="container">
      <Subheader subheader="Session History" />
      {!chatSessions && (
        <DefaultMessage>
          No Sessions attended. Your Session History will be seen here.
        </DefaultMessage>
      )}
      {chatSessions && (
        <Card>
          <Table responsive bordered hover className="text-center">
            <thead>
              <tr>
                <th>Date</th>
                <th>{profile.type == 'client' ? 'Psychic' : 'Client'}</th>
                <th> Conversation Time </th>
                <th>
                  {`${
                    profile.type == 'client'
                      ? 'Amount Spend'
                      : 'Amount Received'
                  }`}
                </th>
                {profile.type == 'psychic' && <th>Sykiks Fees</th>}
              </tr>
            </thead>
            <tbody>
              {slicedData &&
                slicedData.map(chatSession => {
                  return (
                    <tr key={chatSession.key}>
                      <td>
                        <Link to={`/history/${chatSession.sessionKey}`}>
                          <FormattedDate
                            value={chatSession.acceptedAt}
                            year="numeric"
                            month="long"
                            day="2-digit"
                          />
                          <br />
                          <FormattedTime value={chatSession.acceptedAt} />
                        </Link>
                      </td>
                      <td>
                        <ScreenName uid={chatSession[partnerKey]} />
                      </td>
                      <td>
                        {chatSession.sessionTime}
                        {!chatSession.sessionTime && 'Session did not start'}
                      </td>
                      <td
                        style={{
                          color: [profile.type == 'client' ? 'red' : 'green']
                        }}>
                        <FormattedNumber
                          style="currency"
                          currency="USD"
                          value={chatSession.amount - 0}
                        />
                      </td>

                      {profile.type == 'psychic' && (
                        <td
                          style={{
                            color: 'red'
                          }}>
                          <FormattedNumber
                            style="currency"
                            currency="USD"
                            value={
                              ((chatSession.amount - 0) * percentageCut) / 100
                            }
                          />
                        </td>
                      )}
                    </tr>
                  )
                })}
            </tbody>
          </Table>
        </Card>
      )}
      <Pagination data={chatSessionsTable} setData={setData} />
    </div>
  )
}

export default History
