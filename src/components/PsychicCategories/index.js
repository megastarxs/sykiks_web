import React, { Component } from 'react'
import { FormGroup, Label } from 'reactstrap'
import Select from 'react-select'
import './PsychicCategories.scss'
import { CATEGORIES_LIST } from 'constants'

let categoriesList = CATEGORIES_LIST.map(d => ({ name: d }))

class categoriesSelect extends Component {
  state = { categoryValue: [] }

  render() {
    let { name, label, className, input } = this.props

    return (
      <FormGroup className={className}>
        <Label for={name}>{label}</Label>
        <Select
          name={name}
          id={name}
          value={input.value}
          onChange={input.onChange}
          options={categoriesList}
          multi
          simpleValue
          clearable
          searchable
          labelKey="name"
          valueKey="name"
          delimiter=", "
        />
      </FormGroup>
    )
  }
}

export default categoriesSelect
