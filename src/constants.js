export const LIST_PATH = '/'
export const ACCOUNT_PATH = '/account'
export const LOGIN_PATH = '/login'
export const PSYCHIC_PATH = '/psychic-list'
export const SIGNUP_PATH_INDEX = '/signup'
export const SIGNUP_PATH = '/signup/:type'
export const CONTACTUS_PATH = '/contactus'
export const CHANGEPASS_PATH = '/change-pass'
export const ACCOUNT_FORM_NAME = 'account'
export const LOGIN_FORM_NAME = 'login'
export const SIGNUP_FORM_NAME = 'signup'
export const CONTACT_FORM_NAME = 'contact'
export const CHANGEPASS_FORM_NAME = 'changepassword'
export const PROFILE_PATH = '/profile/:id'
export const ADDFUNDS_PATH = '/addfunds'
export const MESSAGES_PATH = '/messages(/:roomId)'
export const CONVERSATION_PATH = '/messages/:id'
export const HISTORY_PATH = '/history'
export const CHATHISTORY_PATH = '/history/:id'
export const ADMIN_PAYOUTS_PATH = '/admin/payouts'
export const ADMIN_CONTACTUS_PATH = '/admin/contactus/:id'
export const ADMIN_APPROVEUSER_PATH = '/admin/approve-user'
export const ADMIN_USERS_PATH = '/admin/users/:id'
export const PAYOUT_PATH = '/admin/payouts/:id'
export const PENDING_PAYOUT_PATH = '/admin/payouts/pending'
export const PAYOUTS_PATH = '/payouts'
export const PSYCHICS_PATH = '/PSYCHICS'
export const REPORTS_PATH = '/admin/reports'
export const MASTERCONFIG_PATH = '/admin/config'
export const PAGES_PATH = '/pages/:id'
export const PAYMENT_PATH = '/payment'
export const REVIEW_PATH = '/review/:id/:sessionid'
export const FORGOTPASS_PATH = '/forgot-pass'

export const formNames = {
  account: ACCOUNT_FORM_NAME,
  signup: SIGNUP_FORM_NAME,
  login: LOGIN_FORM_NAME,
  contact: CONTACT_FORM_NAME
}

export const paths = {
  list: LIST_PATH,
  account: ACCOUNT_PATH,
  login: LOGIN_PATH,
  signup: SIGNUP_PATH
}

export const CATEGORIES_LIST = [
  'Psychic Readings',
  'Tarot',
  'Spiritual counselling',
  'Astrology',
  'Dream Interpretation',
  'Love and Relationships',
  'Numerology'
]

export default { ...paths, ...formNames }
