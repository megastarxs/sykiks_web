import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStateHandlers } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'

export default compose(
  firebaseConnect(({ pair: { client } }, firebase) => [
    {
      path: 'chat-sessions',
      queryParams: ['orderByChild=client', 'equalTo=' + client],
      storeAs: `chatSessions_${client}`
    }
  ]),
  connect(({ firebase: { data } }, { pair: { client } }) => ({
    chatSessions: data[`chatSessions_${client}`]
  })),
  withStateHandlers(
    { sessionKey: null },
    { setChatId: () => sessionKey => ({ sessionKey }) }
  ),
  spinnerWhileLoading(['chatSessions'])
)
