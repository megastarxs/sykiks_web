import React from 'react'
import './content.scss'
export default (
  <div className="page-content">
    <p>
      {' '}
      Sykiks.com respects its users confidentiality. Your personal information
      will be kept completely confidential from other advisors and members.{' '}
    </p>
    <p>
      Sykiks.com will not disclose any personal information obtained from you to
      third paties{' '}
    </p>
    <p>
      On registering with sykiks.com you will be required to input sensitive
      data which includes your email address, age, telephone number and years of
      experience. This information is kept completely private and only your
      email address will be seen by the advisors during payment transactions.{' '}
    </p>
    <p>
      You understand that your payment details are not collected or stored by
      sykiks.com. Your payment details (including debit or credit card
      information) are directly handled by independent payment gateways which
      feature on our platform{' '}
    </p>
    <p>
      Please understand that we only do business with secure and reputable
      payment gateways, you can trust that they will take care of your payment
      details, and will only use your information when facilitating payments to
      your selected advisors
    </p>
    <p>
      You understand that we may use your email address to send you updates
      regarding our site or to share other information that may be of benefit to
      you.{' '}
    </p>
    <p>
      We store information on a private database and use a variety of security
      measures to protect your data from unauthorized users or hackers.
    </p>
    <p>
      Sykiks.com allows you to view the information you have provided after
      registration and edit any information that exists in the data including
      sensitive information such as your email address and telephone number.{' '}
    </p>
    <p>
      Sykiks.com reserves the right to edit or update this Privacy Policy at any
      given time and without prior consent to its members and/or advisors{' '}
    </p>
  </div>
)
