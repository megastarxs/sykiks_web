import React from 'react'

export const Icon = ({ name, className }) => (
  <i className={`icon ion-${name} ${className || ''}`} />
)

export default Icon
