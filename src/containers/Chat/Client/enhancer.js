import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { withHandlers, lifecycle } from 'recompose'
import _ from 'lodash'
import Chat from 'utils/Chat'

export default compose(
  firebaseConnect((props, firebase) => [
    {
      path: 'chat-requests',
      queryParams: [
        'orderByChild=client',
        `equalTo=${firebase.firebase._.authUid}`
      ],
      storeAs: `chatRequestsClient`
    }
  ]),
  connect(({ firebase: { data, auth } }) => ({
    chatRequests: data.chatRequestsClient,
    auth
  })),
  withHandlers({
    removeRequest: ({ firebase }) => reqId => Chat.removeRequest(reqId)
  }),
  lifecycle({
    componentWillReceiveProps({ chatRequests, removeRequest, auth }) {
      if (_.isEmpty(chatRequests)) return
      let requestId = Object.keys(chatRequests)[0]
      let request = chatRequests[requestId]
      if (request.status == 'rejected') {
        setTimeout(() => {
          removeRequest(requestId)
        }, 3000)
      } else if (request.status == 'accepted') {
        Chat.setCurrentSessionKey(auth.uid, request.sessionKey)
        removeRequest(requestId)
      }
    }
  })
)
