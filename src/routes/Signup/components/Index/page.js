import React from 'react'
import { Link } from 'react-router'
import { LOGIN_PATH } from 'constants'
import './index.scss'

export const SignupPageIndex = () => (
  <div className="container text-center">
    <title>Sykiks - Register for an Account</title>

    <div className="row mt-5 mb-5">
      <div className="col-md-6 ">
        <i className="icon ion-ios-contact psychic-header-icon" />
        <h4 className="purple-text">Register as a Client</h4>
        <p className="grey-text mt-3">
          If you'd like to speak with a psychic reader{' '}
        </p>
        <Link
          to="/signup/client"
          className="btn btn-primary col-4 mt-3 font-weight-bold">
          Sign up
        </Link>
      </div>
      <div className="col-md-6">
        <i className="icon ion-ios-flower psychic-header-icon" />
        <h4 className="purple-text">Register as a Psychic</h4>
        <p className="grey-text mt-3">
          If you'd like to offer your services as a reader.<br />
        </p>
        <Link
          to="/signup/psychic"
          className="btn btn-primary col-4 mt-3 font-weight-bold">
          Sign up
        </Link>
      </div>
    </div>

    <div>
      <span>Already have an account?</span>&nbsp;
      <Link to={LOGIN_PATH} className="font-weight-bold">
        Login
      </Link>
    </div>
  </div>
)

export default SignupPageIndex
