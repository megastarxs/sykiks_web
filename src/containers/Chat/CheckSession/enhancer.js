import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, isEmpty } from 'react-redux-firebase'

export default compose(
  firebaseConnect((props, firebase) => [
    {
      path: `settings/${firebase.firebase._.authUid}/currentSessionKey`,
      storeAs: `currentSessionKey`
    }
  ]),
  connect(({ firebase: { data, profile } }) => ({
    currentSessionKey: data.currentSessionKey,
    profile,
    hasCurrentSession: !isEmpty(data.currentSessionKey)
  }))
)
