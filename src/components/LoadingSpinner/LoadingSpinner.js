import React from 'react'
import './LoadingSpinner.scss'

export const LoadingSpinner = ({ size }) => <div className="lds-dual-ring" />

export default LoadingSpinner
