import React from 'react'
import { Input, Label } from 'reactstrap'
const FieldInput = ({
  input,
  meta,
  type,
  placeholder,
  label,
  name,
  min,
  defaultValue,
  max,
  maxLength,
  rows,
  submitting,
  className
}) => (
  <div
    className={`${className || ''} ${
      type != 'checkbox' ? 'form-group' : 'form-check'
    }`}>
    {type != 'checkbox' && label && <Label for={name}>{label}</Label>}
    <Input
      type={type}
      checked={input.value}
      name={name}
      id={name}
      placeholder={placeholder}
      min={min}
      max={max}
      maxLength={maxLength}
      rows={rows}
      value={input.value || defaultValue || ''}
      onChange={input.onChange}
      className={meta.touched && meta.invalid ? 'is-invalid' : ''}
    />
    {type == 'checkbox' && <Label for={name}>{label}</Label>}
    <div className="invalid-feedback">{meta.error}</div>
  </div>
)

export default FieldInput
