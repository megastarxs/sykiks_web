import Messages from './Messages'
import enhance from './Messages.enhancer'

export default enhance(Messages)
