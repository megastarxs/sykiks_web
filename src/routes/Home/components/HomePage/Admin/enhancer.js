import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'

export default compose(
  firebaseConnect((props, firebase) => [
    {
      path: `adminReports`,
      storeAs: 'lastReport'
    },
    {
      path: `master-config/today`,
      storeAs: 'latestReport'
    },
    {
      path: `master-config/lastReportId`,
      storeAs: 'lastReportId'
    },
    {
      path: `master-config/percentageCut`,
      storeAs: 'percentageCut'
    }
  ]),
  connect(({ firebase: { data } }) => ({
    lastReport: data.lastReport,
    latestReport: data.latestReport,
    percentageCut: data.percentageCut,
    lastReportId: data.lastReportId
  })),
  spinnerWhileLoading([
    'lastReport',
    'lastReportId',
    'latestReport',
    'percentageCut'
  ])
)
