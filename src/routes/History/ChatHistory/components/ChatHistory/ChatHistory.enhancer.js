import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'
export default compose(
  UserIsAuthenticated,
  firebaseConnect((props, firebase) => [
    {
      path: `chats/${props.params.id}`,
      storeAs: `chats_${props.params.id}`
    },
    {
      path: `chat-sessions/${props.params.id}`,
      storeAs: `chatSession_${props.params.id}`
    }
  ]),
  connect(({ firebase: { data, profile, auth } }, props) => ({
    chats: data[`chats_${props.params.id}`],
    chatSession: data[`chatSession_${props.params.id}`],
    profile,
    auth
  })),
  spinnerWhileLoading(['chats', 'chatSession'])
)
