import React from 'react'
import { Field } from 'redux-form'
import { Button, Form } from 'reactstrap'
import FieldInput from 'components/FieldInput'
import { required, validateEmail } from 'utils/form'
import PsychicCategories from 'components/PsychicCategories'

export const AccountForm = ({
  profile,
  handleSubmit,
  submitting,
  pristine
}) => (
  <Form onSubmit={handleSubmit}>
    <div>
      <Field name="screenName" component={FieldInput} label="Display Name" />
      {profile.type == 'psychic' && (
        <div>
          <Field
            name="introduction"
            component={FieldInput}
            label="Introduction"
            maxLength={100}
          />
          <Field
            name="paypalId"
            component={FieldInput}
            validate={[required, validateEmail]}
            label="Paypal Id"
            rows="5"
          />
          <Field
            name="aboutme"
            component={FieldInput}
            label="About Me"
            type="textarea"
            rows="5"
          />
          <Field
            name="categories"
            component={PsychicCategories}
            label="Categories"
          />
          <div className="row">
            <Field
              name="chatRate"
              type="number"
              label="Chat Rate"
              component={FieldInput}
              className="mx-sm-3 mb-2"
            />
            <Field
              name="msgRate"
              type="number"
              label="Message Rate"
              component={FieldInput}
              className="mx-sm-3 mb-2"
              max="2"
            />
          </div>
        </div>
      )}
    </div>
    <Button
      className="btn btn-lg btn-info btn-block"
      type="submit"
      disabled={pristine || submitting}>
      {submitting ? 'Saving' : 'Save'}
    </Button>
  </Form>
)

export default AccountForm
