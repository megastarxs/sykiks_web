import React from 'react'
import { Table, Button } from 'reactstrap'
import { FormattedDate } from 'react-intl'
import ProfileModal from 'components/ProfileModal'
import Subheader from 'components/Subheader'
import './Users.scss'
import DefaultMessage from 'components/DefaultMessage'
import AccountBalance from '../AccountBalance'

let queryParams = {
  banned: 'Banned Users',
  clients: 'Clients',
  psychics: 'Psychics',
  admins: 'Admins'
}

function BannedUser({
  users,
  unBanUser,
  routeParams,
  setSearch,
  search,
  updateProfileModal,
  closeModal,
  profileId,
  open
}) {
  if (!users)
    return (
      <div className="container">
        <Subheader subheader={queryParams[routeParams.id]} />
        <DefaultMessage>No Users Listed Here.</DefaultMessage>}
      </div>
    )

  users = Object.keys(users)
    .map((key, index) => {
      users[key].key = key
      return users[key]
    })
    .sort((a, b) => b.createdAt - a.createdAt)

  if (search != '')
    users = users.filter(d =>
      d.screenName.toLowerCase().includes(search.toLowerCase())
    )

  return (
    <div className="container">
      <Subheader subheader={queryParams[routeParams.id]} />
      {users && (
        <div>
          <div className="row float-right">
            <div className="col">
              <div className="row align-items-center" style={{ margin: '1em' }}>
                <div className="col">
                  <input
                    className="form-control form-control-borderless"
                    type="search"
                    placeholder="Search"
                    onChange={setSearch}
                  />
                </div>
              </div>
            </div>
          </div>
          <Table responsive bordered hover className="text-center">
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Member Since</th>
                <th>Balance</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {users.map(user => {
                let userUrl = '/profile/' + user.key
                return (
                  <tr key={user.key}>
                    <td>
                      <Button
                        color="link"
                        onClick={() => updateProfileModal(user.key)}>
                        {user.screenName}
                      </Button>

                      {user.type == 'psychic' && (
                        <a href={userUrl} target="_blank">
                          <i> ({user.type}) </i>
                        </a>
                      )}
                    </td>
                    <td>
                      <a href={'mailto:' + user.email}>{user.email}</a>
                    </td>
                    <td>
                      {user.createdAt && (
                        <FormattedDate
                          value={user.createdAt}
                          year="numeric"
                          month="long"
                          day="2-digit"
                        />
                      )}
                    </td>
                    <td>
                      <AccountBalance uid={user.key} />
                    </td>
                    <td>
                      {user.banned && (
                        <button
                          className="btn btn-lg btn-primary"
                          onClick={() => unBanUser(user.key)}>
                          Unban
                        </button>
                      )}
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </Table>
        </div>
      )}
      <ProfileModal
        key={profileId}
        uid={profileId}
        open={open}
        onClose={() => closeModal()}
      />
    </div>
  )
}

export default BannedUser
