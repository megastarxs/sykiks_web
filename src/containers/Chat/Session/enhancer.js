import _ from 'lodash'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { withStateHandlers, lifecycle, withHandlers } from 'recompose'
import { withRouter, spinnerWhileLoading } from 'utils/components'
import { withNotifications } from 'modules/notification'
import Chat from 'utils/Chat'

let textRef, scrollRef
let autoStartSessionIn = 3 * 60
// let autoStartSessionIn = 30

export default compose(
  // UserIsAuthenticated,
  firebaseConnect(({ sessionKey, ouner }, firebase) => [
    { path: `chats/${sessionKey}`, storeAs: `chats_${sessionKey}` },
    {
      path: `settings/${firebase.firebase._.authUid}/accountInfo/balance`,
      storeAs: 'balance'
    },
    { path: `.info/serverTimeOffset`, storeAs: `serverTimeOffset` },
    {
      path: 'chat-pairs',
      queryParams: [
        'orderByChild=' + ouner.type,
        'equalTo=' + firebase.firebase._.authUid
      ],
      storeAs: `chatPairs`
    },
    {
      path: `chat-sessions/${sessionKey}`,
      storeAs: `chat-session_${sessionKey}`
    }
  ]),
  connect(({ firebase: { data, profile, auth } }, { sessionKey }) => ({
    chat: data[`chats_${sessionKey}`],
    session: data[`chat-session_${sessionKey}`],
    balance: data.balance,
    chatPairs: data.chatPairs,
    serverTimeOffset: data.serverTimeOffset,
    profile: profile,
    from: auth.uid,
    me: auth.uid,
    sessionKey
  })),
  withRouter,
  withNotifications,
  lifecycle({
    componentDidMount() {
      setTimeout(() => {
        if (scrollRef) scrollRef.scrollIntoView({ behavior: 'smooth' })
      }, 1000)
    },
    componentWillUpdate(nextProps) {
      let current = _.last(_.keys(this.props.chat))
      let next = _.last(_.keys(nextProps.chat))

      if (current !== next) {
        setTimeout(() => {
          if (scrollRef) scrollRef.scrollIntoView({ behavior: 'smooth' })
        }, 1000)
      }

      let {
        profile,
        balance,
        session,
        showError,
        sessionKey,
        sessionTime
      } = nextProps
      if (
        profile.type == 'client' &&
        session &&
        session.sessionStarted &&
        !session.sessionEnded &&
        balance < session.chatRate / 2
      ) {
        if (!sessionTime || sessionTime == '')
          sessionTime = 'Paid session did not start'

        showError(
          "You don't have enough credits to continue this chat session.\n Consider adding credits to continue the session"
        )

        Chat.systemMessage({ sessionKey, message: 'Session has ended' })
        Chat.updateSession(sessionKey, {
          sessionEnded: true,
          creditsEmpty: true,
          sessionTime
        })
      }
    }
  }),
  withHandlers({
    startTyping: ({ profile, sessionKey, showError }) => () => {
      let obj = {}
      obj[profile.type] = true
      Chat.updateSession(sessionKey, { typing: obj })
    },
    stopTyping: ({ profile, sessionKey }) => () => {
      let obj = {}
      obj[profile.type] = false
      Chat.updateSession(sessionKey, { typing: obj })
    },
    sendChat: ({ sessionKey, from }) => props => {
      let messageToBeSent = false
      if (props && props.audio) messageToBeSent = { audio: props.audio }
      else if (textRef.value != '') messageToBeSent = textRef.value
      if (messageToBeSent)
        Chat.sendChat({ sessionKey, message: messageToBeSent, from })
      textRef.value = ''
    },
    perHalfMinute: ({
      sessionKey,
      session,
      profile,
      balance,
      me,
      showError
    }) => (seconds, sessionTime) => {
      if (profile.type == 'client') {
        if (balance < session.chatRate / 2) {
          showError(
            "You don't have enough credits to continue this chat session.\n Consider adding credits to continue the session"
          )
          Chat.systemMessage({ sessionKey, message: 'Session has ended' })
          Chat.updateSession(sessionKey, {
            sessionEnded: true,
            sessionTime
          })
          return
        }
        seconds = seconds + 30
        let amount = (session.chatRate * seconds) / 60 // eslint-disable-line
        Chat.updateSession(sessionKey, { amount })
      }
    },
    registerTextRef: () => ref => {
      textRef = ref
    },
    registerScrollRef: () => ref => {
      scrollRef = ref
    },
    startSession: ({ sessionKey, session, profile }) => () => {
      if (profile.type == 'psychic')
        if (
          !session ||
          (session && !session.sessionStarted && !session.sessionEnded)
        ) {
          Chat.startSession(sessionKey, session.chatRate / 2)
        }
    }
  }),
  withStateHandlers(
    { sessionTime: '', autoStartSessionIn, record: false },
    {
      setSessionTime: () => sessionTime => ({
        sessionTime
      }),
      openRecorder: () => e => ({
        record: true
      }),
      closeRecorder: () => e => ({
        record: false
      }),
      endSession: (
        { sessionTime },
        { sessionKey, session, profile, me, router }
      ) => addFund => {
        if (!confirm('Are you sure you want to leave this session')) return
        if (!session.sessionEnded) {
          Chat.systemMessage({ sessionKey, message: 'Session has ended' })
          Chat.updateSession(sessionKey, {
            sessionEnded: true,
            sessionTime
          })
        }
        Chat.endCurrentSession(me, profile.type == 'psychic')

        if (profile.type == 'client') {
          if (addFund == 'addcredit') {
            window.open('/addfunds', '_blank')
          } else {
            router.push(`review/${session.psychic}/${sessionKey}`)
          }
        }
      }
    }
  ),
  spinnerWhileLoading(['chat', 'session'])
)
