import { connect } from 'react-redux'
import {
  withHandlers,
  compose,
  withProps,
  flattenProp,
  withStateHandlers
} from 'recompose'
import { withFirebase, isEmpty, isLoaded } from 'react-redux-firebase'
import { ACCOUNT_PATH } from 'constants'
import { withRouter } from 'utils/components'

export default compose(
  withFirebase, // add props.firebase (firebaseConnect() can also be used)
  connect(({ config, firebase: { auth, profile } }) => ({
    auth,
    profile,
    config
  })),
  withRouter,
  // Wait for auth to be loaded before going further
  withStateHandlers(
    ({ accountMenuOpenInitially = false }) => ({
      accountMenuOpen: accountMenuOpenInitially,
      isOpen: false,
      openCreditsModal: false,
      anchorEl: null
    }),
    {
      closeAccountMenu: ({ accountMenuOpen }) => () => ({
        anchorEl: null
      }),
      toggle: ({ isOpen }) => () => ({
        isOpen: !isOpen
      }),
      toggleCreditsModal: ({ openCreditsModal }) => () => ({
        openCreditsModal: !openCreditsModal
      }),
      handleMenu: () => event => ({
        anchorEl: event.target
      })
    }
  ),
  // Handlers
  withHandlers({
    handleLogout: ({ firebase, router, auth, config }) => () => {
      let endRoute = '/'
      if (config.search) endRoute = '/login' + config.search
      if (confirm('Are you sure, you want to logout?')) {
        firebase.remove(`users/${auth.uid}/online`)
        firebase.logout().then(d => (location.href = endRoute))
        // router.push('/')
      }
    },
    goToAccount: props => () => {
      props.router.push(ACCOUNT_PATH)
      props.closeAccountMenu()
    },
    updateStatus: ({ firebase }) => event => {
      let status = event.target.value
      firebase.updateProfile({ status })
    }
  }),
  withProps(({ auth, profile }) => ({
    authExists: isLoaded(auth) && !isEmpty(auth)
  })),
  // Flatten profile so that avatarUrl and displayName are available
  flattenProp('profile')
)
