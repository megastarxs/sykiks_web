import { withFirebase } from 'react-redux-firebase'
import { withHandlers, pure, compose, withStateHandlers } from 'recompose'
import { withNotifications } from 'modules/notification'
import { now } from 'utils/helpers'
export default compose(
  withNotifications, // add props.showError
  withFirebase, // add props.firebase (firebaseConnect() can also be used)
  // Handlers
  withStateHandlers(
    { captcha: false },
    {
      captchaVerified: () => () => ({
        captcha: true
      })
    }
  ),
  withHandlers({
    submit: ({ firebase, showSuccess, showError, router, captcha }) => form => {
      let name = form.fullName
      let email = form.email
      let msg = form.message == '' ? 'No Message' : form.message
      if (!captcha) return showError('Please check the reCaptcha')
      firebase
        .ref(`contactus`)
        .push({
          name: name,
          email: email,
          message: msg,
          time: now
        })
        .then(
          () =>
            showSuccess(
              `Thank you for contacting. Someone from our support team will respond to
          you within 24 hours. `
            ),
          router.push('/')
        )
    }
  }),
  pure // shallow equals comparison on props (prevent unessesary re-renders)
)
