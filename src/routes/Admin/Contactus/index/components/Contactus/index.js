import Contactus from './Contactus'
import enhance from './Contactus.enhancer'

export default enhance(Contactus)
