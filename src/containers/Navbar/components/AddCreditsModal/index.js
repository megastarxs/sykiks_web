import React from 'react'
import {
  Button,
  Modal,
  ModalBody,
  Card,
  CardBody,
  CardTitle,
  CardDeck
} from 'reactstrap'
import enhancer from './enhancer'
let AddCreditsModal = ({ isOpen, toggle, auth, addCredits, loading }) => (
  <Modal isOpen={isOpen} toggle={toggle}>
    <ModalBody>
      {!loading ? (
        <CardDeck>
          <Card>
            <CardBody onClick={() => addCredits(15)}>
              <CardTitle>$15</CardTitle>
              <Button color="primary">Buy</Button>
            </CardBody>
          </Card>
          <Card>
            <CardBody onClick={() => addCredits(50)}>
              <CardTitle>$50</CardTitle>
              <Button color="primary">Buy</Button>
            </CardBody>
          </Card>
          <Card>
            <CardBody onClick={() => addCredits(100)}>
              <CardTitle>$100</CardTitle>
              <Button color="primary">Buy</Button>
            </CardBody>
          </Card>
        </CardDeck>
      ) : (
        <h3> Please wait.. </h3>
      )}
    </ModalBody>
  </Modal>
)
export default enhancer(AddCreditsModal)
