import { compose, withHandlers, withStateHandlers } from 'recompose'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated, UserIsAdmin } from 'utils/router'
import { withNotifications } from 'modules/notification'
import { ProfileModalHandler } from 'components/ProfileModal'

export default compose(
  // create listener for admin, results go into redux
  UserIsAuthenticated,
  UserIsAdmin,
  firebaseConnect(({ routeParams }) => {
    let queryParams = {
      banned: ['orderByChild=banned', 'equalTo=' + true],
      clients: ['orderByChild=type', 'equalTo=client'],
      psychics: ['orderByChild=type', 'equalTo=psychic'],
      admins: ['orderByChild=type', 'equalTo=admin']
    }
    return [
      {
        path: 'users',
        queryParams: queryParams[routeParams.id],
        storeAs: `adminUsers` + routeParams.id
      }
    ]
  }),
  withNotifications,
  // map redux state to props
  connect(({ firebase: { data } }, { routeParams }) => ({
    users: data[`adminUsers` + routeParams.id]
  })),
  withStateHandlers(
    { search: '' },
    {
      setSearch: () => e => ({ search: e.target.value })
    }
  ),
  withHandlers({
    unBanUser: ({ firebase, showSuccess }) => uid => {
      firebase
        .ref(`users/${uid}/banned`)
        .remove()
        .then(user => showSuccess('User has been Un Banned'))
    }
  }),
  spinnerWhileLoading(['users']),
  ProfileModalHandler
)
