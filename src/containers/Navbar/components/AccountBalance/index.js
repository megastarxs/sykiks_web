import React from 'react'
import enhance from './AccountBalance.enhancer'
import { FormattedNumber } from 'react-intl'

export const AccountBalance = ({ accountInfo }) => {
  let balance = 0
  if (accountInfo && accountInfo.balance) balance = accountInfo.balance - 0
  return (
    <span>
      <FormattedNumber
        style="decimal"
        minimumFractionDigits={2}
        maximumFractionDigits={2}
        value={balance}
      />
    </span>
  )
}

export default enhance(AccountBalance)
