import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { UserIsAdmin } from 'utils/router'
import { spinnerWhileLoading } from 'utils/components'
import { withHandlers } from 'recompose'
import { withNotifications } from 'modules/notification'

export default compose(
  // create listener for masterconfig, results go into redux
  UserIsAdmin,
  withNotifications,
  firebaseConnect([{ path: `master-config`, storeAs: `masterConfig` }]),
  withHandlers({
    updateConfig: ({ firebase, showSuccess }) => formValues => {
      firebase
        .ref('master-config')
        .update(formValues)
        .then(() => showSuccess('Config Updated'))
    }
  }),
  connect(({ firebase: { data } }) => ({
    masterConfig: data.masterConfig
  })),
  spinnerWhileLoading(['masterConfig'])
)
