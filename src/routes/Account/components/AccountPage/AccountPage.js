import React from 'react'
import PropTypes from 'prop-types'
import AccountForm from '../AccountForm'
import Subheader from 'components/Subheader'
import { AvatarByUid } from 'components/Avatar'
import { FormGroup, Input, Label } from 'reactstrap'
import FileUpload from 'components/FileUpload'

export const AccountPage = ({
  avatarUrl,
  updateAccount,
  updateImage,
  updateStatus,
  profile,
  paypalId,
  progress,
  auth
}) => (
  <div className="container edit-account">
    <Subheader subheader="Edit Account" />
    <div className="form-panel row">
      <div className="col-sm-3 col-md-3 col-lg-3 text-center">
        <FileUpload onDone={updateImage}>
          <AvatarByUid
            uid={auth.uid}
            style={{ width: 200, height: 200, objectFit: 'contain' }}
            className="img-fluid"
          />
          {!progress && <h6>Upload Image</h6>}
          {progress && (
            <div className="progress">
              <div className="progress-bar" style={{ width: progress + '%' }} />
            </div>
          )}
        </FileUpload>
      </div>
      <div className="col-sm-9 col-md-9 col-lg-9">
        <FormGroup>
          <Label for="email">Email</Label>
          <Input
            type="email"
            name="email"
            id="email"
            value={auth.email}
            readOnly
            className="form-control-plaintext"
          />
        </FormGroup>

        <AccountForm
          onSubmit={updateAccount}
          profile={profile}
          initialValues={profile}
        />
      </div>
    </div>
  </div>
)

AccountPage.propTypes = {
  avatarUrl: PropTypes.string,
  profile: PropTypes.object,
  updateAccount: PropTypes.func
}

export default AccountPage
