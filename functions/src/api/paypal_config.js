import paypal from 'paypal-rest-sdk'
import * as admin from 'firebase-admin'

try {
  admin.initializeApp()
} catch (e) {}

let initPaypal = async () => {
  let config = await admin
    .database()
    .ref('master-config')
    .once('value')
  config = config.val()
  paypal.configure({
    mode: config.paypalMode,
    client_id: config.clientId,
    client_secret: config.clientSecret
  })
  return paypal
}

export default initPaypal
