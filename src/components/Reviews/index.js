import React from 'react'
import { FormattedDate, FormattedTime } from 'react-intl'
import enhance from './Reviews.enhancer'
import './Reviews.scss'
import Icon from 'components/Icon'

const Reviews = ({ reviews, profile, deleteReview }) => (
  <div className="reviews">
    {reviews &&
      Object.keys(reviews).map((key, index) => (
        <div key={key}>
          {reviews[key].message && (
            <div key={key}>
              <div className="review-msg">{reviews[key].message}</div>
              <div>
                {reviews[key].rating == 'up' && (
                  <span className="thumb-up">
                    <Icon name="ios-thumbs-up" />
                  </span>
                )}
                {reviews[key].rating == 'down' && (
                  <span className="thumb-down">
                    <Icon name="ios-thumbs-down" />
                  </span>
                )}
                <span className="review-time mr-3">
                  <FormattedDate
                    value={reviews[key].postedOn}
                    year="numeric"
                    month="long"
                    day="2-digit"
                  />{' '}
                </span>
                <span className="review-time mr-3">
                  <FormattedTime value={reviews[key].postedOn} />
                </span>
                <span className="review-from">
                  {reviews[key].fromScreenName}
                </span>
                {profile.type == 'admin' &&
                  reviews[key].rating == 'down' && (
                    <button
                      className="btn btn-link"
                      onClick={() => deleteReview(key)}>
                      Delete
                    </button>
                  )}
              </div>
              <hr />
            </div>
          )}
        </div>
      ))}
  </div>
)

export default enhance(Reviews)
