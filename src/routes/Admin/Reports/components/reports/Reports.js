import React from 'react'
import { Table, Button } from 'reactstrap'
import ProfileModal from 'components/ProfileModal'
import Subheader from 'components/Subheader'
import DefaultMessage from 'components/DefaultMessage'
import { FormattedTime, FormattedDate } from 'react-intl'
import Messages from '../messages'
import Chats from '../chats'

function Reports({
  classes,
  reports,
  updateProfileModal,
  closeModal,
  profileId,
  openMessages,
  archive,
  open,
  openMessagesModal,
  closeMessagesModal,
  messageId,
  isMessagesModalOpen,
  openChatModal,
  closeChatModal,
  isChatModalOpen,
  pair
}) {
  return (
    <div className="container">
      <Subheader subheader="User Reports" />
      {!reports && (
        <DefaultMessage>
          No Reports Yet. All User reports are seen here.
        </DefaultMessage>
      )}
      {reports && (
        <Table responsive bordered hover className="text-center">
          <thead>
            <tr>
              <th>Message</th>
              <th>Reported</th>
              <th>Reported from</th>
              <th>On</th>
              <th colSpan="2" />
              <th> Actions </th>
            </tr>
          </thead>
          <tbody>
            {Object.keys(reports)
              .reverse()
              .map((key, index) => {
                let report = reports[key]
                return (
                  <tr key={key}>
                    <td>{report.reason}</td>
                    <td>
                      <Button
                        color="link"
                        onClick={() => updateProfileModal(report.reportedId)}>
                        {report.reported}
                      </Button>
                    </td>
                    <td>
                      <Button
                        color="link"
                        onClick={() => updateProfileModal(report.reportedById)}>
                        {report.reportedBy} ({report.reportedByType})
                      </Button>
                    </td>
                    <td>
                      <FormattedDate
                        value={report.time}
                        year="numeric"
                        month="long"
                        day="2-digit"
                      />
                      {'   '}
                      <FormattedTime value={report.time} />
                    </td>
                    <td>
                      <Button
                        color="link"
                        onClick={() => openMessagesModal(report)}>
                        Check Messages
                      </Button>
                    </td>
                    <td>
                      <Button
                        color="link"
                        onClick={() => openChatModal(report)}>
                        Check chat sessions
                      </Button>
                    </td>
                    <td>
                      <Button color="link" onClick={() => archive(key)}>
                        Archive
                      </Button>
                    </td>
                  </tr>
                )
              })}
          </tbody>
        </Table>
      )}

      <ProfileModal
        key={profileId}
        uid={profileId}
        open={open}
        onClose={() => closeModal()}
      />
      <Messages
        messageId={messageId}
        open={isMessagesModalOpen}
        onClose={() => closeMessagesModal()}
      />
      <Chats
        pair={pair}
        open={isChatModalOpen}
        onClose={() => closeChatModal()}
      />
    </div>
  )
}

export default Reports
