import React from 'react'
import './content.scss'
export default (
  <ol className="page-content">
    <li>
      <p>
        All users including clients and advisors must be at least 18 years of
        age or older to use the services on sykiks.com. Access to the Site and
        its Services by anyone under eighteen (18) is strictly forbidden.
      </p>
    </li>
    <li>
      <p>
        You understand that any information provided to you by the advisors on
        our platform is intended for entertainment purposes only and should not
        replace the expertise of a qualified professional.
      </p>
    </li>

    <li>
      <p>
        You understand that medical or health related questions are strictly
        forbidden on the site and you should seek the council of a doctor or
        licensed medical professional if you are concerned about your current
        state of health.
      </p>
    </li>

    <li>
      <p>
        You understand that members and advisors who participate in using the
        sykiks.com platform do so at their own risk and sykiks.com will not be
        held accountable or liable for any losses or damages caused by
        client/advisor relations, including the reliance of any information,
        opinion or advice given by advisors.
      </p>
    </li>

    <li>
      <p>
        You understand that any transaction made is a binding contract between
        members and advisors only and sykiks.com are not liable or responsible
        for the refund of unsatisfactory services.
      </p>
    </li>
    <li>
      <p>
        You understand that advisors are not employees, agents or
        representatives of sykiks.com.
      </p>
    </li>
    <li>
      <p>
        Sykiks.com auditions advisors to ensure that a high standard of service
        is provided to its members, however sykiks.com is not responsible for
        verifying the identity of any advisor or user and cannot guarantee their
        qualifications, information, background and expertise as being accurate.
      </p>
    </li>
    <li>
      <p>
        You understand that any images you upload must be appropriate for our
        site and must not be offensive to members of the public. Images
        including violence, pornography, degrading language and/or images of
        minors (under the age of 18) are strictly forbidden on our site.
      </p>
    </li>
  </ol>
)
