import React from 'react'
import enhacer from './enhancer'
import { Line } from 'react-chartjs-2'
import './admin.scss'
import { AvatarByUid } from 'components/Avatar'
import ScreenName from 'components/ScreenName'
import { FormattedNumber } from 'react-intl'
let Admin = ({ lastReport, lastReportId, latestReport, percentageCut }) => {
  percentageCut = percentageCut - 0
  let clientCreditsData = []
  let psychicEarningData = []
  let labelsData = []
  let clientCredits = 0
  let psychicEarning = 0
  let latestData
  if (lastReport) latestData = lastReport[lastReportId]
  else latestData = false

  let PsychicEarning = latestReport.psychicEarning
  let SykiksEarning = (latestReport.psychicEarning * percentageCut) / 100

  if (lastReport) {
    Object.keys(lastReport).forEach(key => {
      clientCredits = lastReport[key].clientCredits
      psychicEarning = lastReport[key].psychicEarning
      labelsData.push(key)
      clientCredits = Math.round(clientCredits * 100) / 100
      psychicEarning = Math.round(psychicEarning * 100) / 100
      clientCreditsData.push(clientCredits)
      psychicEarningData.push(psychicEarning)
    })
  }

  const data = {
    labels: labelsData,
    datasets: [
      {
        label: 'Client Credits',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: clientCreditsData
      },
      {
        label: 'Psychic Earning',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,52,0.4)',
        borderColor: 'rgba(75,192,32,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: psychicEarningData
      }
    ]
  }

  return (
    <div className="container">
      <title>Sykiks - Admin Dashboard</title>
      <div className="col-8 offset-2">
        <Line data={data} />
      </div>
      <hr />
      <h3 className="text-center mt-5">Today</h3>
      <div className="row text-center">
        <div className="col-md-4 box">
          <div className="circle">
            <p className="value">
              $<FormattedNumber
                style="decimal"
                minimumFractionDigits={2}
                maximumFractionDigits={2}
                value={latestReport.clientCredits || 0}
              />
            </p>
          </div>
          <p className="title">Client Credits</p>
        </div>
        <div className="col-md-4 box">
          <div className="circle">
            <p className="value">
              $<FormattedNumber
                style="decimal"
                minimumFractionDigits={2}
                maximumFractionDigits={2}
                value={PsychicEarning}
              />
            </p>
          </div>
          <p className="title">Psychic Earning</p>
        </div>
        <div className="col-md-4 box">
          <div className="circle">
            <p className="value">
              $<FormattedNumber
                style="decimal"
                minimumFractionDigits={2}
                maximumFractionDigits={2}
                value={SykiksEarning}
              />
            </p>
          </div>
          <p className="title">Sykiks Earning</p>
        </div>
      </div>
      <hr />
      {latestData && (
        <div>
          <div className="col text-center mt-5">
            <h3>Highest Paid Psychic In Current Payout Cycle</h3>
            <div className="profile-header-container">
              <div className="profile-header-img">
                <AvatarByUid
                  uid={latestData.HighPaidId}
                  className="img-fluid image-circle"
                />
                <div className="rank-label-container">
                  <span className="label label-default rank-label">
                    <ScreenName uid={latestData.HighPaidId} />
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default enhacer(Admin)
