import React from 'react'
import _ from 'lodash'
import enhancer from './psychics.enhancer'
import { AvatarbyImage } from 'components/Avatar'
import { Link } from 'react-router'
import { FormattedNumber } from 'react-intl'
import Icon from 'components/Icon'
import './PsychicList.scss'
import { smallText, ComputeProfile } from 'utils/helpers'
import FavoriteIcon from './favorites'
import CheckAuthButton from 'components/CheckAuthButton'

let PsychicCard = props => {
  let { id, profile, me } = props
  if (profile.status == 'pending' || profile.status == 'Rejected') return null
  return (
    <div className="col-lg-4 col-md-6 psychic-card d-flex align-items-stretch">
      <div className="card">
        <div className="card-body">
          <div className="text-right heart-div">
            <FavoriteIcon uid={id} />
          </div>
          <Link to={`/profile/${id}`}>
            <AvatarbyImage
              src={profile.avatar}
              className="card-image img-fluid"
            />
            <div>
              <h6 className="card-title text-center">{profile.screenName}</h6>
            </div>
          </Link>
          <p className="card-text text-center about">
            {smallText(profile.introduction)}
          </p>
          <p className="card-text text-center card-compute">
            <span className="purple-text">
              <Icon name="ios-thumbs" />
              <FormattedNumber
                value={profile.percentage / 100}
                style="percent"
              />
              {' Accuracy '}
            </span>{' '}
            of {profile.ratingCount} reviews
          </p>

          <div style={{ padding: '1rem' }}>
            <p
              style={{
                backgroundColor: '#e6d8ef',
                width: '100%',
                color: '#764095',
                textAlign: 'center',
                fontWeight: 400,
                padding: 5,
                margin: 5
              }}>
              START WITH{' '}
              <b>
                <FormattedNumber
                  style="currency"
                  currency="USD"
                  value={profile.chatRate * 3}
                />{' '}
              </b>FREE
            </p>
          </div>
          <div className="row card-text">
            <div className="text-center ml-auto mr-auto">
              {profile.status == 'Available' && (
                <CheckAuthButton
                  psychic={id}
                  chatRate={profile.chatRate}
                  online={profile.online}
                />
              )}

              {(!profile.status || profile.status == 'Offline') && (
                <a href="#" className="btn grey-button card-link ">
                  <Icon name="ios-close-circle-outline" />OFFLINE
                </a>
              )}
              {profile.status == 'Busy' && (
                <a href="#" className="btn purple-button card-link ">
                  <Icon name="ios-close-circle-outline" />BUSY
                </a>
              )}
              <div className="fw-500">
                <FormattedNumber
                  style="currency"
                  currency="USD"
                  value={profile.chatRate - 0}
                />
              </div>
              <div className="card-compute grey-text">Per min</div>
            </div>
            <div className="text-center ml-auto mr-auto">
              <CheckAuthButton messagePath={`/messages/${me.uid}-${id}`}>
                <div className="btn message-button card-link">
                  <Icon name="ios-mail" />MESSAGE
                </div>
              </CheckAuthButton>
              <div className="fw-500">
                <FormattedNumber
                  style="currency"
                  currency="USD"
                  value={profile.msgRate - 0}
                />
              </div>
              <div className="card-compute grey-text">Per message</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

let Psychics = ({
  psychics,
  profile,
  filters,
  favorites,
  limit,
  pageno,
  updatePageno
}) => {
  let perpage = 9
  let count = 0
  if (!psychics)
    return <h5 className="text-center"> Sorry no psychics available yet </h5>
  psychics = Object.keys(psychics).map(key => ({
    key,
    ...psychics[key],
    ...ComputeProfile(psychics[key])
  }))

  psychics = psychics.filter(d => {
    let push = true

    if (d.banned) return false

    if (filters) {
      if (filters.favorites && favorites && !favorites[d.key]) push = false

      if (push && filters.specialities) {
        push = false
        filters.specialities.split(', ').forEach(d2 => {
          if (d.categories.includes(d2)) push = true
        })
      }

      if (push && filters.available) {
        push = false
        if (d.status == 'Available') push = true
      }
    }

    return push
  })

  psychics.forEach(d => {
    if (d.online && d.status == 'Available') count++
    if (d.status == 'Busy' || d.status == 'Offline') d.order = 3
    else if (d.online) d.order = 1
    else if (d.status == 'Available') d.order = 2
  })

  let hideUpdatePagebtn = false
  if (!filters) {
    psychics = _.orderBy(psychics, 'order')
    if (limit) psychics = psychics.slice(0, limit)
    hideUpdatePagebtn = true
  }

  if (filters) {
    if (filters.sortBy == 'Top Psychics')
      psychics = _.orderBy(
        psychics,
        ['order', 'percentage', 'ratingCount'],
        ['asc', 'desc', 'desc']
      )
    if (filters.sortBy == 'New Psychics')
      psychics = _.orderBy(psychics, ['order', 'createdAt'], ['asc', 'desc'])
    if (filters.sortBy == 'Price Lowest')
      psychics = _.orderBy(psychics, ['order', 'chatRate'], ['asc', 'asc'])
    if (filters.sortBy == 'Price Highest')
      psychics = _.orderBy(psychics, ['order', 'chatRate'], ['asc', 'desc'])

    if (pageno * perpage >= psychics.length) {
      hideUpdatePagebtn = true
    }
    psychics = psychics.slice(0, pageno * perpage)
  }
  psychics = _.orderBy(
    psychics,
    ['order', 'percentage', 'ratingCount'],
    ['asc', 'desc', 'desc']
  )

  return (
    <div>
      <h2 className="po">{count} Psychics Online </h2>
      <div className="row wide">
        {psychics &&
          psychics.map(psychic => (
            <PsychicCard
              key={psychic.key}
              id={psychic.key}
              profile={psychic}
              me={profile}
            />
          ))}
      </div>
      {!hideUpdatePagebtn && (
        <div className="row">
          <div
            className="btn btn-lg btn-primary"
            style={{ margin: '0 auto' }}
            onClick={updatePageno}>
            Load More
          </div>
        </div>
      )}
    </div>
  )
}
export default enhancer(Psychics)
