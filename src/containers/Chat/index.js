import React from 'react'
import Client from './Client'
import Psychic from './Psychic'
import CheckSession from './CheckSession'
import enhance from './enhancer'
import './Chat.scss'
let ChatRequest = ({ profile, loggedIn }) => (
  <div>
    {profile.type == 'client' && <Client />}
    {profile.type == 'psychic' && <Psychic />}
    {loggedIn && <CheckSession />}
  </div>
)

export default enhance(ChatRequest)
