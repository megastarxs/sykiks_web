import React from 'react'
import ConfigForm from '../ConfigForm'

export const MasterConfig = ({ masterConfig, updateConfig }) => (
  <div className="container">
    <ConfigForm initialValues={masterConfig} onSubmit={updateConfig} />
  </div>
)

export default MasterConfig
