import React from 'react'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import Icon from 'components/Icon'
import { spinnerWhileLoading } from 'utils/components'
import { withHandlers, compose } from 'recompose'
let enhance = compose(
  firebaseConnect((props, firebase) => [
    {
      path: `settings/${firebase.firebase._.authUid}/favorites`,
      storeAs: 'favorites'
    }
  ]),
  connect(({ firebase: { data: { favorites }, auth } }) => ({
    favorites,
    me: auth.uid
  })),
  withHandlers({
    addToFavorites: ({ firebase, me }) => uid => {
      let obj = {}
      obj[uid] = true
      firebase.ref(`settings/${me}/favorites`).update(obj)
    },
    removeFromFavorites: ({ firebase, me }) => uid => {
      firebase.ref(`settings/${me}/favorites/${uid}`).remove()
    }
  }),
  spinnerWhileLoading(['favorites'])
)

let favoriteButton = ({
  favorites,
  uid,
  addToFavorites,
  removeFromFavorites,
  me
}) => (
  <div>
    {(!favorites || !favorites[uid]) &&
      me && (
        <button
          type="button"
          className="btn button btn-sm button"
          onClick={() => addToFavorites(uid)}>
          <Icon name="ios-heart-empty" />Add to favorite
        </button>
      )}
    {favorites &&
      favorites[uid] &&
      me && (
        <button
          type="button"
          className="btn button btn-sm button"
          onClick={() => removeFromFavorites(uid)}>
          <Icon name="ios-heart" />Remove from favorite
        </button>
      )}
  </div>
)

export default enhance(favoriteButton)
