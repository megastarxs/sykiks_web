import React from 'react'
import { Form, FormGroup, Input } from 'reactstrap'
import Icon from 'components/Icon'

export default () => (
  <Form className="form-inline search-box">
    <FormGroup>
      <div className="input-group">
        <Input
          type="search"
          name="search"
          id="exampleSearch"
          placeholder="Search for Advisor"
          className="search-bar bg-light"
        />
        <Icon name="ios-search" className="search-icon" />
      </div>
    </FormGroup>
  </Form>
)
