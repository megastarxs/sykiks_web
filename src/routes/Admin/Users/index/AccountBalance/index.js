import React from 'react'
import { FormattedNumber } from 'react-intl'
import enhancer from './enhancer'

let balanceView = ({ balance }) => (
  <span>
    {balance ? (
      <b>
        {' '}
        <FormattedNumber style="currency" currency="USD" value={balance} />{' '}
      </b>
    ) : (
      <FormattedNumber style="currency" currency="USD" value={0} />
    )}
  </span>
)

export default enhancer(balanceView)
