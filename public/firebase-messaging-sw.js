/* eslint-disable */
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js')

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.

// dev messagingSenderId
// let messagingSenderId = '993072186456'

// dev live
let messagingSenderId = "265219963074"

if(location.hostname !='sykiks.com')
  messagingSenderId = '993072186456'

firebase.initializeApp({
  messagingSenderId: messagingSenderId
})

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging()
