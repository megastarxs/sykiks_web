import firebase from 'firebase/app'
import _ from 'lodash'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStateHandlers, lifecycle } from 'recompose'
import { withNotifications } from 'modules/notification'
import { firebaseConnect, populate } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { now } from 'utils/helpers'
let firebaseDB = firebase.database()

const populates = [
  {
    child: 'from',
    root: 'users',
    childParam: 'screenName',
    childAlias: 'fromScreenName'
  }
]

export default compose(
  firebaseConnect(({ roomId }, firebase) => [
    {
      path: `messages/${roomId}`,
      storeAs: `messages_${roomId}`,
      populates
    },
    {
      path: `settings/${firebase.firebase._.authUid}/accountInfo/balance`,
      storeAs: 'balance'
    }
  ]),
  connect(({ firebase }, { roomId }) => ({
    messages: populate(firebase, `messages_${roomId}`, populates),
    profile: firebase.profile,
    balance: firebase.data.balance,
    auth: firebase.auth
  })),
  withNotifications,
  lifecycle({
    componentWillReceiveProps(nextProps) {
      let me = this.props.auth.uid
      if (this.props.profile.type == 'admin') me = 'admin'
      let from = ''
      try {
        from = _.last(_.values(nextProps.messages)).from
      } catch (e) {}

      if (from != '' && from != me)
        firebaseDB.ref(`settings/${me}/unread/${from}`).remove()
    }
  }),
  withStateHandlers(
    { text: '', record: false },
    {
      changeText: () => e => ({
        text: e.target.value
      }),
      openRecorder: () => e => ({
        record: true
      }),
      closeRecorder: () => e => ({
        record: false
      }),
      sendMessage: (
        { text },
        { firebase, profile, roomId, balance, showError, showSuccess, auth }
      ) => ({ audio }) => {
        let clientId = roomId.split('-')[0]
        let psychicId = roomId.split('-')[1]
        if (audio) text = { audio }
        else if (text.trim() == '') return showError('Please Enter Message')
        if (roomId.includes('admin') || profile.type == 'psychic') {
          let senderId = ''
          let receiverId = ''
          if (profile.type == 'admin') {
            senderId = 'admin'
            if (clientId == 'admin') receiverId = psychicId
            else receiverId = clientId
          }
          if (profile.type == 'psychic') {
            senderId = psychicId
            receiverId = clientId
          }
          if (profile.type == 'client') {
            senderId = clientId
            receiverId = psychicId
          }
          let msgObject = {
            from: senderId,
            roomId,
            lastMessage: text,
            fromServer: false,
            time: now,
            msgRate: 0
          }
          if (profile.type != 'admin') delete msgObject.from
          else msgObject.from = receiverId
          firebase
            .ref(`settings/${senderId}/messages-listing/${receiverId}`)
            .update(msgObject)
            .then(() => showSuccess(`Message sent.`))
        } else {
          firebase
            .ref(`users/${psychicId}/msgRate`)
            .once('value')
            .then(s => {
              let msgRate = s.val()
              if (balance < msgRate)
                return showError(
                  "You don't have enough credits to send this message"
                )
              else
                firebase
                  .ref(`settings/${auth.uid}/messages-listing/${psychicId}`)
                  .update({
                    from: psychicId,
                    roomId,
                    lastMessage: text,
                    fromServer: false,
                    time: now,
                    msgRate
                  })
                  .then(() =>
                    showSuccess(
                      `Message sent. ${msgRate} deducted from your account `
                    )
                  )
            })
        }

        return {
          text: ''
        }
      }
    }
  ),
  spinnerWhileLoading(['messages'])
)
