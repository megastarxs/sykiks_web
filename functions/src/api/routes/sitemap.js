import * as admin from 'firebase-admin'
import { getVal } from '../helpers'

export default app => {
  app.get('/sitemap', async (req, res) => {
    let usersRef = admin.database().ref('users')
    let host = 'https://sykiks.com'

    let lastmod = new Date().toISOString()
    let dynamicLinksConfig = {
      changefreq: 'weekly',
      priority: 0.8
    }

    let staticlinks = [
      {
        loc: '',
        changefreq: 'daily',
        priority: 0.9
      },
      {
        loc: 'login',
        changefreq: 'monthly',
        priority: 0.8
      },
      {
        loc: 'signup/psychic',
        changefreq: 'monthly',
        priority: 0.8
      },
      {
        loc: 'signup/client',
        changefreq: 'monthly',
        priority: 0.8
      },
      {
        loc: 'pages/privacy_policy',
        changefreq: 'monthly',
        priority: 0.8
      },
      {
        loc: 'pages/terms_of_service',
        changefreq: 'monthly',
        priority: 0.8
      },
      {
        loc: 'pages/disclaimer',
        changefreq: 'monthly',
        priority: 0.8
      },
      {
        loc: 'contactus',
        changefreq: 'monthly',
        priority: 0.8
      },
      {
        loc: 'pages/how_it_works',
        changefreq: 'monthly',
        priority: 0.8
      },
      {
        loc: 'psychics',
        changefreq: 'daily',
        priority: 0.9
      },
      {
        loc: 'signup',
        changefreq: 'monthly',
        priority: 0.8
      }
    ]

    let dynamicLinks = await getVal(
      usersRef.orderByChild('type').equalTo('psychic')
    )

    staticlinks = staticlinks
      .map(
        d => `
      <url>
        <loc>${host}/${d.loc}</loc>
        <lastmod>${lastmod}</lastmod>
        <changefreq>${d.changefreq}</changefreq>
        <priority>${d.priority}</priority>
      </url>
    `
      )
      .join('')

    dynamicLinks = Object.keys(dynamicLinks)
      .map(
        d => `
      <url>
        <loc>${host}/${d}</loc>
        <lastmod>${lastmod}</lastmod>
        <changefreq>${dynamicLinksConfig.changefreq}</changefreq>
        <priority>${dynamicLinksConfig.priority}</priority>
      </url>
    `
      )
      .join('')

    var xml = `<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        ${staticlinks}
        ${dynamicLinks}
        </urlset>
      `
    res.header('Content-Type', 'text/xml').send(xml)
  })
}
