import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import { now, getVal } from '../utils/helpers'

try {
  admin.initializeApp()
} catch (e) {}

/**
 * @name payout
 * Cloud Function triggered by Real Time Database Write Event
 * @type {functions.CloudFunction}
 */
export default functions.database
  .ref('/payout/{payoutId}/paidTo/{insertId}')
  .onWrite(payoutEvent)

/**
 * @param  {functions.Event} event - Function event
 * @return {Promise}
 */

function getNextPayoutDate() {
  var today = new Date()
  let requiredDate = new Date(
    today.getFullYear(),
    today.getMonth(),
    today.getDate() + 15
  )
  return requiredDate
}

async function payoutEvent(change, context) {
  const { payoutId } = context.params
  const payoutData = change.after.val()

  if (!payoutData || !payoutData.manual) return

  let db = admin.database()

  let { balance, fees, key } = payoutData
  let nextPayoutDate = getNextPayoutDate()

  let payoutRef = db.ref(`payout/${payoutId}`)

  let paidTo = await getVal(payoutRef.child('paidTo'))

  let totalPaid = 0
  let totalFees = 0
  console.log(paidTo)
  Object.keys(paidTo).forEach(k => {
    let d = paidTo[k]
    totalPaid += d.balance
    totalFees += d.fees
  })

  payoutRef.update({ totalPaid, totalFees, date: now() })

  let user = db.ref(`settings/${key}`)
  user.child('accountInfo').update({ balance: 0, nextPayoutDate })
  let obj = {}
  obj[payoutId] = {
    balance: balance,
    fees: fees,
    date: now(),
    status: 'Paid manually'
  }
  user.child('payouts').update(obj)
}
