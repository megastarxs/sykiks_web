import React from 'react'
import enhance from './Profile.enhancer'
import { AvatarByUid } from 'components/Avatar'
import { FormattedNumber, FormattedDate } from 'react-intl'
import Icon from 'components/Icon'
import Reviews from 'components/Reviews'
import CheckAuthButton from 'components/CheckAuthButton'
import { ComputeProfile } from 'utils/helpers'
import './profile.scss'
import FavoriteBtn from './favorites'
import ProfileModal from 'components/ProfileModal'
export const Profile = ({
  id,
  profile,
  me,
  loggedIn,
  updateProfileModal,
  closeModal,
  open
}) => {
  let profileCal = ComputeProfile(profile)
  let profileUrl = 'https://sykiks.com/profile/' + id
  let iframeUrl =
    'https://www.facebook.com/plugins/like.php?href=' +
    profileUrl +
    '&width=133&layout=button_count&action=like&size=large&show_faces=true&share=false&height=46&appId'
  let shareIframeUrl =
    'https://www.facebook.com/plugins/share_button.php?href=' +
    profileUrl +
    '&layout=button_count&size=large&mobile_iframe=false&width=84&height=28&appId'
  return (
    <div className="container psychic-profile">
      <title>{profile.screenName} Psychic Reader - Sykiks</title>
      <div className="top row pf-head">
        <div className="col-lg-3 propic">
          <AvatarByUid uid={id} className="img-fluid profile-image" />
        </div>
        <div className="col-lg-5 px-5 justify-content-center align-self-center">
          <p className="screen-name">{profile.screenName}</p>
          <p className="intro">{profile.introduction}</p>
          {loggedIn && <FavoriteBtn uid={id} />}
          <div className="row col-6 mt-3 pl-0">
            <div className="col-6">
              <iframe
                src={iframeUrl}
                width="auto"
                height="46"
                style={{ border: 'none', overflow: 'hidden' }}
                scrolling="no"
                frameBorder="0"
                allowTransparency="true"
                allow="encrypted-media"
              />
            </div>
            <div className="col-6">
              <iframe
                src={shareIframeUrl}
                width="auto"
                height="46"
                style={{ border: 'none', overflow: 'hidden' }}
                scrolling="no"
                frameBorder="0"
                allowTransparency="true"
                allow="encrypted-media"
              />
            </div>
          </div>
        </div>

        <div className="col-lg-4 justify-content-center align-self-center text-center">
          <Icon name="md-thumbs-up" className="large-thumb" />
          <span className="percentage">
            <FormattedNumber
              value={profileCal.percentage / 100}
              style="percent"
            />
          </span>
          <p>
            <span className="purple-text">Accuracy </span>
            <span className="review-text">
              of {profileCal.ratingCount} reviews
            </span>
          </p>
          <div className="row">
            <div className="ml-auto mr-auto col-md-6">
              {profile.status == 'Available' && (
                <CheckAuthButton
                  psychic={id}
                  chatRate={profile.chatRate}
                  online={profile.online}
                />
              )}
              {profile.status == 'Offline' && (
                <a href="#" className="btn grey-button col-md-12">
                  <Icon name="ios-close-circle-outline" />OFFLINE
                </a>
              )}
              {profile.status == 'Busy' && (
                <a href="#" className="btn purple-button col-md-12">
                  <Icon name="ios-close-circle-outline" />BUSY
                </a>
              )}
              <div className="fw-500">
                <FormattedNumber
                  style="currency"
                  currency="USD"
                  value={profile.chatRate - 0}
                />
              </div>
              <div className="card-compute grey-text">Per min</div>
            </div>
            <div className="ml-auto mr-auto col-md-6">
              <CheckAuthButton messagePath={`/messages/${me}-${id}`}>
                <div className="btn message-button col-md-12">
                  <Icon name="ios-mail" />MESSAGE
                </div>
              </CheckAuthButton>
              <div className="fw-500">
                <FormattedNumber
                  style="currency"
                  currency="USD"
                  value={profile.msgRate - 0}
                />
              </div>
              <div className="card-compute grey-text">Per message</div>
            </div>
          </div>
        </div>
      </div>
      <hr />
      <div>
        <span className="card-compute grey-text">Categories </span>
        <span className="categories ml-5">{profile.categories}</span>
      </div>
      <div className="mt-4">
        <div className="title-text mb-3">Member Since </div>
        <div className="content-text">
          <FormattedDate
            value={profile.createdAt}
            year="numeric"
            month="long"
            day="2-digit"
          />
        </div>
      </div>
      <div className="mt-4">
        <div className="title-text mb-3">About Me </div>
        <div className="content-text aboutme">{profile.aboutme}</div>
      </div>
      <div className="mt-4">
        {loggedIn && (
          <button
            onClick={() => updateProfileModal(id)}
            className="btn btn-primary">
            Report / Block
          </button>
        )}
      </div>

      <div className="mt-4">
        <div className="title-text mb-3">Reviews </div>
        <Reviews uid={id} />
      </div>
      <ProfileModal
        key={id}
        uid={id}
        open={open}
        onClose={() => closeModal()}
      />
    </div>
  )
}

export default enhance(Profile)
