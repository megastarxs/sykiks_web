import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import { getVal } from '../utils/helpers'

try {
  admin.initializeApp()
} catch (e) {}

/**
 * @name chatTransaction
 * Cloud Function triggered by Real Time Database Write Event
 * @type {functions.CloudFunction}
 */
export default functions.database
  .ref('/chat-sessions/{insertId}/amount')
  .onWrite(chatTransactionEvent)

/**
 * @param  {functions.Event} event - Function event
 * @return {Promise}
 */
async function chatTransactionEvent(change, context) {
  let sessionData = await change.after.ref.parent.once('value')
  sessionData = sessionData.val()
  console.log(sessionData)
  let {
    chatRate,
    client,
    psychic,
    sessionStarted,
    amount,
    sessionKey
  } = sessionData
  if (!sessionStarted) return

  if (amount < (chatRate - 0) / 2) return
  chatRate = (chatRate - 0) / 2
  let clientRef = admin.database().ref(`settings/${client}/accountInfo`)
  let clientBalance = await getVal(clientRef.child('balance'))
  if (clientBalance < chatRate) {
    admin
      .database()
      .ref(`chat-sessions/${sessionKey}`)
      .update({ sessionEnded: true })
  } else {
    return Promise.all([
      admin
        .database()
        .ref(`settings/${client}/accountInfo/balance`)
        .transaction(balance => balance - 0 - chatRate),
      admin
        .database()
        .ref(`settings/${psychic}/accountInfo/balance`)
        .transaction(balance => balance - 0 + chatRate),
      admin
        .database()
        .ref(`master-config/today/psychicEarning`)
        .transaction(balance => balance - 0 + chatRate),
      admin
        .database()
        .ref(`settings/${psychic}/accountInfo/lastSession`)
        .set(amount)
    ])
  }
}
