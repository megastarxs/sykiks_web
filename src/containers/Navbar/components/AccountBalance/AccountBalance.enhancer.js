import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'

export default compose(
  firebaseConnect((props, firebase) => [
    {
      path: `settings/${firebase.firebase._.authUid}/accountInfo`,
      storeAs: 'accountInfo'
    }
  ]),
  connect(({ firebase: { data: { accountInfo } } }) => ({
    accountInfo
  }))
)
