import React from 'react'
import windowsImageUrl from 'static/images/windows.png'
import macImageUrl from 'static/images/mac.png'
import macScreenshot from 'static/images/mac-task.jpg'
import winScreenshot from 'static/images/win-screen.png'

let windowsUrl =
  'https://firebasestorage.googleapis.com/v0/b/sykiks-live.appspot.com/o/Sykiks%20Setup%201.0.0.exe?alt=media&token=798e2215-453b-49e7-abaa-7892e94e3f63'
let macUrl =
  'https://firebasestorage.googleapis.com/v0/b/sykiks-live.appspot.com/o/Sykiks-1.0.0.dmg?alt=media&token=ebee0bf5-f055-461f-a21d-af02dac200d8'
export const Download = () => (
  <div className="container">
    <div className="text-left not-verified">
      <div className="mt-5">
        <center>
          <h5>
            Please download our Sykiks desktop app in order to receive
            notifications from clients.
          </h5>
        </center>
        <div className="ml-auto mr-auto mt-5 row">
          <div className="col-md-6 ">
            <center>
              <a href={windowsUrl} target="_blank">
                <img src={windowsImageUrl} style={{ height: 50 }} />
              </a>
              <br />
              <br />
              <h4>PC Users</h4>
              <a href={windowsUrl} target="_blank">
                <i
                  className="icon ion-ios-cloud-download"
                  style={{ fontSize: '1.2em' }}
                />Download Sykiks.exe for Windows 7/10
              </a>
            </center>
            <br />
            1. Download app from link above.<br />
            2. Install by running Sykiks.exe and following the installer.<br />
            3. Open application and login with your psychic account.<br />
            <br />
            <p style={{ fontSize: '.8em' }}>
              NOTE: You can close the app to run in the background, you will
              still receive incoming calls. Just click on the icon in the
              taskbar and select "Open Sykiks" to show the app again or select
              "Exit" to go offline.<br />
            </p>
            <img src={winScreenshot} style={{ height: 109 }} />
          </div>
          <br />
          <div className="col-md-6 text-left">
            <center>
              <a href={macUrl} target="_blank">
                <img src={macImageUrl} style={{ height: 50 }} />
              </a>
              <br />
              <br />
              <h4>Mac Users</h4>
              <a href={macUrl} target="_blank">
                <i
                  className="icon ion-ios-cloud-download"
                  style={{ fontSize: '1.2em' }}
                />{' '}
                Download Sykiks-1.0.0-mac.dmg for IOS
              </a>
            </center>
            <br />
            1. Download app from link above.<br />
            2. Install by dragging Sykiks icon to application folder. (In your
            mac system preferences, you must allow permission from 3rd party
            applications in order to run the app)<br />
            3. Run application and login with psychic account.<br />
            <br />
            <p style={{ fontSize: '.8em' }}>
              NOTE: You can close the app to run in the background, you will
              still receive incoming calls. Just click on the icon in the
              taskbar and select "Open Sykiks" to show the app again or select
              "Exit" to go offline.<br />
            </p>
            <img src={macScreenshot} style={{ height: 79 }} />
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Download
