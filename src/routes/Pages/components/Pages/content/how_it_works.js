import React from 'react'

export default (
  <div className="how-it-works">
    <div className="row">
      <div className="col-lg-2 text-center">
        <i className="icon ion-ios-people psychic-header-icon" />
      </div>
      <div className="col-lg-10">
        <h3>1. Register</h3>
        <h5>Client Registration</h5>
        In order to begin chatting with your desired Advisor,{' '}
        <a href="/signup/client" target="_new">
          Click to Register as Client
        </a>. It takes less than 2 minutes to create an account and begin using
        the platform.
        <br />
        <br />
        <h5>Psychic Registration</h5>
        If you are interested in joining as a psychic{' '}
        <a href="/signup/client" target="_new" title="Register as a Psychic">
          Click to Register as Psychic
        </a>. After you've submitted your details, your profile will not yet be
        viewable to clients until you are interviewed and manually approved by
        one of our team members.
      </div>
    </div>
    <hr />
    <div className="row">
      <div className="col-lg-2 text-center">
        <i className="icon ion-ios-cash psychic-header-icon" />
      </div>
      <div className="col-lg-10">
        <h3>2. Add Funds</h3>
        <h5>PayPal Payments</h5>
        In order to begin chatting with your desired Advisor, you must add funds
        to your account by clicking on the [+] next to your balance on the main
        user menu.
        <br />
        <br />Funds can be added in increments of $15, $50, and $100 via PayPal
        safe, securely, and fast! Once transaction is completed from PayPal,
        your funds will immediately be made available on Sykiks.
        <br />
      </div>
    </div>
    <hr />
    <div className="row">
      <div className="col-lg-2 text-center">
        <i className="icon ion-ios-chatboxes psychic-header-icon" />
      </div>
      <div className="col-lg-10">
        <h3>3. Get a Reading</h3>
        <h5>Live Chat</h5>
        After your account is funded you can speak with any of our{' '}
        <a href="/psychics" target="_new" title="Psychic Readers">
          trusted psychic advisors
        </a>{' '}
        by hitting the "Chat Now" button. Your expert will be ready and waiting
        to assist you via our chat interface!
        <br />
        <br />
        <h5>Offline Messages</h5>
        If your favorite psychic isn't online, you can always send them an
        offline message. Some per message cost applies according to individual
        rates set by psychic. You will be charged their desired rate each time
        you send a message but will not be charged for every message received.
        <br />
      </div>
    </div>
  </div>
)
