import React from 'react'
import {
  Nav,
  NavItem,
  NavLink,
  DropdownToggle,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem
} from 'reactstrap'
import { Link } from 'react-router'
let AdminNavbar = () => (
  <div className="container admin-navbar">
    <Nav pills>
      <NavItem>
        <NavLink tag={Link} activeClassName="active" to="/admin/reports">
          Reports
        </NavLink>
      </NavItem>
      <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
          Payouts
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem>
            <NavLink tag={Link} activeClassName="active" to="/admin/payouts">
              Past Payouts
            </NavLink>
            <NavLink
              tag={Link}
              activeClassName="active"
              to="/admin/payouts/pending">
              Pending Payouts
            </NavLink>
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
      <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
          Users
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem>
            <NavLink
              tag={Link}
              activeClassName="active"
              to="/admin/users/banned">
              Banned
            </NavLink>
            <NavLink
              tag={Link}
              activeClassName="active"
              to="/admin/users/clients">
              Clients
            </NavLink>
            <NavLink
              tag={Link}
              activeClassName="active"
              to="/admin/users/psychics">
              Psychics
            </NavLink>
            {/* <NavLink
              tag={Link}
              activeClassName="active"
              to="/admin/users/admins">
              Admins
            </NavLink> */}
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
      <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
          Contacts
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem>
            <NavLink
              tag={Link}
              activeClassName="active"
              to="/admin/contactus/nonarchived">
              Non-archived
            </NavLink>
            <NavLink
              tag={Link}
              activeClassName="active"
              to="/admin/contactus/archived">
              Archived
            </NavLink>
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
      <NavItem>
        <NavLink tag={Link} activeClassName="active" to="/admin/approve-user">
          Approve Psychics
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink tag={Link} activeClassName="active" to="/admin/config">
          Config
        </NavLink>
      </NavItem>
    </Nav>
  </div>
)

export default AdminNavbar
