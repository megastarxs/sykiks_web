import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'

export default compose(
  firebaseConnect((props, firebase) => [
    {
      path: `settings/${firebase.firebase._.authUid}/accountInfo`,
      storeAs: 'accountInfo'
    }
  ]),
  connect(({ firebase: { data: { accountInfo }, auth, profile } }) => ({
    accountInfo,
    auth,
    profile
  })),
  spinnerWhileLoading(['accountInfo'])
)
