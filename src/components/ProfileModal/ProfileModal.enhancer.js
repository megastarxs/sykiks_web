import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading, withRouter } from 'utils/components'
import { withStateHandlers, withHandlers } from 'recompose'
import { now } from 'utils/helpers'
import { withNotifications } from 'modules/notification'
export default compose(
  // UserIsAuthenticated,
  firebaseConnect(({ uid }, firebase) => [
    { path: `users/${uid}`, storeAs: `user_${uid}` },
    {
      path: `settings/${firebase.firebase._.authUid}/blocked/${uid}`,
      storeAs: `blocked`
    }
  ]),
  withRouter,
  connect(({ firebase: { data, auth, profile } }, { uid }) => ({
    user: data[`user_${uid}`],
    profile,
    userId: uid,
    myId: auth.uid,
    blocked: data[`blocked`]
  })),
  withNotifications,
  withStateHandlers(
    () => ({ report: false, block: false, regular: true, reportText: '' }),
    {
      reportProfileModal: () => () => ({
        report: true,
        regular: false,
        block: false
      }),
      changeText: () => e => ({
        reportText: e.target.value
      }),
      blockProfileModal: () => () => ({
        report: false,
        regular: false,
        block: true
      }),
      regularProfileModal: () => () => ({
        report: false,
        regular: true,
        block: false
      }),
      sendReport: (
        { reportText },
        { firebase, profile, myId, uid, showSuccess, onClose }
      ) => () => {
        firebase
          .ref(`reports`)
          .push({
            reportedById: myId,
            reportedByType: profile.type,
            reportedId: uid,
            time: now,
            reason: reportText
          })
          .then(() => {
            onClose()
            showSuccess(
              `Thank you for reporting. Your report has been registered successfully. `
            )
          })
      }
    }
  ),
  withHandlers({
    blockUser: ({ firebase, myId, uid, showSuccess, onClose }) => () => {
      firebase
        .ref(`settings/${myId}/blocked/${uid}`)
        .set(true)
        .then(() => {
          onClose()
          showSuccess(`This user has been blocked.`)
        })
    },
    banUser: ({ firebase, uid, showSuccess, onClose }) => () => {
      firebase
        .ref(`users/${uid}`)
        .update({
          banned: true
        })
        .then(() => {
          onClose()
          showSuccess(`User has been banned successfully.`)
        })
    },
    unbanUser: ({ firebase, uid, showSuccess, onClose }) => () => {
      firebase
        .ref(`users/${uid}/banned`)
        .remove()
        .then(() => {
          onClose()
          showSuccess(`User has been Unbanned successfully.`)
        })
    },
    goToMessages: ({ router, onClose }) => path => {
      router.push(path)
      onClose()
    }
  }),
  spinnerWhileLoading(['user'])
)
