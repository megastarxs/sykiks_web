import { compose } from 'recompose'
import { connect } from 'react-redux'
import { withFirebase, firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading, PaginateHandler } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'

export const clientEnhancer = compose(
  firebaseConnect(({ uid }, firebase) => [
    {
      path: 'chat-sessions',
      queryParams: [
        'orderByChild=client',
        'equalTo=' + firebase.firebase._.authUid
      ],
      storeAs: 'chatSessions'
    }
  ]),
  connect(({ firebase: { data } }) => ({
    chatSessions: data.chatSessions,
    percentageCut: 0
  })),
  PaginateHandler,
  spinnerWhileLoading(['chatSessions', 'percentageCut'])
)

export const psychicEnhancer = compose(
  firebaseConnect(({ uid }, firebase) => [
    {
      path: 'chat-sessions',
      queryParams: [
        'orderByChild=psychic',
        'equalTo=' + firebase.firebase._.authUid
      ],
      storeAs: 'chatSessions'
    },
    {
      path: `master-config/percentageCut`,
      storeAs: 'percentageCut'
    }
  ]),
  connect(({ firebase: { data } }) => ({
    chatSessions: data.chatSessions,
    percentageCut: data.percentageCut
  })),
  spinnerWhileLoading(['chatSessions']),
  PaginateHandler
)

export const profileEnhancer = compose(
  UserIsAuthenticated,
  withFirebase,
  connect(({ firebase: { auth, profile } }) => ({
    auth,
    profile
  })),
  spinnerWhileLoading(['profile'])
)
