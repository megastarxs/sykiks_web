import React from 'react'
import { Table, Button } from 'reactstrap'
import Subheader from 'components/Subheader'
import './Contactus.scss'
import { FormattedTime, FormattedDate } from 'react-intl'
let queryParams = {
  archived: 'Archived',
  nonarchived: 'Non-Archived'
}

function Contactus({ contacts, archive, routeParams }) {
  return (
    <div className="container">
      <Subheader subheader={queryParams[routeParams.id] + ` Contacts`} />
      <Table responsive bordered hover className="text-center">
        <thead>
          <tr>
            <th style={{ width: '20%' }}>Name</th>
            <th style={{ width: '15%' }}>Email</th>
            <th style={{ width: '55%' }}>Message</th>
            {queryParams[routeParams.id] == 'Non-Archived' && (
              <th style={{ width: '10%' }} />
            )}
          </tr>
        </thead>
        <tbody>
          {contacts &&
            Object.keys(contacts)
              .reverse()
              .map((key, index) => {
                let contact = contacts[key]
                return (
                  <tr key={key}>
                    <td>
                      <div className="screen-name">{contact.name}</div>
                      <div className="time">
                        <FormattedDate
                          value={contact.time}
                          year="numeric"
                          month="long"
                          day="2-digit"
                        />
                        {'   '}
                        <FormattedTime value={contact.time} />
                      </div>
                    </td>
                    <td>
                      <a href={'mailto:' + contact.email}>{contact.email}</a>
                    </td>
                    <td className="message-list">{contact.message}</td>

                    {queryParams[routeParams.id] == 'Non-Archived' && (
                      <td>
                        <Button color="link" onClick={() => archive(key)}>
                          Archive
                        </Button>
                      </td>
                    )}
                  </tr>
                )
              })}
        </tbody>
      </Table>
    </div>
  )
}

export default Contactus
