import { withFirebase } from 'react-redux-firebase'
import { connect } from 'react-redux'
import { compose, withHandlers, withStateHandlers } from 'recompose'
import { uploadAttachment } from 'utils/helpers'

let recorderRef

export default compose(
  withFirebase,
  connect(({ firebase: { auth } }) => ({
    auth
  })),

  withStateHandlers(
    { progress: false },
    {
      updateProgress: () => progress => ({ progress })
    }
  ),
  withHandlers({
    setRef: () => instance => {
      recorderRef = instance
    },
    attach: ({ firebase, auth, close, onDone, updateProgress }) => () => {
      let filename = `audio/${auth.uid}__${firebase.push().key}.wav`
      let file = recorderRef.chunks[0]
      uploadAttachment(file, filename, updateProgress, url => {
        onDone({ audio: url })
        close()
      })
    }
  })
)
