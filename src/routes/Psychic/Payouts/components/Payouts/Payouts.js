import React from 'react'
import Subheader from 'components/Subheader'
import { Table, Card } from 'reactstrap'
import { FormattedTime, FormattedDate, FormattedNumber } from 'react-intl'
import DefaultMessage from 'components/DefaultMessage'
export const Payouts = ({ payouts }) => (
  <div className="container">
    <Subheader subheader="Payouts" />
    {!payouts && (
      <DefaultMessage>
        No Payouts Yet. All your payouts are seen here.
      </DefaultMessage>
    )}
    {payouts && (
      <Card>
        <Table responsive striped bordered hover>
          <thead>
            <tr>
              <th>Date</th>
              <th>Status</th>
              <th>Paid</th>
              <th>Fees</th>
              <th>Reference id</th>
            </tr>
          </thead>
          <tbody>
            {Object.keys(payouts)
              .reverse()
              .map(key => {
                return (
                  <tr key={key}>
                    <td>
                      <FormattedDate
                        value={payouts[key].date}
                        year="numeric"
                        month="long"
                        day="2-digit"
                      />
                      {'   '}
                      <FormattedTime value={payouts[key].date} />
                    </td>
                    <td>{payouts[key].status}</td>
                    <td>
                      <FormattedNumber
                        style="currency"
                        currency="USD"
                        value={payouts[key].balance - 0}
                      />
                    </td>
                    <td>
                      <FormattedNumber
                        style="currency"
                        currency="USD"
                        value={payouts[key].fees - 0}
                      />
                    </td>
                    <td>{key}</td>
                  </tr>
                )
              })}
          </tbody>
        </Table>
      </Card>
    )}
  </div>
)

export default Payouts
