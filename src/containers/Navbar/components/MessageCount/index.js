import React from 'react'
import enhance from './MessageCount.enhancer'

export const MessageCount = ({ messagesCount }) => {
  let count = 0
  count = messagesCount && Object.keys(messagesCount).length
  return (
    <div className="message-count">
      {count && <span className="badge badge-pill badge-light">{count}</span>}
    </div>
  )
}

export default enhance(MessageCount)
