import { compose } from 'redux'
import _ from 'lodash'
import { connect } from 'react-redux'
import { withHandlers, withStateHandlers } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { withNotifications } from 'modules/notification'
import { UserIsAuthenticated } from 'utils/router'
import { updateImage } from 'utils/helpers'

export default compose(
  UserIsAuthenticated, // redirect to /login if user is not authenticated
  firebaseConnect((props, firebase) => [
    {
      path: `settings/${firebase.firebase._.authUid}/accountInfo/paypalId`,
      storeAs: 'paypalId'
    }
  ]),
  connect(({ firebase: { profile, auth, data: { paypalId } } }) => ({
    profile: { ...profile, paypalId },
    paypalId,
    auth
  })),
  spinnerWhileLoading(['profile', 'paypalId']),
  withNotifications,
  withStateHandlers(
    { progress: false },
    {
      updateProgress: () => progress => ({ progress })
    }
  ),
  withHandlers({
    updateAccount: ({
      firebase,
      profile,
      auth,
      showSuccess,
      showError
    }) => newAccount => {
      newAccount = _.omit(newAccount, 'avatar')
      if (newAccount.screenName.length > 20)
        return showError('Display name should not be more than 20 characters')
      if (profile.type == 'psychic') {
        if (newAccount.chatRate <= 0)
          return showError('Chat Rate must be greater than 0')
        if (newAccount.msgRate <= 0)
          return showError('Message Rate must be greater than 0')
        if (!newAccount.categories)
          return showError('Please select some categories')
        if (profile.paypalId != newAccount.paypalId)
          firebase
            .ref(`settings/${auth.uid}/accountInfo/paypalId`)
            .set(newAccount.paypalId)
        newAccount = _.omit(newAccount, 'paypalId')
      }
      firebase
        .updateProfile(newAccount)
        .then(() => showSuccess('Profile updated successfully'))
        .catch(error => {
          showError('Error updating profile: ', error.message || error)
          console.error('Error updating profile', error.message || error) // eslint-disable-line no-console
          return Promise.reject(error)
        })
    },
    updateImage: ({
      firebase,
      auth,
      showSuccess,
      showError,
      profile,
      updateProgress
    }) => file => {
      firebase
        .storage()
        .ref()
        .child(`avatars/${auth.uid}.png.thumb`)
        .delete()
      firebase
        .storage()
        .ref()
        .child(`avatars/${auth.uid}.png`)
        .delete()

      updateImage(
        firebase,
        auth.uid,
        file,
        showSuccess,
        showError,
        updateProgress
      )
    }
  })
)
