import React from 'react'
import './conversation.scss'
import { isEmpty } from 'react-redux-firebase'
import Icon from 'components/Icon'
import MsgBubble from 'components/MsgBubble'
import Recorder from 'components/Recorder'

const Conversatons = ({
  messages,
  auth,
  roomId,
  profile,
  changeText,
  sendMessage,
  openRecorder,
  closeRecorder,
  record,
  text
}) => (
  <div className="conversations frame">
    <div className="content">
      {isEmpty(messages) ? (
        <p className="text-center empty-msg">Your messages will display here</p>
      ) : (
        <div className="messages-data">
          <ul>
            {Object.keys(messages)
              .reverse()
              .map(key => {
                let message = messages[key]
                return (
                  <div key={key}>
                    <MsgBubble
                      uid={message.from}
                      key={key}
                      time={message.time}
                      msgRate={message.msgRate}>
                      {message.message}
                    </MsgBubble>
                  </div>
                )
              })}
          </ul>
        </div>
      )}
      {roomId && (
        <div className="message-input">
          <Recorder open={record} close={closeRecorder} onDone={sendMessage} />
          <div className="wrap">
            <textarea
              type="text"
              placeholder="Write your message..."
              rows={5}
              onChange={changeText}
              value={text}
            />
            <button className="btn btn-primary" onClick={sendMessage}>
              <Icon name="ios-send" /> Send Message
            </button>
            <button className="btn btn-primary" onClick={openRecorder}>
              <Icon name="ios-mic" /> Record
            </button>
          </div>
        </div>
      )}
    </div>
  </div>
)

export default Conversatons
