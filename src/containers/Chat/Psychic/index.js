import React from 'react'
import { Modal, ModalBody, Card, CardBody, Button } from 'reactstrap'
import enhancer from './enhancer'
import _ from 'lodash'
import { AvatarByUid } from 'components/Avatar'
import ScreenName from 'components/ScreenName'
import Sound from 'react-sound'

import '../Chat.scss'
let ClientChatRequest = ({
  chatRequests,
  denyRequest,
  acceptRequest,
  currentSessionKey,
  blocked
}) => {
  if (currentSessionKey) return null
  let open = _.isEmpty(chatRequests)
  if (open) return null
  let requestId = false
  let request = {}
  _.eachRight(chatRequests, (v, k) => {
    if (v.status == 'init' && !(blocked && blocked[v.client])) {
      requestId = k
      request = v
    }
  })

  open = request.status != 'init'
  return (
    <Modal isOpen={!open}>
      <ModalBody>
        <Card>
          <CardBody className="text-center client-chat-request">
            <Sound url="/ring.mp3" playStatus={'PLAYING'} loop />
            <div className="text">
              <span className="purple-text">
                <ScreenName uid={request.client} />
              </span>{' '}
              wants to talk to you
            </div>
            <div className="text">
              <AvatarByUid uid={request.client} className="avt-image" />
            </div>
            <Button
              color="primary"
              onClick={() => acceptRequest(requestId, request.client)}>
              Accept
            </Button>
            <Button
              className="light-purple-button"
              onClick={() => denyRequest(requestId)}>
              Deny
            </Button>
          </CardBody>
        </Card>
      </ModalBody>
    </Modal>
  )
}
export default enhancer(ClientChatRequest)
