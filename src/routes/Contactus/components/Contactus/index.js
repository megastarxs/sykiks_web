import Contactus from './Contactus'
import enhance from './enhancer'

export default enhance(Contactus)
