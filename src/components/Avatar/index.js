import React from 'react'
import _ from 'lodash'
import enhance from './Avatar.enhancer'
import defaultUserImageUrl from 'static/images/user-default.png'

const Avatar = props => {
  let { divClassName, src } = props
  let imgurl = defaultUserImageUrl
  if (src) {
    imgurl = src.replace('.png', '.png.thumb').replace('http:', '')
  }
  return (
    <div className={` ${divClassName || ''} avtar-by-uid`}>
      <img src={imgurl} {..._.pick(props, ['className', 'style'])} />
    </div>
  )
}

export const AvatarbyImage = Avatar
export const AvatarByUid = enhance(Avatar)
