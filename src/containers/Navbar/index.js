import React from 'react'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  Form,
  FormGroup,
  Input
} from 'reactstrap'
import { Link } from 'react-router'
import { AvatarByUid } from 'components/Avatar'
// import Search from './Search'
import MessageCount from './components/MessageCount'
import enhance from './components/Navbar.enhancer'
import User from './components/User'
import Admin from './components/Admin'
import './Navbar.scss'
import Icon from 'components/Icon'
import AddCreditsModal from './components/AddCreditsModal'
import AccountBalance from './components/AccountBalance'
let SykiksNavbar = ({
  handleLogout,
  authExists,
  profile,
  auth,
  updateStatus,
  isOpen,
  toggle,
  openCreditsModal,
  toggleCreditsModal,
  config
}) => {
  if (config.desktop && !authExists) return null
  return (
    <div>
      <Navbar
        color="light"
        light
        expand="lg"
        className="top-navbar"
        id="topnav">
        <div className="container">
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
              <NavItem>
                <NavLink tag={Link} to="/" className="">
                  <Icon name="ios-home" />
                </NavLink>
              </NavItem>
              <NavItem>
                {/* <Search /> */}
                {profile.type == 'psychic' &&
                  profile.status != 'pending' &&
                  profile.status != 'Rejected' &&
                  auth.emailVerified && (
                    <Form className="form-inline status-box">
                      <FormGroup>
                        <label className="status-title">Status : </label>
                        <Input
                          type="select"
                          name="select"
                          value={profile.status}
                          onChange={updateStatus}>
                          <option>Available</option>
                          <option>Offline</option>
                        </Input>
                      </FormGroup>
                    </Form>
                  )}
              </NavItem>
            </Nav>
            {authExists ? (
              <div className="display-inherit">
                {profile.type != 'admin' && auth.emailVerified && <User />}
                <Nav className="ml-auto default-navbar" navbar>
                  <NavItem>
                    <NavLink
                      tag={Link}
                      activeClassName="active"
                      className="d-flex align-items-center"
                      to={'/messages'}>
                      <Icon name="ios-mail" className="icon" />
                      Messages
                      <MessageCount type={profile.type} />
                    </NavLink>
                  </NavItem>

                  <NavItem>
                    <NavLink
                      tag={Link}
                      activeClassName="active"
                      to="/account"
                      className="d-flex align-items-center">
                      <Icon name="ios-settings" className="icon" />
                      Settings
                    </NavLink>
                  </NavItem>

                  {profile.type != 'admin' && (
                    <NavItem>
                      <span className="d-flex align-items-center nav-link">
                        <Icon name="logo-usd" className="icon" />
                        <AccountBalance />
                      </span>
                    </NavItem>
                  )}
                  <NavItem>
                    {profile.type == 'client' &&
                      auth.emailVerified && (
                        <NavLink
                          tag={Link}
                          target="_blank"
                          activeClassName="active"
                          to="/addfunds"
                          className="d-flex align-items-center">
                          <Icon name="md-add-circle" className="icon" />
                          Add Funds
                        </NavLink>
                      )}
                  </NavItem>

                  <li className={`dropdown nav-item navbar-menu`}>
                    <a className="nav-link dropdown-toggle" href="#">
                      <AvatarByUid
                        uid={auth.uid}
                        className="rounded-circle"
                        style={{ width: 30, height: 30, objectFit: 'cover' }}
                      />
                      <strong className="profile-header">
                        {' '}
                        Hi, {profile.screenName}{' '}
                      </strong>
                    </a>
                    <div className="dropdown-menu">
                      {profile.type == 'psychic' &&
                        auth.emailVerified && (
                          <Link to="/payouts" className="dropdown-item">
                            Payouts
                          </Link>
                        )}
                      {/* {profile.type == 'client' &&
                      auth.emailVerified && (
                        <a
                          className="dropdown-item"
                          onClick={() => toggleCreditsModal()}>
                          Add credits
                        </a>
                      )} */}

                      <Link to="/change-pass" className="dropdown-item">
                        Change Password
                      </Link>

                      <a className="dropdown-item" onClick={handleLogout}>
                        Logout
                      </a>
                    </div>
                  </li>
                </Nav>
              </div>
            ) : (
              <Nav className="ml-auto no-desktop" navbar>
                <NavItem>
                  <NavLink tag={Link} activeClassName="active" to="/login">
                    LOGIN
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} activeClassName="active" to="/signup">
                    SIGN UP
                  </NavLink>
                </NavItem>
              </Nav>
            )}
          </Collapse>
        </div>
      </Navbar>
      {profile.type == 'admin' && <Admin />}
      {auth && (
        <AddCreditsModal
          isOpen={openCreditsModal}
          toggle={toggleCreditsModal}
          auth={auth}
        />
      )}
    </div>
  )
}
export default enhance(SykiksNavbar)
