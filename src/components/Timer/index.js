import React, { Component } from 'react'
import moment from 'moment'

class StopWatch extends Component {
  state = {
    displayTime: '00:00:00'
  }

  componentDidMount() {
    let { startTime, start } = this.props
    if (start) {
      this.start(moment(new Date(startTime)))
    }
  }

  componentWillReceiveProps(newProps) {
    let { start, startTime } = newProps
    if (start) {
      this.start(moment(new Date(startTime)))
    } else this.stop()
  }

  stop() {
    clearInterval(this.interval)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  start(startTime) {
    let {
      callback,
      perHalfMinute,
      serverTimeOffset,
      countdown,
      countdownCallback
    } = this.props
    this.interval = this.interval
      ? this.interval
      : setInterval(() => {
          let a = startTime
          let seconds
          var b = moment(new Date())
          if (countdown)
            seconds = Math.round(
              a.diff(b, 'seconds') + countdown - serverTimeOffset / 1000
            )
          else
            seconds = Math.round(
              -1 * a.diff(b, 'seconds') + serverTimeOffset / 1000
            )
          let displayTime = moment()
            .hour(0)
            .minute(0)
            .second(seconds)
            .format('HH:mm:ss')
          if (seconds == 0 && countdownCallback) countdownCallback()
          if (seconds % 30 == 0 && perHalfMinute)
            perHalfMinute(seconds, displayTime)
          this.setState({ displayTime })
          if (callback) callback(displayTime)
        }, 1000)
  }

  render() {
    let { displayTime } = this.state

    return (
      <div>
        <span>{displayTime}</span>
      </div>
    )
  }
}

export default StopWatch
