import { connect } from 'react-redux'
import { compose, withHandlers } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated, UserIsAdmin } from 'utils/router'
import { withNotifications } from 'modules/notification'

export default compose(
  UserIsAuthenticated,
  UserIsAdmin,
  firebaseConnect(({ routeParams }) => {
    let queryParams = {
      archived: ['orderByChild=archived', 'notParsed', 'equalTo=' + true],
      nonarchived: ['orderByChild=archived', 'notParsed', 'equalTo=null']
    }
    return [
      {
        path: 'contactus',
        storeAs: `contacts` + routeParams.id,
        queryParams: queryParams[routeParams.id]
      }
    ]
  }),
  connect(({ firebase: { data } }, { routeParams }) => ({
    contacts: data[`contacts` + routeParams.id]
  })),
  withNotifications,
  withHandlers({
    archive: ({ firebase, showSuccess }) => id => {
      if (confirm('Are you sure you want to archive this item?'))
        firebase
          .ref(`contactus/${id}/archived`)
          .set(true)
          .then(() => {
            showSuccess(`This item has been archived.`)
          })
    }
  }),
  spinnerWhileLoading(['contacts'])
)
