import { compose, withStateHandlers } from 'recompose'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'

import { spinnerWhileLoading } from 'utils/components'

export default compose(
  // create listener for admin, results go into redux
  firebaseConnect(({ limit }, { firebase }) => {
    let arr = [
      {
        path: 'users',
        queryParams: [
          'orderByChild=type',
          'equalTo=psychic'
          // limit ? `limitToFirst=${limit}` : ''
        ],
        storeAs: `psychics_${limit}`
      }
    ]
    if (firebase._.authUid)
      arr.push({
        path: `settings/${firebase._.authUid}/favorites`,
        storeAs: 'favorites'
      })
    return arr
  }),
  // map redux state to props
  connect(({ firebase: { data, auth } }, { limit }) => ({
    psychics: data[`psychics_${limit}`],
    favorites: data.favorites,
    profile: auth
  })),
  withStateHandlers(
    {
      pageno: 1
    },
    {
      updatePageno: ({ pageno }) => () => ({
        pageno: pageno + 1
      })
    }
  ),
  spinnerWhileLoading(['psychics'])
)
