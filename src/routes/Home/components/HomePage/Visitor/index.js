import React from 'react'
import './HomePage.scss'
import PsychicList from '../Client/PsychicList'
import Topbar from 'components/Topbar'
import { Link } from 'react-router'

export default ({ profile }) => (
  <div>
    <title>Sykiks - Online Psychic Reading, Love Guidance, Live Chat</title>
    <div className="client-home-page">
      <div className="jumbotron">
        <div className="container headers j-inner">
          <Topbar transparent />
          <h1 className="mainh1 headerh1 ml-auto mr-auto">
            Love Readings, Astrology, Tarot &amp; More
          </h1>
          <h5 className="headerh1 ml-auto mr-auto">
            Get a deeper look into what's in store for your future
          </h5>
          <br />
          <Link
            to="/psychics"
            className="btn transparent-button btn-lg buttons">
            BROWSE PSYCHICS
          </Link>
          <Link to="/signup/client" className="btn green-button btn-lg buttons">
            {' '}
            GET STARTED FREE{' '}
          </Link>
          <br />
        </div>
        <div className="free-min">
          <i className="ion-ios-star" /> First 3 minutes free!
        </div>
      </div>
    </div>
    <div className="container">
      <div className="row home-points">
        <h2>Live Readings from Trusted Psychics</h2>
        <div className="col-md-4">
          <i className="ion-ios-heart" />
          <h4>Feeling Stuck?</h4>
          <p>
            Get answers to questions that your gut has been telling you to ask.
            Is he the one? Is this job for you? Are they telling the truth?
          </p>
        </div>
        <div className="col-md-4">
          <i className="ion-ios-chatboxes" />
          <h4>1-on-1 Chat</h4>
          <p>
            Select any of our highy experienced readers to speak with via live
            pay-per-minute chat or send them an offline message 24/7.
            (individual rates apply)
          </p>
        </div>
        <div className="col-md-4">
          <i className="ion-ios-flower" />
          <h4>Tested &amp; Trusted Guidance</h4>
          <p>
            Each psychic we've selected to join the Sykiks platform has been
            personally interviewed for accuracy. Meaning, if we haven't received
            a reading from them, neither will you.
          </p>
        </div>
      </div>
      <hr />
      <PsychicList limit="6" />
      <center>
        <Link to="/psychics" className="btn view-more">
          VIEW ALL PSYCHICS
        </Link>
      </center>
    </div>
  </div>
)
