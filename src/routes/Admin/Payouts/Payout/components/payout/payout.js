import React from 'react'
import { FormattedNumber } from 'react-intl'
import Subheader from 'components/Subheader'
import './payout.scss'
import { Table, Button } from 'reactstrap'
import ProfileModal from 'components/ProfileModal'
const Payoutdetails = ({
  payoutdetails,
  id,
  profileId,
  open,
  updateProfileModal,
  closeModal
}) => (
  <div className="payoutdetails container">
    <Subheader subheader={'Payout Details of ' + id} />
    <Table responsive bordered hover className="text-center">
      <thead>
        <tr>
          <th>Screen Name</th>
          <th>Paypal Id</th>
          <th>Paid</th>
          <th>Fees</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        {Object.keys(payoutdetails)
          .reverse()
          .map((key, index) => {
            let payout = payoutdetails[key]
            return (
              <tr key={key}>
                <td>
                  <Button
                    color="link"
                    onClick={() => updateProfileModal(payout.key)}>
                    {payout.screenName}{' '}
                  </Button>
                </td>
                <td>
                  <a href={'mailto:' + payout.paypalId}>{payout.paypalId}</a>
                </td>
                <td className="currency">
                  <FormattedNumber
                    style="currency"
                    currency="USD"
                    value={payout.balance - 0}
                  />
                </td>
                <td className="currency">
                  <FormattedNumber
                    style="currency"
                    currency="USD"
                    value={payout.fees - 0}
                  />
                </td>
                <td className="currency">
                  <FormattedNumber
                    style="currency"
                    currency="USD"
                    value={payout.balance - 0 + payout.fees}
                  />
                </td>
              </tr>
            )
          })}
      </tbody>
    </Table>
    <ProfileModal
      key={profileId}
      uid={profileId}
      open={open}
      onClose={() => closeModal()}
    />
  </div>
)

export default Payoutdetails
