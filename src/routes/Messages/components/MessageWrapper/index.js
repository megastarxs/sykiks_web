import React from 'react'
import enhance from './MessageWrapper.enhancer'
import Messages from '../Messages'
export const MessageWrapper = ({ profile, roomId }) => (
  <Messages profile={profile} roomId={roomId} />
)

export default enhance(MessageWrapper)
