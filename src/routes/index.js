import CoreLayout from '../layouts/CoreLayout'
import Home from './Home'

import PsychicsRoute from './Home/components/HomePage/Client/route'

import LoginRoute from './Login'
import SignupRouteIndex from './Signup/components/Index'
import SignupRoute from './Signup/components/SignupPage'
import ContactRoute from './Contactus'
import ChangePassRoute from './ChangePass'
import AccountRoute from './Account'
import NotFoundRoute from './NotFound'
import DownloadRoute from './Download'
import ProfileRoute from './Profile'
import PaymentRoute from './Payment'
import ForgotPassRoute from './ForgotPass'

import PayoutsRoute from './Psychic/Payouts'
import AddFundsRoute from './AddFunds'

import MessagesRoute from './Messages'

import HistoryRoute from './History/index'
import ChatHistoryRoute from './History/ChatHistory'

import MasterConfigRoute from './Admin/MasterConfig'
import ReportsRoute from './Admin/Reports'
import AdminPayoutsRoute from './Admin/Payouts/index'
import PayoutRoute from './Admin/Payouts/Payout'
import PayoutsPendingRoute from './Admin/Payouts/Pending'
import AdminContactsRoute from './Admin/contactus/index'
import AdminApproveUserRoute from './Admin/ApproveUser/index'
import AdminUsersRoute from './Admin/Users/index'

import PagesRoute from './Pages'
import ReviewRoute from './Review'

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

export const createRoutes = store => ({
  path: '/',
  component: CoreLayout,
  indexRoute: Home,
  childRoutes: [
    AccountRoute(store),
    LoginRoute(store),
    SignupRouteIndex(store),
    SignupRoute(store),
    ContactRoute(store),
    ProfileRoute(store),
    MessagesRoute(store),
    PaymentRoute(store),
    HistoryRoute(store),
    PayoutsRoute(store),
    PayoutsPendingRoute(store),
    PsychicsRoute(store),
    ChatHistoryRoute(store),
    ReviewRoute(store),
    ForgotPassRoute(store),
    ChangePassRoute(store),
    AddFundsRoute(store),
    // Admin routes
    ReportsRoute(store),
    AdminPayoutsRoute(store),
    PayoutRoute(store),
    PagesRoute(store),
    MasterConfigRoute(store),
    AdminContactsRoute(store),
    AdminApproveUserRoute(store),
    AdminUsersRoute(store),
    DownloadRoute(store),
    // AsyncRoute(store) // async routes setup by passing store
    // SyncRoute, // sync routes just need route object by itself
    /* Place all Routes above here so NotFoundRoute can act as a 404 page */
    NotFoundRoute(store)
  ]
})

/*  Note: childRoutes can be chunked or otherwise loaded programmatically
    using getChildRoutes with the following signature:

    getChildRoutes (location, cb) {
      require.ensure([], (require) => {
        cb(null, [
          // Remove imports!
          require('./Counter').default(store)
        ])
      })
    }

    However, this is not necessary for code-splitting! It simply provides
    an API for async route definitions. Your code splitting should occur
    inside the route `getComponent` function, since it is only invoked
    when the route exists and matches.
*/

export default createRoutes
