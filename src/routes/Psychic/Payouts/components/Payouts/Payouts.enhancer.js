import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'

export default compose(
  UserIsAuthenticated,
  firebaseConnect((props, firebase) => [
    {
      path: `settings/${firebase.firebase._.authUid}/payouts`,
      storeAs: 'payouts'
    }
  ]),
  connect(({ firebase: { data } }) => ({
    payouts: data.payouts
  })),
  spinnerWhileLoading(['payouts'])
)
