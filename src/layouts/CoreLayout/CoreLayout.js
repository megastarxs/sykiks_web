import React from 'react'
import Navbar from 'containers/Navbar'
import { Notifications } from 'modules/notification'
import Footer from 'components/Footer'
import enhancer from './enhancer'
import Topbar from 'components/Topbar'
import Alerts from 'containers/Alerts'
import AlertContainer from 'containers/Alerts/AlertContainer'

export const CoreLayout = ({
  children,
  profile,
  location,
  auth,
  maintenanceMode,
  updateconfig,
  config,
  loggedIn
}) => {
  if (maintenanceMode && profile.type != 'admin' && loggedIn)
    return (
      <AlertContainer>
        <p> This site is under maintenance. </p>
      </AlertContainer>
    )
  else
    return (
      <div className={config.desktop ? 'desktop-mode' : ''}>
        <Navbar />
        <Notifications />
        <div style={{ minHeight: config.desktop ? '83vh' : '100vh' }}>
          {location.pathname == '/' && profile.type && <Topbar />}
          {location.pathname != '/' && <Topbar />}
          <Alerts>{children}</Alerts>
        </div>
        {!config.desktop && <Footer />}
      </div>
    )
}
export default enhancer(CoreLayout)
