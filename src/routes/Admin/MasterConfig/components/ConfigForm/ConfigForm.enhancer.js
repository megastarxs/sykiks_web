import { reduxForm } from 'redux-form'

export default reduxForm({ form: 'config' })
