import React from 'react'
import './ForgotPass.scss'
let inputRef
export const ForgotPass = ({ resetPass }) => (
  <div className="form-forgotpass">
    <title>Sykiks - Forgot Password</title>
    <div>
      <div className="form-group">
        <label className="label-style">Email</label>
        <input
          type="text"
          className="form-control"
          placeholder="Enter email here"
          ref={ref => (inputRef = ref)}
        />
        <button
          className="btn btn-lg btn-info btn-block button-style"
          onClick={() => resetPass(inputRef.value)}>
          Reset Password
        </button>
      </div>
    </div>
  </div>
)

export default ForgotPass
