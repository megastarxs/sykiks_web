import React from 'react'
import './Contactus.scss'
import Form from './ContactUsForm'

const Contactus = ({ submit, captchaVerified }) => (
  <div className="container">
    <div className="contact-page">
      <div className="page-content col-8 offset-2 text-center">
        <p>
          {' '}
          For further enquiries or troubleshooting, please message us via our
          contact for below and someone from our support team will respond to
          you within 24 hours.{' '}
        </p>
      </div>
      <div className="row contain">
        <div className="col-md-6 col-lg-6 col-sm-6 lcol">
          <p className="company">SYKIKS LTD</p>
          <p className="address1">130 OLD STREET,</p>
          <p className="address">LONDON, EC1V9BD</p>
          <p className="email">Email Address</p>
          <p className="emailid">support@sykiks.com</p>
        </div>
        <div className="col-md-6 col-lg-6 col-sm-6 form-div lcol">
          <Form onSubmit={submit} captchaVerified={captchaVerified} />
        </div>
      </div>
    </div>
  </div>
)

export default Contactus
