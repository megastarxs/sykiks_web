import React from 'react'
import { Button, Card, CardBody, CardTitle, CardDeck } from 'reactstrap'
export const AddFunds = ({ addCredits, loading }) => (
  <div className="form-addfunds container text-center mt-5">
    <title>Sykiks - Add Funds</title>
    {!loading ? (
      <CardDeck>
        <Card>
          <CardBody onClick={() => addCredits(15)}>
            <CardTitle>$15</CardTitle>
            <Button color="primary">Buy</Button>
          </CardBody>
        </Card>
        <Card>
          <CardBody onClick={() => addCredits(50)}>
            <CardTitle>$50</CardTitle>
            <Button color="primary">Buy</Button>
          </CardBody>
        </Card>
        <Card>
          <CardBody onClick={() => addCredits(100)}>
            <CardTitle>$100</CardTitle>
            <Button color="primary">Buy</Button>
          </CardBody>
        </Card>
      </CardDeck>
    ) : (
      <h3> Please wait.. </h3>
    )}
  </div>
)

export default AddFunds
