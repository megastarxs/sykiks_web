// ------------------------------------
// Constants
// ------------------------------------
export const CONFIG_CHANGE = 'CONFIG_CHANGE'

// ------------------------------------
// Actions
// ------------------------------------
export function configChange(config = '/') {
  return {
    type: CONFIG_CHANGE,
    payload: config
  }
}

// ------------------------------------
// Specialized Action Creator
// ------------------------------------
export const updateconfig = (dispatch, nextconfig) =>
  dispatch(configChange(nextconfig))

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = { desktop: false }
export default function configReducer(state = initialState, action) {
  return action.type === CONFIG_CHANGE ? action.payload : state
}
