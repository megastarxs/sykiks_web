import React from 'react'
import { Modal, ModalBody } from 'reactstrap'
import { AvatarByUid } from 'components/Avatar'
import enhance from './ProfileModal.enhancer'
import './ProfileModal.scss'
import AdminModal from './AdminModal'
import UserModal from './UserModal'
import { FormattedDate } from 'react-intl'
const ProfileModalContent = props => {
  let { user, uid, profile } = props
  return (
    <div className="text-center modal-window">
      <ModalBody>
        <h5>{user.screenName}</h5>
        <AvatarByUid
          uid={uid}
          style={{
            height: 150,
            width: 150,
            marginLeft: 'auto',
            marginRight: 'auto',
            objectFit: 'cover',
            borderRadius: '50%',
            marginTop: '1rem'
          }}
        />
        <div>
          <span className="member-since">Member Since : </span>
          <span className="member-since-time">
            <FormattedDate
              value={user.createdAt}
              year="numeric"
              month="long"
              day="2-digit"
            />
          </span>
        </div>
        {profile.type != 'admin' && <UserModal {...props} />}
        {profile.type == 'admin' && <AdminModal {...props} />}
      </ModalBody>
    </div>
  )
}
let ProfileModalContentEnhance = enhance(ProfileModalContent)

const ProfileModal = ({ classes, open, onClose, uid }) => (
  <Modal toggle={onClose} isOpen={open}>
    <ProfileModalContentEnhance uid={uid} onClose={onClose} />
  </Modal>
)

export default ProfileModal
export ProfileModalHandler from './ProfileModalHandler'
