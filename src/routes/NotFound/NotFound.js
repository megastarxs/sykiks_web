import React from 'react'
import Icon from 'components/Icon'
export const NotFound = () => (
  <div className="container">
    <div className="text-center not-verified">
      <Icon name="md-information-circle" />
      <h1>Whoops! 404!</h1>
      <p>This page was not found.</p>
    </div>
  </div>
)

export default NotFound
