import React from 'react'
import './PsychicList.scss'
import Icon from 'components/Icon'
import Select from 'react-select'
import { CATEGORIES_LIST } from 'constants'

let categoriesList = CATEGORIES_LIST.map(d => ({ name: d }))

export const Filter = ({ updateFilter, filters, auth }) => (
  <div className="container filter-div">
    <div className="filter">
      <div className="row">
        <div className="sb-filter">
          <span className="label">Sort By</span>
          <select
            className="select-box"
            onChange={e => updateFilter({ sortBy: e.target.value })}>
            <option>Top Psychics</option>
            <option>New Psychics</option>
            <option>Price Lowest</option>
            <option>Price Highest</option>
          </select>
        </div>
      </div>
      <div className="cat-filters">
        <Select
          onChange={specialities => updateFilter({ specialities })}
          options={categoriesList}
          value={filters.specialities}
          multi
          simpleValue
          clearable={false}
          searchable
          labelKey="name"
          valueKey="name"
          delimiter=", "
          closeOnSelect={false}
          renderHiddenField
          className="placeholder"
          valueRenderer={props => null}
          inputRenderer={props => (
            <label className="label-style">
              <span>Select Categories </span>
              <span style={{ float: 'right' }}>
                <Icon name="md-menu" />
              </span>
              <input {...props} className="hide-input" />
            </label>
          )}
          placeholder=""
          openOnFocus
        />
      </div>
      <div className="selected-cats">
        <Select
          onChange={specialities => updateFilter({ specialities })}
          options={categoriesList}
          value={filters.specialities}
          multi
          simpleValue
          clearable={false}
          openOnClick={false}
          labelKey="name"
          valueKey="name"
          placeholder=""
          delimiter=", "
          className="category-list ml-auto mr-auto"
          inputRenderer={props => null}
        />
      </div>
      <div className="row av-fav">
        <div className="inner">
          <input
            type="checkbox"
            className="filter-checkbox"
            onChange={e => updateFilter({ available: e.target.checked })}
          />
          <span className="filter-checkbox-title">Available</span>
          {auth.uid && (
            <span>
              <input
                type="checkbox"
                className="filter-checkbox"
                onChange={e => updateFilter({ favorites: e.target.checked })}
              />
              <span className="filter-checkbox-title">Favorites</span>
            </span>
          )}
        </div>
      </div>
    </div>
  </div>
)

export default Filter
