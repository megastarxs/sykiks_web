import React from 'react'
import enhancer from './enhancer'
import Client from './Client'
import Visitor from './Visitor'
import Psychic from './Psychic'
import PsychicDesktop from './PsychicDesktop'
import Admin from './Admin'
export const Home = ({ profile, auth, verifyEmail, loggedIn, isDesktop }) => {
  if (!loggedIn) return <Visitor />
  if (profile.type == 'client') return <Client profile={profile} />
  if (profile.type == 'admin') return <Admin />
  if (profile.type == 'psychic') {
    if (isDesktop) return <PsychicDesktop />
    else return <Psychic />
  }
}

export default enhancer(Home)
