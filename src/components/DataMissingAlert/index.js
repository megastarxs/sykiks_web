import React from 'react'
import { Link } from 'react-router'
let DataMissingAlert = ({ profile, accountInfo }) => {
  let showChatRateAlert = false
  let showMsgRateAlert = false
  let showPaypalAlert = false
  if (profile.chatRate <= 0 || !profile.chatRate) showChatRateAlert = true
  if (profile.msgRate <= 0 || !profile.msgRate) showMsgRateAlert = true
  if (!accountInfo) showPaypalAlert = true
  else if (accountInfo && !accountInfo.paypalId) showPaypalAlert = true

  return (
    <div
      className="col-lg-8 col-md-8"
      style={{ marginLeft: 'auto', marginRight: 'auto' }}>
      {(showChatRateAlert || showMsgRateAlert || showPaypalAlert) && (
        <div className="alert alert-danger" role="alert">
          <div className="bold-link">In order to receive payments</div>
          {showPaypalAlert && <li>Please update your PayPal address.</li>}
          {showChatRateAlert && <li>Please set your chat rate.</li>}
          {showMsgRateAlert && <li>Please set your message rate.</li>}
          <Link to="/account" className="bold-link">
            Click Here
          </Link>{' '}
          To update
        </div>
      )}
    </div>
  )
}

export default DataMissingAlert
