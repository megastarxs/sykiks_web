import React from 'react'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import Icon from 'components/Icon'
import { withHandlers, compose } from 'recompose'
let enhance = compose(
  firebaseConnect((props, firebase) => {
    let arr = []
    if (firebase.firebase._.authUid)
      arr = [
        {
          path: `settings/${firebase.firebase._.authUid}/favorites`,
          storeAs: 'favorites'
        }
      ]
    return arr
  }),
  connect(({ firebase: { data: { favorites }, auth } }) => ({
    favorites,
    me: auth.uid
  })),
  withHandlers({
    addToFavorites: ({ firebase, me }) => uid => {
      let obj = {}
      obj[uid] = true
      firebase.ref(`settings/${me}/favorites`).update(obj)
    },
    removeFromFavorites: ({ firebase, me }) => uid => {
      firebase.ref(`settings/${me}/favorites/${uid}`).remove()
    }
  })
)

let favoriteIcon = ({
  favorites,
  uid,
  addToFavorites,
  removeFromFavorites,
  me
}) => (
  <div>
    {(!favorites || !favorites[uid]) &&
      me && (
        <button
          type="button"
          className="heart-button"
          onClick={() => addToFavorites(uid)}>
          <Icon name="ios-heart" className="grey-favorite" />
        </button>
      )}
    {favorites &&
      favorites[uid] &&
      me && (
        <button
          type="button"
          className="heart-button"
          onClick={() => removeFromFavorites(uid)}>
          <Icon name="ios-heart" className="pink-favorite" />
        </button>
      )}
  </div>
)

export default enhance(favoriteIcon)
