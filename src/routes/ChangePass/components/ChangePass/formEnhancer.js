import { reduxForm } from 'redux-form'
import { CHANGEPASS_FORM_NAME } from 'constants'

export default reduxForm({
  form: CHANGEPASS_FORM_NAME
})
