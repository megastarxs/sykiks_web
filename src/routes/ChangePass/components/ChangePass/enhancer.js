import { withFirebase } from 'react-redux-firebase'
import { withHandlers, pure, compose } from 'recompose'
import { withNotifications } from 'modules/notification'
import { UserIsAuthenticated } from 'utils/router'
export default compose(
  UserIsAuthenticated,
  withNotifications, // add props.showError
  withFirebase, // add props.firebase (firebaseConnect() can also be used)
  // Handlers

  withHandlers({
    submit: ({ firebase, showSuccess, showError, router }) => form => {
      let oldPass = form.oldPass
      let newPass = form.newPass
      let confirmPass = form.confirmPass
      var user = firebase.auth().currentUser
      var cred = firebase.auth.EmailAuthProvider.credential(user.email, oldPass)
      if (newPass == confirmPass) {
        user
          .reauthenticateAndRetrieveDataWithCredential(cred)
          .then(function() {
            user
              .updatePassword(newPass)
              .then(function() {
                showSuccess(`Password Updated Successfully`)
              })
              .catch(function(error) {
                showError(error.message)
              })
          })
          .catch(function() {
            showError(`Oops! Old Password entered is Invalid`)
          })
      }
      if (newPass != confirmPass)
        showError(`New Password and Confirm Password must be equal`)
    }
  }),
  pure // shallow equals comparison on props (prevent unessesary re-renders)
)
